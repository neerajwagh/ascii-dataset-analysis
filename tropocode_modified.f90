! i) Subsetting a region
! Say you have a global dataset, and you only want the data for the northeast portion of the Pacific Ocean.
	! $ ncea -d lat,minimum_lat,maximum_lat -d lon,minimum_lon,maximum_lon in.nc out.nc
! where "lat" is what latitudes are called your file, and minimum_lat and maximum_lat are latitudes. Integer latitudes/longitudes are treated as indices, and floating point latitudes/longitudes are actual latitudes/longitudes. An example is in the NCO documentation.

! gfortran -fbacktrace -fbounds-check -ffree-line-length-1000 pwl_interp_1d.f90 tropocode_modified.f90 -o tropocode -I/usr/local/include -L/usr/local/lib -lnetcdff && ./tropocode

program main
	use netcdf
	! use datetime_module, only: datetime, timedelta, clock
	implicit none

	! files and handles
	integer :: in_ncid, out_ncid
	real :: trp_SINGLE, trp_DOUBLE
	character (len = *), parameter :: INPUT_FILE_NAME = "../data/original/ERA_Interim/Air_Temperature/T_Aug_2016_all_lev_daily.nc"
	integer, parameter :: NLATS = 241, NLONS = 480, NRECS = 31, NLVLS = 37
	! character (len = *), parameter :: INPUT_FILE_NAME = "../data/original/ERA_Interim/Air_Temperature/airtemp_2014_daily.nc"
	! integer, parameter :: NLATS = 181, NLONS = 360, NRECS = 31, NLVLS = 17
	
	! OUTPUT FILE: For every lat,lon grid point, write the pressure levels and corresponding lapse rate
	! character (len = *), parameter :: OUTPUT_FILE_NAME = "../data/T_Aug_2016_lapse_rate.nc"
	
	! name given to variables in files
	character (len = *), parameter :: UNITS_STRING =    "units"
	
	character (len = *), parameter :: LAT_NAME =        "latitude"
	character (len = *), parameter :: LAT_UNITS =       "degrees_north"
	
	character (len = *), parameter :: LON_NAME =        "longitude"
	character (len = *), parameter :: LON_UNITS =       "degrees_east"
	
	character (len = *), parameter :: REC_NAME =        "time"
	character (len = *), parameter :: REC_UNITS =       "hours since 1900-01-01 00:00:0.0"
	character (len = *), parameter :: CALENDAR_UNITS =  "gregorian"
	
	character (len = *), parameter :: LVL_NAME =        "level"
	character (len = *), parameter :: LVL_UNITS =       "millibar"
	
	character (len = *), parameter :: AIRTEMP_NAME =    "t"
	
	character (len = *), parameter :: LAPSE_RATE_NAME =  "lapse_rate"
	character (len = *), parameter :: LAPSE_RATE_UNITS = "K/km"
	
	integer :: start(4), count(4), rec, lat, lon, airtemp_i
	real :: lats    (NLATS) 
    real :: lons    (NLONS)
    real :: levels  (NLVLS)
	real :: recs  (NRECS)
	! since we're reading one timestep at a time
	real :: airtemp_in  (NLONS, NLATS, NLVLS), scale_factor, add_offset
	integer :: lon_varid, lat_varid, rec_varid, lvl_varid, airtemp_varid
	integer :: bounding_box_bottom, bounding_box_left, bounding_box_right, bounding_box_top, NO_OF_ADJACENT_GRIDS_TO_SCAN, i, lat_range_higher, lat_range_lower, lon_range_higher, lon_range_lower
	real :: search_for_lat, search_for_lon

	! real :: tp1 (NLONS, NLATS)
	! real :: tp2 (NLONS, NLATS)

	real :: across_all_lons_sum_trp1
	real :: across_all_lons_sum_trp2
	real :: count_for_avg
	real :: across_all_lons_avg_trp1
	real :: across_all_lons_avg_trp2
	integer :: CHECK_FOR_REC
	real :: CHECK_FOR_LAT_LOWER
	real :: CHECK_FOR_LAT_UPPER
	real :: CHECK_FOR_LON_LOWER
	real :: CHECK_FOR_LON_UPPER
	

	! total number of pressure levels after interpolation at 10hPa
	! 10hPa INTERPOLATION WORKS BEST!
	integer, parameter :: interpol_NLVLS = 101
	! integer, parameter :: interpol_NLVLS = 34
	! integer, parameter :: interpol_NLVLS = 50
	! integer, parameter :: interpol_NLVLS = 26
	! integer, parameter :: interpol_NLVLS = 20
	! integer, parameter :: interpol_NLVLS = 67
	
	real :: interpol_levels(interpol_NLVLS)
	data interpol_levels /0.,   10.,   20.,   30.,   40.,   50.,   60.,   70.,   80.,   90.,  100.,	110.,  120.,  130.,  140.,  150.,  160.,  170.,  180.,  190.,  200.,  210.,  220.,  230.,  240.,  250.,  260.,  270.,  280.,  290.,  300.,  310.,  320.,	330.,  340.,  350.,  360.,  370.,  380.,  390.,  400.,  410.,  420.,  430.,	440.,  450.,  460.,  470.,  480.,  490.,  500.,  510.,  520.,  530.,  540.,	550.,  560.,  570.,  580.,  590.,  600.,  610.,  620.,  630.,  640.,  650.,	660.,  670.,  680.,  690.,  700.,  710.,  720.,  730.,  740.,  750.,  760.,	770.,  780.,  790.,  800.,  810.,  820.,  830.,  840.,  850.,  860.,  870.,	880.,  890.,  900.,  910.,  920.,  930.,  940.,  950.,  960.,  970.,980.,	990., 1000./
	! data interpol_levels /0., 30., 60., 90., 120., 150., 180., 210., 240., 270., 300., 330., 360., 390., 420., 450., 480., 510., 540., 570., 600., 630., 660., 690., 720., 750., 780., 810., 840., 870., 900., 930., 960., 990./
	! data interpol_levels /0.,	20.,	40.,	60.,	80.,	100.,	120.,	140.,	160.,	180.,	200.,	220.,	240.,	260.,	280.,	300.,	320.,	340.,	360.,	380.,	400.,	420.,	440.,	460.,	480.,	500.,	520.,	540.,	560.,	580.,	600.,	620.,	640.,	660.,	680.,	700.,	720.,	740.,	760.,	780.,	800.,	820.,	840.,	860.,	880.,	900.,	920.,	940.,	960.,	980./
	! data interpol_levels /0.,	40.,	80.,	120.,	160.,	200.,	240.,	280.,	320.,	360.,	400.,	440.,	480.,	520.,	560.,	600.,	640.,	680.,	720.,	760.,	800.,	840.,	880.,	920.,	960.,	1000./
	! data interpol_levels /0.,	50.,	100.,	150.,	200.,	250.,	300.,	350.,	400.,	450.,	500.,	550.,	600.,	650.,	700.,	750.,	800.,	850.,	900.,	950./
	! data interpol_levels /0., 15., 30., 45., 60., 75., 90., 105., 120., 135., 150., 165., 180., 195., 210., 225., 240., 255., 270., 285., 300., 315., 330., 345., 360., 375., 390., 405., 420., 435., 450., 465., 480., 495., 510., 525., 540., 555., 570., 585., 600., 615., 630., 645., 660., 675., 690., 705., 720., 735., 750., 765., 780., 795., 810., 825., 840., 855., 870., 885., 900., 915., 930., 945., 960., 975., 990./
	
	real :: interpol_airtemp_in  (NLONS, NLATS, interpol_NLVLS)
	
	! read file
	call check( nf90_open(INPUT_FILE_NAME, nf90_nowrite, in_ncid) )
	
	call check( nf90_inq_varid(in_ncid, LAT_NAME, lat_varid) )
	call check( nf90_get_var(in_ncid, lat_varid, lats) )
	
	call check( nf90_inq_varid(in_ncid, LON_NAME, lon_varid) )
	call check( nf90_get_var(in_ncid, lon_varid, lons) )
	! convert from 0./360. to -180./180.
	lons = lons - 180.0

	call check( nf90_inq_varid(in_ncid, REC_NAME, rec_varid) )
	call check( nf90_get_var(in_ncid, rec_varid, recs) )
	
	call check( nf90_inq_varid(in_ncid, LVL_NAME, lvl_varid) )
	call check( nf90_get_var(in_ncid, lvl_varid, levels) )
	! convert from hPa to Pa
	interpol_levels = interpol_levels * 100.
	levels = levels * 100.
	
	call check( nf90_inq_varid(in_ncid, AIRTEMP_NAME, airtemp_varid) )
	
	! start and count should be reverse of the netcdf var dimensions
	start = (/ 1, 1, 1, 1 /)
	count = (/ NLONS, NLATS, NLVLS, 1 /)
	
	! # these define the enclosing grid of above station
	bounding_box_left = 0
	bounding_box_right = 0
	bounding_box_top = 0
	bounding_box_bottom = 0
	
	! TODO: put all stations in a loop
	! https://libatoms.github.io/QUIP/dictionary.html
	! https://github.com/mapmeld/fortran-machine/tree/master/flibs-0.9/flibs/src/datastructures
	
	! delhi
	search_for_lat = 28.7041
	search_for_lon = 77.1025
	
	! ! mangalore
	! search_for_lat = 12.9141
	! search_for_lon = 74.8560
	
	! ! test
	! search_for_lat = 30.0
	! search_for_lon = 270.0
	
	! karaikal
	! search_for_lat = 10.9254
	! search_for_lon = 79.8380
	
	! ! nagpur
	! search_for_lat = 21.1458
	! search_for_lon = 79.0882
	
	! ! guwahati
	! search_for_lat = 26.1445
	! search_for_lon = 91.7362
	
	! balloon drifts at most 100km away from its initial position = 1deg grid
	! consider at most 2 adjacent grids
	NO_OF_ADJACENT_GRIDS_TO_SCAN = 2
	
	! restrict reading around the station
	do i = 1, NLONS
		if (search_for_lon .ge. lons(i) .and. search_for_lon .le. lons(i+1)) then
			bounding_box_left = i
			bounding_box_right = i + 1
			exit
		end if
	end do
		
	do i = 1, NLATS
		if (search_for_lat .le. lats(i) .and. search_for_lat .ge. lats(i+1)) then
			bounding_box_bottom = i + 1
			bounding_box_top = i
			exit
		end if
	end do
	
	! # define how much area around the coordinates to scan
	! # lat data is decreasing
	lat_range_lower = bounding_box_bottom + NO_OF_ADJACENT_GRIDS_TO_SCAN
	lat_range_higher = bounding_box_top - NO_OF_ADJACENT_GRIDS_TO_SCAN
	! # lon data is increasing
	lon_range_lower = bounding_box_left - NO_OF_ADJACENT_GRIDS_TO_SCAN
	lon_range_higher = bounding_box_right + NO_OF_ADJACENT_GRIDS_TO_SCAN
	
	! for ad-hoc plots
	CHECK_FOR_REC = 28
	
	CHECK_FOR_LAT_LOWER = 20.0
	CHECK_FOR_LAT_UPPER = 40.0
	
	CHECK_FOR_LON_LOWER = -30.0
	CHECK_FOR_LON_UPPER = 150.0
	
	! for every timestep of netcdf file
	do rec = 1, NRECS
		start(4) = rec
		! print *, "for time step ---------- ", rec/4, mod(rec,4)
		
		! read the airtemp data, one timestep at a time, run ncdump for scale and offset
		call check( nf90_get_var(in_ncid, airtemp_varid, airtemp_in, start = start, count = count) )
		scale_factor = 0.00233578920244321
		add_offset =  254.052993177176
		airtemp_in = airtemp_in * scale_factor + add_offset

		do lon = 1, NLONS
			
			across_all_lons_sum_trp1 = 0.0
			across_all_lons_sum_trp2 = 0.0
			count_for_avg = 0.0
			
			across_all_lons_avg_trp1 = 0.0
			across_all_lons_avg_trp2 = 0.0
			
			do lat = NLATS, 1, -1
				! if lat,lon within desired scan boundary
				! lat fixed: 26-30N avg / 28N
				! lon range: 65-85E
				! if (lat .lt. lat_range_lower .and. lat .gt. lat_range_higher .and. lon .gt. lon_range_lower .and. lon .lt. lon_range_higher) then
				! if (rec == 10 .and. lats(lat) == 28.5 .and. lons(lon) .gt. 65.0 .and. lons(lon) .lt. 85.0) then	
				if (rec == CHECK_FOR_REC .and. lons(lon) .gt. CHECK_FOR_LON_LOWER .and. lons(lon) .lt. CHECK_FOR_LON_UPPER .and. lats(lat) .gt. CHECK_FOR_LAT_LOWER .and. lats(lat) .lt. CHECK_FOR_LAT_UPPER) then
					
					count_for_avg = count_for_avg + 1.0
					
					! interpolate data to 10hPa
					call pwl_value_1d(NLVLS, levels, airtemp_in(lon, lat, :), interpol_NLVLS, interpol_levels, interpol_airtemp_in(lon, lat, :))
					
					! invoke twmo at every grid point, with correct units, and pressure levels
					call twmo(interpol_NLVLS, interpol_airtemp_in(lon, lat, :), interpol_levels, trp_SINGLE, trp_DOUBLE)
					! call twmo(NLVLS, airtemp_in(lon, lat, :), levels, trp_SINGLE, trp_DOUBLE)
					
					trp_SINGLE = trp_SINGLE/100.
					trp_DOUBLE = trp_DOUBLE/100.
					
					across_all_lons_sum_trp1 = across_all_lons_sum_trp1 + trp_SINGLE
					across_all_lons_sum_trp2 = across_all_lons_sum_trp2 + trp_DOUBLE
					
					! tp1(lon, lat) = trp_SINGLE
					! tp2(lon, lat) = trp_DOUBLE
					
					! TODO: write the output in nc, with correct units
					! if (trp_DOUBLE/100.0 .gt. 0.) then
					! print *, rec, lons(lon), lats(lat), trp_SINGLE, trp_DOUBLE
					! end if
					! pause
					
				end if
			end do
			
			! if (rec == CHECK_FOR_REC .and. lats(lat) .gt. CHECK_FOR_LAT_LOWER .and. lats(lat) .lt. CHECK_FOR_LAT_UPPER) then
			! 	across_all_lons_avg_trp1 = across_all_lons_sum_trp1 / count_for_avg 
			! 	across_all_lons_avg_trp2 = across_all_lons_sum_trp2 / count_for_avg
			! 	print *, "avg for lat:", lats(lat), "across all lons - ", across_all_lons_avg_trp1, across_all_lons_avg_trp2
			! end if

			if (rec == CHECK_FOR_REC .and. lons(lon) .gt. CHECK_FOR_LON_LOWER .and. lons(lon) .lt. CHECK_FOR_LON_UPPER) then
				across_all_lons_avg_trp1 = across_all_lons_sum_trp1 / count_for_avg
				across_all_lons_avg_trp2 = across_all_lons_sum_trp2 / count_for_avg
				print *, "avg for lon:", lons(lon), "across all lats - ", across_all_lons_avg_trp1, across_all_lons_avg_trp2
			end if

		end do
	
	! process tp1/tp2 to find average
	! do k = 1

	
	! entire file read
	end do

end program

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! http://www.inscc.utah.edu/~reichler/research/projects/TROPO/tropocode.htm
! Original twmo subroutine modified to check for double tropopause

! TROPOPAUSE DEFINITION
! (1) the first tropopause is defined as the lowest level at which the lapse rate Y (defined as -dT/dz) decreases to 2degCkm-1 or less, provided that the average lapse rate between this level and all higher levels within 2 km does not exceed 2degCkm-1, and (2) if above the first tropopause the average lapse rate between any level and all higher levels within 1 km exceeds 3degCkm-1, then a second tropopause is defined under the same criterion as under (1). The second tropopause may be either within or above the 1 km layer discussed in step 2. 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine twmo(level, t, p, trp_SINGLE, trp_DOUBLE)

	implicit none
	integer, intent(in)                 :: level
	real, intent(in),dimension(level)   :: t, p
	real, intent(out)                   :: trp_SINGLE
	real, intent(out)                   :: trp_DOUBLE
	real, parameter                     :: plimu = 20000
	real, parameter                     :: pliml = 5000
	real, parameter                     :: kap = 0.286
	real, parameter                     :: faktor = -9.81 / 287.0
	real, parameter                     :: deltaz_1km = 1000.0
	real, parameter                     :: deltaz_2km = 2000.0
	real, parameter                     :: deltaz_3km = 3000.0
	real, parameter                     :: ka1 = kap - 1.0
	! TODO: update thresholds to < 2.10
	real, parameter                     :: gamma = -0.002 ! K/m
	real, parameter                     :: gamma_DT = -0.003 ! K/m
	
	logical 							:: DT_flag
	real                                :: pmk, pm, a, b, tm, dtdp, dtdz
	real                                :: pmk_DT, pm_DT, a_DT, b_DT, tm_DT, dtdp_DT, dtdz_DT	
	real                                :: ag, bg, ptph, window_start
	real                                :: pmk0, dtdz0
	real                                :: pmk0_DT, dtdz0_DT	
	real                                :: p1km, p2km, asum, asum2, aquer, aquer2
	real                                :: pmk2, pm2, a2, b2, tm2, dtdp2, dtdz2
	real                                :: a_DT_1, a_DT_2, a_DT_3
	
	real                                :: pmk2_DT, pm2_DT, a2_DT, b2_DT, tm2_DT, dtdp2_DT, dtdz2_DT
	integer                             :: icount, icount2, jj, kk, tt, scan_for_DT_from_this_level
	integer                             :: j
	
	trp_SINGLE = -9900.0                           ! negative means not valid
	trp_DOUBLE = -9900.0                           ! negative means not valid
	DT_flag = .false.
	scan_for_DT_from_this_level = -9999

	do j=level,2,-1
	
		! dt/dz
		pmk = .5 * (p(j-1)**kap+p(j)**kap)
		pm = pmk**(1/kap)
		a = (t(j-1)-t(j))/(p(j-1)**kap-p(j)**kap)
		b = t(j)-(a*p(j)**kap)
		tm = a * pmk + b
		dtdp = a * kap * (pm**ka1)
		dtdz = faktor*dtdp*pm/tm
		
		! print *, "dtdz:", dtdz
		! dt/dz valid?
		! print *, "j:", j, "DT flag:", DT_flag
		if (DT_flag .eqv. .true. .and. j .ge. scan_for_DT_from_this_level) go to 999
		if (j.eq.level)    go to 999     ! no, start level, initialize first
		! if we're searching for DT, then we need to scan starting from bottom of 1km window
		if (dtdz.le.gamma) go to 999     ! no, dt/dz < -2 K/km
		if (pm.gt.plimu)   go to 999     ! no, too low

		! dtdz is valid, calculate tropopause pressure
		! the first tropopause is defined as the lowest level at which the lapse rate Y (defined as -dT/dz) decreases to 2degCkm-1 or less
		if (dtdz0.lt.gamma) then
			ag = (dtdz-dtdz0) / (pmk-pmk0)
			bg = dtdz0 - (ag * pmk0)
			ptph = exp(log((gamma - bg) / ag) / kap)
		else
			ptph = pm
		endif
	
		if (ptph.lt.pliml) go to 999
		if (ptph.gt.plimu) go to 999

		! 2nd test: dtdz above 2 km must not exceed gamma
		! provided that the average lapse rate between this level and all higher levels within 2 km does not exceed 2degCkm-1
		p2km = ptph + (deltaz_2km*(pm/tm)*faktor)    ! p at ptph + 2km
		asum = 0.0                                   ! dtdz above
		icount = 0                                   ! number of levels above
		
		! test until apm < p2km
		do jj=j,2,-1
			
			pmk2 = .5 * (p(jj-1)**kap+p(jj)**kap)    ! p mean ^kappa
			pm2 = pmk2**(1/kap)                      ! p mean
			if(pm2.gt.ptph) go to 110                ! doesn't happen
			if(pm2.lt.p2km) go to 888                ! ptropo is valid
			
			a2 = (t(jj-1)-t(jj))                     ! a
			a2 = a2/(p(jj-1)**kap-p(jj)**kap)
			b2 = t(jj)-(a2*p(jj)**kap)               ! b
			tm2 = a2 * pmk2 + b2                     ! T mean
			dtdp2 = a2 * kap * (pm2**(kap-1))        ! dt/dp
			dtdz2 = faktor*dtdp2*pm2/tm2
			asum = asum+dtdz2
			icount = icount+1
			aquer = asum/float(icount)               ! dt/dz mean
			
			if (aquer.le.gamma) go to 999

		110 continue
		enddo                           ! test next level

	888 continue                        ! ptph is valid
		! tropopause criteria satisfied, save the pressure level
		if (DT_flag .eqv. .true.) then
			trp_DOUBLE = ptph
			! print *, "DT recorded"
			return
		else
			! FIXME: USE NEW VARIABLE NAMES FOR DT SCANNING, THERE MIGHT BE A SUBTLE CONFLICT 
			trp_SINGLE = ptph
			! print *, "first tp found at:", ptph, "j:", j
			! return
			
			! first one recorded, check for second tropopause
			! if above the first tropopause the average lapse rate between __any__ level and all higher levels within 1 km exceeds 3degCkm-1
			! scan requires a sliding window 1km wide, until data is available
			! window_start = ptph
			do tt=j,2,-1
				pmk_DT = .5 * (p(tt-1)**kap+p(tt)**kap)
				pm_DT = pmk_DT**(1/kap)
				a_DT_1 = t(tt-1)-t(tt)
				a_DT_2 = p(tt-1)**kap
				a_DT_3 = p(tt)**kap
				a_DT = (a_DT_1/(a_DT_2 - a_DT_3))
				b_DT = t(tt)-(a*p(tt)**kap)
				tm_DT = a_DT * pmk_DT + b_DT
				dtdp_DT = a_DT * kap * (pm_DT**ka1)
				dtdz_DT = faktor*dtdp_DT*pm_DT/tm_DT
				! print *,"a_DT_1:", a_DT_1
				! print *,"a_DT_2:", a_DT_2 				
				! print *,"a_DT_3:", a_DT_3 				
				! print *,"a_DT:", a_DT 								 				
				! print *,"pmk_DT:", pmk_DT 
				! print *,"t(tt):", t(tt), "t(tt-1):", t(tt-1)
				! print *,"p(tt):", p(tt), "p(tt-1):", p(tt-1)
				! print *, "kap:", kap
				! print *,"pm_DT:", pm_DT, "tm_DT", tm_DT
				! print *, "dtdz_DT:", dtdz_DT, "dtdp_DT:", dtdp_DT

				p1km = pm_DT + (deltaz_1km*(pm_DT/tm_DT)*faktor)     ! p at ptph + 1km
				
				asum2 = 0.0                                   ! dtdz above
				icount2 = 0                                   ! number of levels above
				! print *, "---------------------------"
				! print *, "sliding window range:", pm_DT, p1km
				! FIXME: sliding window must obey pliml and plimu
				! FIXME: there's a gap while shifting the sliding window, check which variables you are printing
				! test until apm < p1km
				do kk=tt,2,-1
					pmk2_DT = .5 * (p(kk-1)**kap+p(kk)**kap)    ! p mean ^kappa
					pm2_DT = pmk2_DT**(1/kap)                      ! p mean
					! if(kk .eq. tt) go to 220	! skip first iteration
					! if(pm2_DT.gt.ptph) go to 220                ! doesn't happen
					if(pm2_DT.lt.p1km) then
						! then a second tropopause is defined under the same criterion as under (1).
						if (aquer2 .le. gamma_DT) then
							DT_flag = .true.
							scan_for_DT_from_this_level = tt
							! print *, "final avg2:", aquer2
							! print *, "flag set, window scan start:", scan_for_DT_from_this_level
							go to 999
						else
							! print *, pm2_DT
							! print *, "level outside sliding window, with avg < gamma, try next sliding window"
							go to 777
						end if
					end if
		
					a2_DT = (t(kk-1) - t(kk))                     ! a
					a2_DT = a2_DT / (p(kk-1)**kap - p(kk)**kap)
					b2_DT = t(kk) - (a2_DT*p(kk)**kap)               ! b
					tm2_DT = a2_DT * pmk2_DT + b2_DT                     ! T mean
					dtdp2_DT = a2_DT * kap * (pm2_DT**(kap-1))        ! dt/dp
					dtdz2_DT = faktor * dtdp2_DT * pm2_DT/tm2_DT
					asum2 = asum2 + dtdz2_DT
					icount2 = icount2 + 1
					aquer2 = asum2 / float(icount2)               ! dt/dz mean
					! print *, "pm2_DT", pm2_DT 
					! print *, "avg2:", aquer2
					! print *, "icount2:", icount2
			
			220 continue
				enddo                           ! test next level

				! update for next iteration of sliding window
				! ?
			777	continue	
			end do

			! print *, "sliding window exhausted"
			! no instance of sliding window could find avg > 3, exit with only single tropopause recorded? 
			! trp_DOUBLE = -9900.0
			return
		end if

		! return
	999 continue                        ! continue search at next higher level
		pmk0 = pmk
		dtdz0 = dtdz

	enddo 

	! no tropopouse found
	return

end subroutine twmo

subroutine check(status)
	use netcdf
	implicit none
	integer, intent ( in) :: status
	if(status /= nf90_noerr) then 
		print *, trim(nf90_strerror(status))
		stop "Stopped"
	end if
end subroutine check

! ALGORITHM TO FIND SINGLE (ST) AND DOUBLE TROPOPAUSE (DT):
! ------------------------------------------------

! loop over all levels, starting with the lowest
! 	calculate dtdz from dtdp
! 	if dtdz is below 2K/km
! 		i -> current level
! 		j -> i + 2km
! 		loop from i to j
! 			add every lapse rate -> sum
! 			track count of levels -> iters
! 		avg lapse rate for the 2km window -> sum/iters
! 		if avg < 2K/km 
! 			if DT flag set
! 				save DT
! 				exit subroutine with ST and DT
! 			else
! 				save ST and check if DT is possible

! 				ii -> ST level
! 				while ii level exists in data
! 					jj -> ii + 1km above
! 					loop from ii to jj
! 						add every lapse rate -> sum
! 						track count of levels -> iters
! 					avg lapse rate for the window -> sum/iters
! 					if avg > 3K/km 
! 						DT is possible -> set DT flag
! 						go back to the ST loop with level = ii
! 					else
! 						increment ii to one level above for next iteration
! 				no DT found, return with only ST
! 		no ST found, return with neither ST nor DT

! TROPOPAUSE DEFINITION
! (1) the first tropopause is defined as the lowest level at which the lapse rate Y (defined as -dT/dz) decreases to 2degCkm-1 or less, provided that the average lapse rate between this level and all higher levels within 2 km does not exceed 2degCkm-1, and (2) if above the first tropopause the average lapse rate between any level and all higher levels within 1 km exceeds 3degCkm-1, then a second tropopause is defined under the same criterion as under (1). The second tropopause may be either within or above the 1 km layer discussed in step 2. 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
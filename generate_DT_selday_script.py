# -*- coding: utf-8 -*-
from __future__ import division

import os.path
import time
import numpy as np
import pandas as pd

station_list = [
	"Delhi",
	"Nagpur",
	"Guwahati",
	"Mangalore",
	"Karaikal"
	]

months_list = [
	"Aug",
	"Dec"
	]

for station_name in station_list:
	for month in months_list:
		DIRECTORY_PATH = "../data/computed_tropopause_data_NEW/" + station_name.lower() + "_all_data.csv"
		df = pd.read_csv(DIRECTORY_PATH)
		df.dropna(inplace=True)

		output_file_name = "./Neeraj_scr_Anom_DTday_" + month + "_" + station_name + ".sh"
		out_fp = open(output_file_name, "w")

		for year in range(2005, 2018):
			year = str(year)
			filtered_days_df = df.loc[df['dates'].str.contains(month + " " + year)]	
			
			days_list = [x.split(' ')[1] for x in filtered_days_df["dates"].tolist()]

			if not days_list:
				continue

			unique_days = sorted(list(set([int(x) for x in days_list])))
			unique_days = [str(x) for x in unique_days]

			days = ""
			for day in unique_days:
				days += day + ","
			days = days[:-1]
			
			# cdo -selday,5,6,13,14,15,20,21,25,30,31 dailym_SH_Dec_1979.nc Neeraj_Delhi_DTDec1979.nc
			command = "cdo -selday," + days + " ../dailym_SH_" + month + "_" + year + ".nc Neeraj_" + station_name + "_DT" + month + year + ".nc\n"
			
			# cdo timmean Neeraj_Delhi_DTDec1979.nc Neeraj_timmean_Delhi_DTDec1979.nc
			# command = "cdo timmean Neeraj_" + station_name + "_DT" + month + year + ".nc\n"

			# command = "cdo -selday," + days + " dif_" + month + year + ".nc Anom_" + station_name + "_DT" + month + year + ".nc\n"
			
			out_fp.write(command)

		out_fp.close()
		print "done: ", month, station_name
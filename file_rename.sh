#! /bin/bash

for entry in `ls *.xlsx`
do
    nc_string="calibrated.xlsx"
    extracted_string=${entry:0:24}
    final_filename=$extracted_string$nc_string
    # echo "$final_filename"
    mv $entry $final_filename
done

for entry in `ls *.nc`
do
    nc_string="calibrated.xlsx"
    extracted_string=${entry:0:24}
    final_filename=$extracted_string$nc_string
    # echo "$final_filename"
    mv $entry $final_filename
done

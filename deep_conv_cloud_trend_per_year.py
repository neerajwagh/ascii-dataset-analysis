# -*- coding: utf-8 -*-
from __future__ import division
from glob2 import glob
from netCDF4 import Dataset as NetCDFFile
import pandas as pd
import numpy as np
import os.path
import time

# https://stackoverflow.com/questions/582336/how-can-you-profile-a-script
# FIXME: too slow! takes 30s for one row of the csv. TOO DAMN SLOW! Bottleneck?
# https://stackoverflow.com/questions/20548628/how-to-do-parallel-programming-in-python
# FIXME: parallelize scans!

DIRECTORY_PATH = "../data/original/AIRS"
list_of_files = glob(DIRECTORY_PATH + "/*.nc4")
# if % of NA grids among total grids scanned is greater than 65, report as NA
NA_PERCENT_TOLERANCE = 65
# = 2 means a grid of 5x5 cells with the station/city at the centre cell will be scanned
# since AIRS data is at 1 degree resolution, we're scanning 2 degrees away from station = 200km, in all directions
NO_OF_ADJACENT_GRIDS_TO_SCAN = 2
# temperature threshold to check for presence of deep convective cloud
TEMPERATURE_THRESHOLD = 258.0

COORDINATES_MAP = {
	'43285': {
		"name": "mangalore",
		"lat": 12.9141,
		"lon": 74.8560
	},
	# vidd
	'42182': {
		"name": "delhi",
		"lat": 28.7041,
		"lon": 77.1025
	},

	'43346': {
		"name": "karaikal",
		"lat": 10.9254,
		"lon": 79.8380
	},
	# vanp
	'42867': {
		"name": "nagpur",
		"lat": 21.1458,
		"lon": 79.0882
	},
	# vegt
	'42410': {
		"name": "guahati",
		"lat": 26.1445,
		"lon": 91.7362
	}
}

# fetch station coordinates
search_for_lat = COORDINATES_MAP["43346"]["lat"]
search_for_lon = COORDINATES_MAP["43346"]["lon"]

# build yearly map
YEARLY_COUNT_MAP = {}
for year in range(2003, 2018):
	year = str(year)
	YEARLY_COUNT_MAP[year] = 0

rows_list = []
for file in list_of_files:

	# deconstruct AIRS filename to get date
	# example - AIRS.2017.12.28.L3.RetStd_IR001.v6.0.31.0.G17363143748.hdf.nc4
	day = file.split('/')[-1].split('.')[3]
	month = file.split('/')[-1].split('.')[2]
	year = file.split('/')[-1].split('.')[1]
	
	# skip years before 2003
	if int(year) < 2003:
		continue

	temp = {}
	temp["st. no."] = "43346"
	temp["dates"] = day + " " + month + " " + year

	# open and query
	nc = NetCDFFile(file)

	# variables that are same for all files, read only once.
	if file == list_of_files[0]:
		lat = nc.variables['Latitude'][:]
		lon = nc.variables['Longitude'][:]
		lon_list = lon.tolist()
		lat_list = lat.tolist()
		
		# these define the enclosing grid of above station
		bounding_box_left = 0
		bounding_box_right = 0
		bounding_box_top = 0
		bounding_box_bottom = 0

		for index, i in enumerate(lon_list):
			if search_for_lon > i and search_for_lon < lon_list[index+1]:
				bounding_box_left = index
				bounding_box_right = index + 1
				break

		for index, i in enumerate(lat_list):
			if search_for_lat < i and search_for_lat > lat_list[index+1]:
				bounding_box_bottom = index + 1
				bounding_box_top = index
				break

		# define how much area around the coordinates to scan
		# lat data is decreasing
		lat_range_lower = bounding_box_bottom + NO_OF_ADJACENT_GRIDS_TO_SCAN
		lat_range_higher = bounding_box_top - NO_OF_ADJACENT_GRIDS_TO_SCAN
		# lon data is increasing 
		lon_range_lower = bounding_box_left - NO_OF_ADJACENT_GRIDS_TO_SCAN
		lon_range_higher = bounding_box_right + NO_OF_ADJACENT_GRIDS_TO_SCAN

	# variables that change for every file
	cloud_top_temp_ascending = nc.variables['CloudTopTemp_A'][:]
	cloud_top_temp_descending = nc.variables['CloudTopTemp_D'][:]
	
	# scan cloud_top_temp grid for NAs
	na_count = 0
	grid_count = 0
	for i in range(lat_range_higher, lat_range_lower + 1):
		for j in range(lon_range_lower, lon_range_higher + 1):
			grid_count = grid_count + 1
			if cloud_top_temp_ascending[i,j] == cloud_top_temp_ascending.fill_value or cloud_top_temp_descending[i,j] == cloud_top_temp_descending.fill_value:
				na_count = na_count + 1
	# if % of NA occurences in scanned grid are higher than 65, report day as NA, otherwise continue 
	if ((na_count/grid_count)*100 < NA_PERCENT_TOLERANCE):
		temp["cloud_present"] = 0
		for i in range(lat_range_higher, lat_range_lower + 1):
			for j in range(lon_range_lower, lon_range_higher + 1):
				if cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD or cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD:				
					# report presence of cloud
					temp["cloud_present"] = 1
					YEARLY_COUNT_MAP[year] += 1
					# no need to check every cell within 5x5 grid
					break
	# not enough data in file
	else:
		temp["cloud_present"] = "NA"

	rows_list.append(temp)
	print "dcc check complete: ", day, month, year

print "all years check complete"

# write rows_list to csv
# FIXME: daily not in order
out_df = pd.DataFrame(rows_list, columns=["st. no.", "dates", "cloud_present"])
out_df.to_csv("daily_dcc_check.csv", index=False, header=True)
print "dates written to file"

# write yearly counts
rows_list = []
for year in YEARLY_COUNT_MAP.keys():
	temp = {}
	temp["year"] = int(year)
	temp["total_counts"] = int(YEARLY_COUNT_MAP[year])
	rows_list.append(temp)

out_df = pd.DataFrame(rows_list, columns=["year", "total_counts"])
out_df.sort_values(by="year", ascending=True, inplace=True)
out_df.to_csv("dcc_yearly_counts.csv", index=False, header=True)
print "yearly counts to file"
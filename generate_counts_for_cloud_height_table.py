from __future__ import division

import numpy as np
import pandas as pd
from glob2 import glob

SEASON_MAP = {
	"winter": {
		"months": ["Dec", "Jan", "Feb"]
	},
	"pre-monsoon": {
		"months": ["Mar", "Apr", "May"]
	},
	"monsoon": {
		"months": ["Jun", "Jul", "Aug", "Sep"]
	},
	"post-monsoon": {
		"months": ["Oct", "Nov"]
	}
}

TOTAL_DT_DAYS = {
	"Jan" : 0,
	"Feb" : 0,
	"Mar" : 0,
	"Apr" : 0,
	"May" : 0,
	"Jun" : 0,
	"Jul" : 0,
	"Aug" : 0,
	"Sep" : 0,
	"Oct" : 0,
	"Nov" : 0,
	"Dec" : 0
}

BELOW_LRT1 = {
	"Jan" : 0,
	"Feb" : 0,
	"Mar" : 0,
	"Apr" : 0,
	"May" : 0,
	"Jun" : 0,
	"Jul" : 0,
	"Aug" : 0,
	"Sep" : 0,
	"Oct" : 0,
	"Nov" : 0,
	"Dec" : 0
}

NEAR_LRT1 = {
	"Jan" : 0,
	"Feb" : 0,
	"Mar" : 0,
	"Apr" : 0,
	"May" : 0,
	"Jun" : 0,
	"Jul" : 0,
	"Aug" : 0,
	"Sep" : 0,
	"Oct" : 0,
	"Nov" : 0,
	"Dec" : 0
}

ABOVE_LRT1 = {
	"Jan" : 0,
	"Feb" : 0,
	"Mar" : 0,
	"Apr" : 0,
	"May" : 0,
	"Jun" : 0,
	"Jul" : 0,
	"Aug" : 0,
	"Sep" : 0,
	"Oct" : 0,
	"Nov" : 0,
	"Dec" : 0
}

# use only the 5x5 grid columns, drop rows that have "na_file"
station_name = "nagpur"
PATH = "./../data/computed_tropopause_data_NEW/" + station_name + "_all_data_cloudtop_check.csv"
df = pd.read_csv(PATH, na_values=["na_file", "na_data"])
df.dropna(inplace=True)

PRES_THRESHOLD = 30.0

INTERPOLATE_EVERY_X_METRES = 100
heights = np.arange(10000, 27000, INTERPOLATE_EVERY_X_METRES)

for row in df[1:].itertuples():

	DT_day = getattr(row, "dates")
	year = DT_day.split(' ')[-1]
	month = DT_day.split(' ')[2]
	day = DT_day.split(' ')[1]

	# enable for testing
	# if int(day) == 31 and month == "Dec" and int(year) == 2009:
	# 	pass
	# else:
	# 	continue
	
	print "current day: ", DT_day
	ct_pres = float(getattr(row, "min_cloud_top_pressure_5x5grid"))
	lrt1_height = float(getattr(row, "lrt1"))
	lrt2_height = float(getattr(row, "lrt2"))

	# open raw data file which has current date
	DIR = "./../data/Wyoming_Balloon_Data_csv_only/" + station_name
	my_cols = ["PRES","HGHT","TEMP","DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"]
	raw_data_df = pd.read_csv(glob(DIR + "/*_" + year + ".csv")[0], names=my_cols)
	raw_data_df.drop(["DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"], axis = 1, inplace = True)
	incomplete_data = raw_data_df['TEMP'].isnull()
	separator_row = raw_data_df['PRES'] > 1500
	raw_data_df.drop(raw_data_df.index[incomplete_data & ~separator_row], inplace=True)
	raw_data_df.reset_index(drop=True, inplace=True)

	# obtain the indices for splitting
	separator_row = raw_data_df["TEMP"].isnull()
	indices_for_split = np.array(raw_data_df.index[separator_row]).tolist()

	# check at these indices which day it is
	for iter_index, index in enumerate(indices_for_split):
		if raw_data_df.loc[index]["HGHT"] == DT_day:
			try:
				subset = raw_data_df.loc[index:indices_for_split[iter_index+1]-1][:]
			except IndexError:
				# last subset in raw file
				subset = raw_data_df.loc[index:][:]

	# reset indices to find separator at index 0 always
	subset.reset_index(drop=True, inplace=True)
		
	# remove the first row from subset, set dataframe as float again
	subset.drop(subset.index[0], inplace=True)
	subset = subset.astype(np.float32)

	# find max recorded height in data, do not consider heights above this
	# if (subset['HGHT'].empty):
	# 	print "subset empty!"
	max_hgt_in_subset = float(np.array(subset['HGHT'], dtype=np.float64).max())
	
	# handle the data
	hgt_list = []
	avg_pres_list = []

	for index, hgt in enumerate(heights[:-1]):
		if max_hgt_in_subset > hgt:
			height_range = subset['HGHT'].between(hgt, heights[index+1], inclusive=False)
			hgt_list.append(hgt)
			avg_pres_list.append(subset[height_range]['PRES'].mean())

	# interpolate to remove NaNs in avg_pres, restrain decimal digits
	avg_pres_list = pd.Series(avg_pres_list).interpolate(method='linear').tolist()
	
	# find interp raw pres corresponding to LRT1
	raw_lrt1_pres = avg_pres_list[hgt_list.index(lrt1_height)]
	raw_lrt2_pres = avg_pres_list[hgt_list.index(lrt2_height)]	
	
	print "lrt1: ", lrt1_height
	print "lrt2: ", lrt2_height	
	print "raw_tp1: ", raw_lrt1_pres
	print "ct_pres: ", ct_pres
	print "---------------------"

	# between LRT1 and LRT2
	if ct_pres < raw_lrt1_pres and ct_pres > raw_lrt2_pres:
		ABOVE_LRT1[month] += 1
	
	# between LRT1 and LRT1+30
	if ct_pres < (raw_lrt1_pres + PRES_THRESHOLD) and ct_pres > raw_lrt1_pres:
		NEAR_LRT1[month] += 1

	# between LRT1+30 and 400 - UTLS
	if ct_pres > (raw_lrt1_pres + PRES_THRESHOLD) and ct_pres < 400:
		BELOW_LRT1[month] += 1

	TOTAL_DT_DAYS[month] += 1

# aggregate monthly counts according to seasons
for i in SEASON_MAP.keys():
	list_of_months_in_season = SEASON_MAP[i]["months"]
	
	below_lrt1_seasonal_count = 0
	for month in list_of_months_in_season:
		below_lrt1_seasonal_count += BELOW_LRT1[month]
	
	near_lrt1_seasonal_count = 0
	for month in list_of_months_in_season:
		near_lrt1_seasonal_count += NEAR_LRT1[month]
	
	above_lrt1_seasonal_count = 0
	for month in list_of_months_in_season:
		above_lrt1_seasonal_count += ABOVE_LRT1[month]
	
	dt_days_seasonal_count = 0
	for month in list_of_months_in_season:
		dt_days_seasonal_count += TOTAL_DT_DAYS[month]

	print "\n\nfor season: ", i
	print "--------------------"
	print "below_lrt1: ", below_lrt1_seasonal_count/dt_days_seasonal_count*100
	print "near_lrt1: ", near_lrt1_seasonal_count/dt_days_seasonal_count*100
	print "above_lrt1 (computed as b/w lrt1 and lrt2): ", above_lrt1_seasonal_count/dt_days_seasonal_count*100
	print "total_dt_days: ", dt_days_seasonal_count
	print "--------------------"
	
import numpy as np
import pandas as pd
from glob2 import glob

# DIRECTORY = "../data/computed_tropopause_data_NEW/*_all_data_cloudtop_check_NEW.csv"
DIRECTORY = "../data/computed_tropopause_data_ST/*_all_ST_data_cloudtop_check_NEW.csv"

# https://stackoverflow.com/questions/6773584/how-is-pythons-glob-glob-ordered
files = sorted(glob(DIRECTORY))
MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

pressure_range = [
"80 - 90",
"90 - 100",
"100 - 150",
"150 - 200",
"200 - 250",
"250 - 300",
"300 - 350",
"350 - 400"
# "400 - 450",
# "450 - 500",
# "500 - 550"
]

SEASON_MAP = {
	"winter": {
		"months": ["Dec", "Jan", "Feb"]
	},
	"pre-monsoon": {
		"months": ["Mar", "Apr", "May"]
	},
	"monsoon": {
		"months": ["Jun", "Jul", "Aug", "Sep"]
	},
	"post-monsoon": {
		"months": ["Oct", "Nov"]
	}
}

# for every station
for f in files:
	filename = f.split('/')[-1].split('_cloudtop')[0]
	df = pd.read_csv(f, na_values=["na_file", "na_data"])
	df.dropna(inplace=True)
	
	rows_list = []
	for window in pressure_range:
		# if window is 90 - 100, lower = 90, upper = 100
		lower = float(window.split(' - ')[0])
		upper = float(window.split(' - ')[1])

		temp = {}
		temp["Range"] = window
		for month in MONTHS:
			# select rows with only a particular month
			temp_df = df.loc[df['dates'].str.contains(month)]
			# (100 included in 90-100 range)
			bool_mask_lower = temp_df.min_cloud_top_pressure_3x3grid > lower
			bool_mask_upper = temp_df.min_cloud_top_pressure_3x3grid <= upper
			bool_mask_ctt = temp_df.min_cloud_top_temp_3x3grid < 235.00
			temp[month] = temp_df.loc[bool_mask_lower & bool_mask_upper & bool_mask_ctt].shape[0]

		# write seasonal sums
		for season in SEASON_MAP.keys():
			season_months = SEASON_MAP[season]["months"]
			seasonal_total = 0
			for month in season_months:
				seasonal_total = seasonal_total + temp[month]
			temp[season] = seasonal_total

		# write yearly sum
		yearly_total = 0
		for month in MONTHS:
			yearly_total = yearly_total + temp[month]
		temp["yearly_total"] = yearly_total
		
		rows_list.append(temp)

	# generate year_df from months, save
	year_df = pd.DataFrame(rows_list, columns=["Range"] + MONTHS + SEASON_MAP.keys() + ["yearly_total"])
	year_df.to_csv("../data/computed_tropopause_data_ST/" + filename + "_cloudtop_analysis_NEW.csv", index=False, header=True)
	print "station done: ", f
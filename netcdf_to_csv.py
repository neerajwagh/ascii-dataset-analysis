# -*- coding: utf-8 -*-
from __future__ import division

import os.path
import time

import numpy as np
import pandas as pd
from dateutil import parser
from glob2 import glob
from netCDF4 import Dataset as NetCDFFile
from netCDF4 import date2index

# = 2 means a grid of 5x5 cells with the station/city in the centre cell
# since AIRS data is at 1 degree resolution, we're scanning 5 degrees away from station = 500km, in all directions

# # for 200km
# NO_OF_ADJACENT_GRIDS_TO_SCAN_AIRS = 2
# NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD = 8
# NO_OF_ADJACENT_GRIDS_TO_SCAN_TRMM = 8

# for 500km
NO_OF_ADJACENT_GRIDS_TO_SCAN_AIRS = 5
NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD = 20
NO_OF_ADJACENT_GRIDS_TO_SCAN_TRMM = 20

out_columns=[
"st. no.", "dates",
"max_rainfall_500km_radius_IMD",
"max_rainfall_500km_radius_TRMM"
]

MONTH_MAP = {
	'Jan' : '01',
	'Feb' : '02',
	'Mar' : '03',
	'Apr' : '04',
	'May' : '05',
	'Jun' : '06',
	'Jul' : '07',
	'Aug' : '08',
	'Sep' : '09',
	'Oct' : '10',
	'Nov' : '11',
	'Dec' : '12'
}

# station coordinates
search_for_lat = 29.3803
search_for_lon = 79.4636

rows_list = []
# for every day of the month
for day in range(1,32):
	
	temp = {}
	temp["st. no."] = "nainital"
	temp["dates"] = str(day).zfill(2) + "-08-2016"

	# open IMD data, write it's max rainfall
	nc = NetCDFFile("./../data/original/IMD/rain_imd_2016.nc")
	
	lat = nc.variables['lat'][:]
	lon = nc.variables['lon'][:]
	lon_list = lon.tolist()
	lat_list = lat.tolist()
	time = nc.variables['time']
	rainfall = nc.variables['rf'][:]
	
	# these define the enclosing grid of above station
	bounding_box_left = 0
	bounding_box_right = 0
	bounding_box_top = 0
	bounding_box_bottom = 0

	for index, i in enumerate(lon_list):
		if search_for_lon > i and search_for_lon < lon_list[index+1]:
			bounding_box_left = index
			bounding_box_right = index + 1
			break

	for index, i in enumerate(lat_list):
		if search_for_lat > i and search_for_lat < lat_list[index+1]:
			bounding_box_bottom = index
			bounding_box_top = index + 1
			break

	# define how much area around the coordinates to scan
	# lat data is increasing
	lat_range_lower = bounding_box_bottom - NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD
	lat_range_higher = bounding_box_top + NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD
	# lon data is increasing 
	lon_range_lower = bounding_box_left - NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD
	lon_range_higher = bounding_box_right + NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD

	formatted_date = "2016" + "-" + "08" + "-" + str(day).zfill(2)
	time_index = date2index(parser.parse(formatted_date), time)
	max_rainfall = rainfall[time_index, lat_range_lower:lat_range_higher+1, lon_range_lower:lon_range_higher+1].max()

	if type(max_rainfall) is np.float32:
		temp["max_rainfall_500km_radius_IMD"] = max_rainfall
	else:
		# FIXME: interpolate nan values?
		temp["max_rainfall_500km_radius_IMD"] = "na_data"

	# construct corresponding TRMM filenames - 8 TRMM files per day
	trmm_filename_pattern = "3B42RT." + "2016" + "08" + str(day).zfill(2)
	trmm_available_files = glob("./../data/original/TRMM_3Hourly_Nainital_Aug2016/" + trmm_filename_pattern + "*.nc4")
	global_max_rainfall_found = -999.0
	# SEE NCDUMP OF TRMM FILE
	slice_around_station = np.zeros(shape=(81,81))

	# search every TRMM 3-hourly file for max rainfall
	for trmm_file in trmm_available_files:
		nc_trmm = NetCDFFile(trmm_file)
		
		lat_trmm = nc_trmm.variables['lat'][:]
		lon_trmm = nc_trmm.variables['lon'][:]
		lon_list_trmm = lon_trmm.tolist()
		lat_list_trmm = lat_trmm.tolist()
		
		precipitation = nc_trmm.variables['precipitation'][:]

		bounding_box_left_trmm = 0
		bounding_box_right_trmm = 0
		bounding_box_top_trmm = 0
		bounding_box_bottom_trmm = 0
		
		for index, i in enumerate(lon_list_trmm):
			if i < search_for_lon and search_for_lon < lon_list_trmm[index+1]:
				bounding_box_left_trmm = index
				bounding_box_right_trmm = index + 1
				break

		for index, i in enumerate(lat_list_trmm):
			if i < search_for_lat and search_for_lat < lat_list_trmm[index+1]:
				bounding_box_bottom_trmm = index
				bounding_box_top_trmm = index + 1
				break
		
		# define how much area around the coordinates to scan
		# TRMM data is 0.25 deg resolution, so multiply by 4 to follow global scan scale
		# lat data is increasing
		lat_range_lower_trmm = bounding_box_bottom_trmm - NO_OF_ADJACENT_GRIDS_TO_SCAN_TRMM
		lat_range_higher_trmm = bounding_box_top_trmm + NO_OF_ADJACENT_GRIDS_TO_SCAN_TRMM
		# lon data is increasing
		lon_range_lower_trmm = bounding_box_left_trmm - NO_OF_ADJACENT_GRIDS_TO_SCAN_TRMM
		lon_range_higher_trmm = bounding_box_right_trmm + NO_OF_ADJACENT_GRIDS_TO_SCAN_TRMM

		# for all cells within grid, find *3, and sum for all intervals within day
		for i in range(lat_range_lower_trmm, lat_range_higher_trmm+1):
			for j in range(lon_range_lower_trmm, lon_range_higher_trmm+1):
				if type(precipitation[i,j]) == np.float32:
					slice_around_station[i,j] += (precipitation[i,j] * 3.0)
				# else:
				# 	# FIXME: interpolate and then consider!

	temp["max_rainfall_500km_radius_TRMM"] = slice_around_station.max()

	rows_list.append(temp)
	print "day processed:", str(day)
	print "---------------------------------------------------"

# write rows_list to csv
out_df = pd.DataFrame(rows_list, columns=out_columns)
# print "% of cloud presence in available data: ", out_df["cloud_present"].value_counts(1)['1']
out_df.to_csv("../data/computed_tropopause_data/Nainital_TRMM_Aug2016.csv", index=False, header=True)
print "done!"
# -*- coding: utf-8 -*-
from __future__ import division

import os.path
import time

import numpy as np
import pandas as pd
from glob2 import glob
from netCDF4 import Dataset as NetCDFFile

# # https://github.com/ray-project/ray
# import ray
# # https://blog.dominodatalab.com/simple-parallelization/
# from joblib import Parallel, delayed

# https://stackoverflow.com/questions/582336/how-can-you-profile-a-script
# FIXME: too slow! takes 30s for one row of the csv. TOO DAMN SLOW! Bottleneck?
# https://stackoverflow.com/questions/20548628/how-to-do-parallel-programming-in-python
# FIXME: parallelize scans!

DIRECTORY_PATH = "../data/copied_dir"
# if % of NA grids among total grids scanned is greater than 65, report as NA
NA_PERCENT_TOLERANCE = 65
# = 2 means a grid of 5x5 cells with the station/city at the centre cell will be scanned
# since AIRS data is at 1 degree resolution, we're scanning 2 degrees away from station = 200km, in all directions
NO_OF_ADJACENT_GRIDS_TO_SCAN = 2
# temperature threshold to check for presence of cloud
TEMPERATURE_THRESHOLD = 258.0
# if greater, Rossby wave breaking confirmed
PV_THRESHOLD = 2.0

# T < 220 -> very deep conv
# 220 < T < 235 -> deep conv
# 235 < T < 255 -> bkg convective cloudiness

COORDINATES_MAP = {
	'43285': {
		"name": "mangalore",
		"lat": 12.9141,
		"lon": 74.8560
	},
	# vidd
	'42182': {
		"name": "delhi",
		"lat": 28.7041,
		"lon": 77.1025
	},

	'43346': {
		"name": "karaikal",
		"lat": 10.9254,
		"lon": 79.8380
	},
	# vanp
	'42867': {
		"name": "nagpur",
		"lat": 21.1458,
		"lon": 79.0882
	},
	# vegt
	'42410': {
		"name": "guahati",
		"lat": 26.1445,
		"lon": 91.7362
	}
}

MONTH_MAP = {
	'Jan' : '01',
	'Feb' : '02',
	'Mar' : '03',
	'Apr' : '04',
	'May' : '05',
	'Jun' : '06',
	'Jul' : '07',
	'Aug' : '08',
	'Sep' : '09',
	'Oct' : '10',
	'Nov' : '11',
	'Dec' : '12'
}

list_of_files = glob(DIRECTORY_PATH + "/*.xlsx")
my_cols = ["st. no.", "dates"]

for file in list_of_files:

	df = pd.ExcelFile(file, names=my_cols).parse('All_data')
	STATION_ID = str(int(df['st. no.'][0]))
	dates_list = [str(x) for x in df["dates"].tolist()]

	# fetch station coordinates
	search_for_lat = COORDINATES_MAP[STATION_ID]["lat"]
	search_for_lon = COORDINATES_MAP[STATION_ID]["lon"]
	
	# Parallel(n_jobs=6)(delayed(initialize_check_for_DT)(year) for date in dates_list)
	# print "all DT days processed for file: ", file

	rows_list = []
	for date in dates_list:

		# since AIRS data is from 2003 onwards, skip tropopause dates before it
		if int(date.strip().split(' ')[-1]) < 2003:
			continue

		temp = {}
		temp["st. no."] = STATION_ID
		temp["dates"] = date

		# construct corresponding AIRS filename
		airs_filename_pattern = "AIRS." + date.split(' ')[-1] + "." + MONTH_MAP[date.split(' ')[2]] + "." + date.split(' ')[1]
		airs_available_files = [x for x in glob("./../data/original/AIRS/" + airs_filename_pattern + "*.nc4")]

		if airs_available_files:

			print "loop start: ", time.ctime()
			print "####################################"
			
			# open nc4 file and query
			nc = NetCDFFile(airs_available_files[0])
			
			lat = nc.variables['Latitude'][:]
			lon = nc.variables['Longitude'][:]
			lon_list = lon.tolist()
			lat_list = lat.tolist()
			
			cloud_top_temp_ascending = nc.variables['CloudTopTemp_A'][:]
			cloud_top_temp_descending = nc.variables['CloudTopTemp_D'][:]
			
			cloud_top_pres_ascending = nc.variables['CloudTopPres_A'][:]
			cloud_top_pres_descending = nc.variables['CloudTopPres_D'][:]

			# these define the enclosing grid of above station
			bounding_box_left = 0
			bounding_box_right = 0
			bounding_box_top = 0
			bounding_box_bottom = 0

			for index, i in enumerate(lon_list):
				if search_for_lon > i and search_for_lon < lon_list[index+1]:
					bounding_box_left = index
					bounding_box_right = index + 1
					break

			for index, i in enumerate(lat_list):
				if search_for_lat < i and search_for_lat > lat_list[index+1]:
					bounding_box_bottom = index + 1
					bounding_box_top = index
					break

			# define how much area around the coordinates to scan
			# lat data is decreasing
			lat_range_lower = bounding_box_bottom + NO_OF_ADJACENT_GRIDS_TO_SCAN
			lat_range_higher = bounding_box_top - NO_OF_ADJACENT_GRIDS_TO_SCAN
			# lon data is increasing 
			lon_range_lower = bounding_box_left - NO_OF_ADJACENT_GRIDS_TO_SCAN
			lon_range_higher = bounding_box_right + NO_OF_ADJACENT_GRIDS_TO_SCAN

			# scan cloud_top_temp grid for NAs
			na_count = 0
			grid_count = 0
			for i in range(lat_range_higher, lat_range_lower + 1):
				for j in range(lon_range_lower, lon_range_higher + 1):
					grid_count = grid_count + 1
					if cloud_top_temp_ascending[i,j] == cloud_top_temp_ascending.fill_value or cloud_top_temp_descending[i,j] == cloud_top_temp_descending.fill_value:
						na_count = na_count + 1

			# if % of NA occurences in scanned grid are higher than 65, report day as NA, otherwise continue 
			if ((na_count/grid_count)*100 < NA_PERCENT_TOLERANCE):

				temp["cloud_present"] = 0
				temp["min_cloud_top_pressure"] = "NA"
				temp["max_rainfall"] = "NA"
				temp["pv>2_at_200hPa"] = "NA"
				temp["pv>2_at_250hPa"] = "NA"

				# FIXME: make scan for all vars independent of each other
				# right now, all checks are depending on cloud_present to succeed.
				for i in range(lat_range_higher, lat_range_lower + 1):
					for j in range(lon_range_lower, lon_range_higher + 1):
						if cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD or cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD:
							
							# report presence of cloud
							temp["cloud_present"] = 1
							
							# report max cloup top height, pressure-wise minimum
							min_pressure = min(cloud_top_pres_ascending[lat_range_higher:lat_range_lower+1, lon_range_lower:lon_range_higher+1].min(), cloud_top_pres_descending[lat_range_higher:lat_range_lower+1, lon_range_lower:lon_range_higher+1].min())
							
							if type(min_pressure) is np.float32:
								temp["min_cloud_top_pressure"] = min_pressure

							# report max rainfall
							# construct corresponding TRMM filenames - 8 TRMM files per 1 AIRS file
							trmm_filename_pattern = "3B42RT." + date.split(' ')[-1] + MONTH_MAP[date.split(' ')[2]] + date.split(' ')[1]
							trmm_available_files = glob("./../data/original/TRMM_3Hourly/" + trmm_filename_pattern + "*.nc4")
							global_max_rainfall_found = -999

							# search every TRMM 3-hourly file for max rainfall
							for trmm_file in trmm_available_files:
								nc_trmm = NetCDFFile(trmm_file)
								
								lat_trmm = nc_trmm.variables['lat'][:]
								lon_trmm = nc_trmm.variables['lon'][:]
								lon_list_trmm = lon_trmm.tolist()
								lat_list_trmm = lat_trmm.tolist()
								
								precipitation = nc_trmm.variables['precipitation'][:]

								bounding_box_left_trmm = 0
								bounding_box_right_trmm = 0
								bounding_box_top_trmm = 0
								bounding_box_bottom_trmm = 0

								for index, i in enumerate(lon_list_trmm):
									if i < search_for_lon and search_for_lon < lon_list_trmm[index+1]:
										bounding_box_left_trmm = index
										bounding_box_right_trmm = index + 1
										break

								for index, i in enumerate(lat_list_trmm):
									if i < search_for_lat and search_for_lat < lat_list_trmm[index+1]:
										bounding_box_bottom_trmm = index
										bounding_box_top_trmm = index + 1
										break
								
								# define how much area around the coordinates to scan
								# TRMM data is 0.25 deg resolution, so multiply by 4 to follow global scan scale
								# lat data is increasing
								lat_range_lower_trmm = bounding_box_bottom_trmm - (NO_OF_ADJACENT_GRIDS_TO_SCAN*4)
								lat_range_higher_trmm = bounding_box_top_trmm + (NO_OF_ADJACENT_GRIDS_TO_SCAN*4)
								# lon data is increasing
								lon_range_lower_trmm = bounding_box_left_trmm - (NO_OF_ADJACENT_GRIDS_TO_SCAN*4)
								lon_range_higher_trmm = bounding_box_right_trmm + (NO_OF_ADJACENT_GRIDS_TO_SCAN*4)

								# scan range for rainfall, find max, update global accordingly
								max_rainfall = precipitation[lat_range_lower_trmm : lat_range_higher_trmm + 1, lon_range_lower_trmm : lon_range_higher_trmm + 1].max()
								
								# mm/hr to mm/day, restrict decimals to 2 places
								if type(max_rainfall) is np.float32:
									max_rainfall = max_rainfall * 24
									max_rainfall = float("{0:.2f}".format(max_rainfall))

									if max_rainfall > global_max_rainfall_found:
										global_max_rainfall_found = max_rainfall

							if global_max_rainfall_found > 0:
								temp["max_rainfall"] = global_max_rainfall_found
							
							# report if PV > 2 at 200hPa and 250hPa, if date is Aug
							if "Aug" in date:
								temp["pv>2_at_200hPa"] = 0
								temp["pv>2_at_250hPa"] = 0

								print time.ctime()
								nc_pv = NetCDFFile("../data/original/ERA_Interim/Potential_Vorticity/" + date.split(' ')[-1] + "_pv_Aug_dailymean.nc")
								
								lat_pv = nc_pv.variables['latitude'][:]
								lon_pv = nc_pv.variables['longitude'][:]
								lev_pv = nc_pv.variables['level'][:]
								lon_pv = lon_pv - 180.0
								lon_list_pv = lon_pv.tolist()
								lat_list_pv = lat_pv.tolist()
								
								pv = nc_pv.variables['pv'][:]
								pv = pv * 1E6

								bounding_box_left_pv = 0
								bounding_box_right_pv = 0
								bounding_box_top_pv = 0
								bounding_box_bottom_pv = 0

								for index, i in enumerate(lon_list_pv):
									if search_for_lon > i and search_for_lon < lon_list_pv[index+1]:
										bounding_box_left_pv = index
										bounding_box_right_pv = index + 1
										break

								for index, i in enumerate(lat_list_pv):
									if search_for_lat < i and search_for_lat > lat_list_pv[index+1]:
										bounding_box_bottom_pv = index + 1
										bounding_box_top_pv = index
										break

								# define how much area around the coordinates to scan
								# lat data is decreasing
								lat_range_lower_pv = bounding_box_bottom_pv + NO_OF_ADJACENT_GRIDS_TO_SCAN
								lat_range_higher_pv = bounding_box_top_pv - NO_OF_ADJACENT_GRIDS_TO_SCAN
								# lon data is increasing 
								lon_range_lower_pv = bounding_box_left_pv - NO_OF_ADJACENT_GRIDS_TO_SCAN
								lon_range_higher_pv = bounding_box_right_pv + NO_OF_ADJACENT_GRIDS_TO_SCAN

								day = int(date.split(' ')[1])
								time_index = day - 1
								for p in range(lat_range_higher, lat_range_lower + 1):
									for q in range(lon_range_lower, lon_range_higher + 1):
										# print "in here!"
										if pv[time_index, lev_pv.tolist().index(200), p, q] > PV_THRESHOLD:
											print "1"
											temp["pv>2_at_200hPa"] = 1
										if pv[time_index, lev_pv.tolist().index(250), p, q] > PV_THRESHOLD:
											print "1"
											temp["pv>2_at_250hPa"] = 1

							# within the 5x5 grid, any point works for checking values. no need to check every grid
							break

			# not enough data in file
			else:
				temp["cloud_present"] = "NA"
				temp["min_cloud_top_pressure"] = "NA"
				temp["max_rainfall"] = "NA"
				temp["pv>2_at_200hPa"] = "NA"
				temp["pv>2_at_250hPa"] = "NA"

		# no corresponding AIRS file found
		else:
			temp["cloud_present"] = "NA"
			temp["min_cloud_top_pressure"] = "NA"
			temp["max_rainfall"] = "NA"
			temp["pv>2_at_200hPa"] = "NA"
			temp["pv>2_at_250hPa"] = "NA"

		rows_list.append(temp)

	# write rows_list to csv
	out_df = pd.DataFrame(rows_list, columns=["st. no.", "dates", "cloud_present", "min_cloud_top_pressure", "max_rainfall", "pv>2_at_200hPa", "pv>2_at_250hPa"])
	# print "% of cloud presence in available data: ", out_df["cloud_present"].value_counts(1)['1']
	out_df.to_csv(DIRECTORY_PATH + "/" + file.split('/')[-1].split('.')[0] + "_airs_trmm.csv", index=False, header=True)
	print "variables check complete :", file

import numpy as np
import pandas as pd
from glob2 import glob

DIRECTORY = "../data/computed_tropopause_data_NEW/*_all_data.csv"
# https://stackoverflow.com/questions/6773584/how-is-pythons-glob-glob-ordered
files = sorted(glob(DIRECTORY))
MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

# for every station
for f in files:
	filename = f.split('/')[-1].split('.')[0]
	df = pd.read_csv(f)
	rows_list = []
	# iterate once for every month
	for month in MONTHS:
		# select rows with only a particular month
		temp_df = df.loc[df['dates'].str.contains(month)]
		# add row of month to output df
		temp = {}
		temp["Month"] = month
		temp["Total_DT_Days"] = temp_df.shape[0]
		temp["Mean_LRT1"] = float(format(temp_df["lrt1"].mean(), ".2f"))
		temp["Mean_LRT2"] = float(format(temp_df["lrt2"].mean(), ".2f"))
		temp["Mean_Temp1"] = float(format(temp_df["temp1"].mean(), ".2f"))
		temp["Mean_Temp2"] = float(format(temp_df["temp2"].mean(), ".2f"))
		temp["StdDev_LRT1"] = float(format(temp_df["lrt1"].std(), ".2f"))
		temp["StdDev_LRT2"] = float(format(temp_df["lrt2"].std(), ".2f"))
		temp["StdDev_Temp1"] = float(format(temp_df["temp1"].std(), ".2f"))
		temp["StdDev_Temp2"] = float(format(temp_df["temp2"].std(), ".2f"))
		rows_list.append(temp)

	# generate year_df from months, save
	year_df = pd.DataFrame(rows_list, columns=["Month", "Total_DT_Days", "Mean_LRT1", "Mean_LRT2", "Mean_Temp1", "Mean_Temp2", "StdDev_LRT1", "StdDev_LRT2", "StdDev_Temp1", "StdDev_Temp2"])
	year_df.to_csv("../data/computed_tropopause_data_NEW/" + filename + "_monthly_analysis.csv", index=False, header=True)
	print "station done: ", f
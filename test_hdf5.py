from netCDF4 import Dataset
import matplotlib.pyplot as plt
# https://annefou.github.io/metos_python/

f = Dataset('./../data/swath2grid/MLS/MLS-Aura_L2GP-Temperature_v04-20-c01_2014d005.he5','r')
# f = Dataset('./../data/swath2grid/MLS-Aura_L2GP-BrO_v04-23-c03_2016d302.he5','r')

# '/HDFEOS/SWATHS/BrO/Geolocation Fields/Latitude'
# '/HDFEOS/SWATHS/BrO/Geolocation Fields/Longitude'
# '/HDFEOS/SWATHS/BrO/Geolocation Fields/Pressure'
# '/HDFEOS/SWATHS/BrO/Data Fields/L2gpValue'
# data = f['/HDFEOS/SWATHS/Temperature/Data Fields/L2gpValue']
# data = f['/HDFEOS/SWATHS/Temperature/Data Fields/L2gpValue'][:]
data = f['/HDFEOS/SWATHS/Temperature/Geolocation Fields/Pressure'][:]
# data = f['/HDFEOS/SWATHS/Temperature/Geolocation Fields/Latitude'][:]

# data=f.variables['Temperature'][:]
# print(type(data))
print "shape: ", data.shape
print "max: ", data.max()
print "min: ", data.min()
print data[:90]
# plt.imshow(data)
# plt.show()

f.close()
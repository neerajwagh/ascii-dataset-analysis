# -*- coding: utf-8 -*-
from __future__ import division

import re
import sys
import timeit

import numpy as np
import pandas as pd
from glob2 import glob
from netCDF4 import Dataset as NetCDFFile

# SETUP ENVIRONMENT FOR EXECUTION
# ------------------------------
# install python 2.7
# install pip utility
# $ pip install glob2
# $ pip install re
# ------------------------------
COORDINATES_MAP = {
	'43285': {
		"name": "mangalore",
		"lat": 12.9141,
		"lon": 74.8560
	},
	# vidd
	'42182': {
		"name": "delhi",
		"lat": 28.7041,
		"lon": 77.1025
	},

	'43346': {
		"name": "karaikal",
		"lat": 10.9254,
		"lon": 79.8380
	},
	# vanp
	'42867': {
		"name": "nagpur",
		"lat": 21.1458,
		"lon": 79.0882
	},
	# vegt
	'42410': {
		"name": "guahati",
		"lat": 26.1445,
		"lon": 91.7362
	}
}
# HOW TO RUN PROGRAM
# ------------------------------
# INPUT: .csv files with separator = station_number date, assumed original columns =
# -------------------------------------------------------------------------------------------
#    "PRES","HGHT","TEMP","DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"
#     hPa     m      C      C      C      %      %    g/kg    deg   knot     K      K      K 
# -------------------------------------------------------------------------------------------
# OUTPUT: corresponding files with separator = station_number date, columns = height, temperature (averaged)
# new height = 7km to 25km with 1km step, average the temperature for middle heights, interpolate for NaNs 

# MAKE A COPY OF THE ENTIRE TOP-LEVEL DIRECTORY THAT CONTAINS ALL THE STATION FOLDERS
# INSERT PATH FOR COPIED TOP-LEVEL DIRECTORY HERE, REMOVE THE / AT THE END
DIRECTORY_PATH = "/home/neeraj/ascii-dataset-cleaning/data/copied_dir"
CURERNT_STATION_ID = "43285"
search_for_lon = COORDINATES_MAP["43285"]["lon"]
search_for_lat = COORDINATES_MAP["43285"]["lat"]
INTERPOLATE_EVERY_X_METRES = 100
# lapse rate threshold
THRESHOLD = 2.00
NO_OF_ADJACENT_GRIDS_TO_SCAN = 2
# $ python tropopause_calculation.py
# ------------------------------

MONTH_MAP = {
	'Jan' : '01',
	'Feb' : '02',
	'Mar' : '03',
	'Apr' : '04',
	'May' : '05',
	'Jun' : '06',
	'Jul' : '07',
	'Aug' : '08',
	'Sep' : '09',
	'Oct' : '10',
	'Nov' : '11',
	'Dec' : '12'
}

raw_columns = ["PRES", "HGHT", "TEMP", "DWPT", "FRPT", "RELH", "RELI", "MIXR", "DRCT", "SKNT", "THTA", "THTE", "THTV"]
compiled_columns = ["st. no.", "dates", "lrt1", "temp1", "lrt2", "temp2"]

heights = np.arange(10000, 27000, INTERPOLATE_EVERY_X_METRES)
DIV_FACTOR = INTERPOLATE_EVERY_X_METRES / 1000.0
SCALE_FACTOR = int(1000.0 / INTERPOLATE_EVERY_X_METRES)

# open compiled DT file for one station
main_df = pd.ExcelFile("./../data/computed_tropopause_data/" + CURERNT_STATION_ID + "_all_data.xlsx", usecols=compiled_columns).parse('All_data')

# add empty pressure columns to it.
main_df['pres1'] = np.nan
main_df['pres2'] = np.nan
main_df['min_cloudtop_pres'] = np.nan

# for every DT day in df
for row in main_df.itertuples():
	DT_day = getattr(row, "dates")
	day = DT_day.split(' ')[1]
	month = MONTH_MAP[DT_day.split(' ')[2]]
	year = DT_day.split(' ')[-1]

	# need only DT days 2003 onwards
	if int(DT_day.split(' ')[-1]) < 2003:
		continue 

	# construct corresponding raw data file name
	list_of_files = glob(DIRECTORY_PATH + "/**/" + CURERNT_STATION_ID + "_" + DT_day.split(' ')[-1] + ".csv")

	# read, remove columns not required
	df = pd.read_csv(list_of_files[0], usecols = [0, 1, 2], names = raw_columns[:3])

	# remove incomplete data at the beginning of each subset
	incomplete_data = df['TEMP'].isnull()
	separator_row = df['PRES'] > 1500
	df.drop(df.index[incomplete_data & ~separator_row], inplace=True)
	# CAUTION: this will make the input's original indices unusable
	df.reset_index(drop=True, inplace=True)

	# obtain the indices for splitting
	separator_row = df["TEMP"].isnull()
	indices_for_split = np.array(df.index[separator_row]).tolist()

	# split the dataframe at those indices
	dfs = np.split(df, indices_for_split)
	dfs = dfs[1:]

	# collect all rows into a list, then create dataframe at the end, MUCH FASTER!
	avg_df_rows_list = []

	for subset in dfs:
		
		# reset indices to find separator at index 0 always     
		subset.reset_index(drop=True, inplace=True)
		
		# skip subsets for days other than current DT day
		if DT_day != subset.loc[0]['HGHT']:
			continue

		# remove the first row from subset, set dataframe as float again
		subset.drop(subset.index[0], inplace=True)
		subset = subset.astype(np.float64)

		# find max recorded height in data, do not consider heights above this
		if (subset['HGHT'].empty):
			print "subset empty!"

		max_hgt_in_subset = float(np.array(subset['HGHT'], dtype=np.float64).max())
		max_pres_in_subset = float(np.array(subset['PRES'], dtype=np.float64).max())

		# handle the data
		hgt_list = []
		avg_temp_list = []
		avg_pres_list = []

		for index, hgt in enumerate(heights[:-1]):
			if max_hgt_in_subset > hgt:
				
				height_range = subset['HGHT'].between(hgt, heights[index+1], inclusive=False)
				hgt_list.append(hgt)

				avg_temp_list.append(subset[height_range]['TEMP'].mean())
				avg_pres_list.append(subset[height_range]['PRES'].mean())

		# interpolate to remove NaNs in avg_temp, restrain decimal digits
		avg_temp_list = pd.Series(avg_temp_list).interpolate(method='linear').tolist()
		avg_temp_list = [format(x, '.2f') for x in avg_temp_list]

		avg_pres_list = pd.Series(avg_pres_list).interpolate(method='linear').tolist()
		avg_pres_list = [format(x, '.2f') for x in avg_pres_list]

		# extract height at which ST and DT were found
		hgt1 = getattr(row, "lrt1")
		hgt2 = getattr(row, "lrt2")
		
		try:
			# find corresponding pressures to these heights
			pres1 = avg_pres_list[hgt_list.index(hgt1)]
			pres2 = avg_pres_list[hgt_list.index(hgt2)]

			# save to df
			main_df.at[getattr(row, "Index"), 'pres1'] = pres1
			main_df.at[getattr(row, "Index"), 'pres2'] = pres2
		except ValueError:
			print "ST/DT less than 10km on day:", DT_day

		# don't need to scan remaining days
		break

	# START CHECK FOR CLOUD TOP PRESSURE
	# ----------------------------------------------
	# construct AIRS filename
	airs_file_pattern = "AIRS." + year + "." + month + "." + day 
	list_of_airs_files = glob("./../data/original/AIRS/" + airs_file_pattern + "*.nc4")

	if list_of_airs_files:

		nc = NetCDFFile(list_of_airs_files[0])
		
		lat = nc.variables['Latitude'][:]
		lon = nc.variables['Longitude'][:]
		lon_list = lon.tolist()
		lat_list = lat.tolist()

		cloud_top_pres_ascending = nc.variables['CloudTopPres_A'][:]
		cloud_top_pres_descending = nc.variables['CloudTopPres_D'][:]

		# these define the enclosing grid of above station
		bounding_box_left = 0
		bounding_box_right = 0
		bounding_box_top = 0
		bounding_box_bottom = 0

		for index, i in enumerate(lon_list):
			if search_for_lon > i and search_for_lon < lon_list[index+1]:
				bounding_box_left = index
				bounding_box_right = index + 1
				break

		for index, i in enumerate(lat_list):
			if search_for_lat < i and search_for_lat > lat_list[index+1]:
				bounding_box_bottom = index + 1
				bounding_box_top = index
				break

		# define how much area around the coordinates to scan
		# lat data is decreasing
		lat_range_lower = bounding_box_bottom + NO_OF_ADJACENT_GRIDS_TO_SCAN
		lat_range_higher = bounding_box_top - NO_OF_ADJACENT_GRIDS_TO_SCAN
		# lon data is increasing
		lon_range_lower = bounding_box_left - NO_OF_ADJACENT_GRIDS_TO_SCAN
		lon_range_higher = bounding_box_right + NO_OF_ADJACENT_GRIDS_TO_SCAN

		# report max cloup top height, pressure-wise minimum
		min_pressure = min(cloud_top_pres_ascending[lat_range_higher:lat_range_lower+1, lon_range_lower:lon_range_higher+1].min(), cloud_top_pres_descending[lat_range_higher:lat_range_lower+1, lon_range_lower:lon_range_higher+1].min())

		# if type(min_pressure) is np.float32:
		main_df.at[getattr(row, "Index"), 'min_cloudtop_pres'] = min_pressure		

	print "DT day updated: ", DT_day

# write extended df to file
main_df.to_excel("extended_" + CURERNT_STATION_ID + "_all_data.xlsx", index=False, header=True, columns = ["st. no.", "dates", "lrt1", "temp1", "pres1", "min_cloudtop_pres", "lrt2", "temp2", "pres2"])

print "all done!"
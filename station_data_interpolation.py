# -*- coding: utf-8 -*-
from __future__ import division
from glob2 import glob
import timeit
import pandas as pd
import numpy as np
import re

# FIXME: file processing can be made parallel

# SETUP ENVIRONMENT FOR EXECUTION
# ------------------------------
# install python 2.7
# install pip utility
# $ pip install glob2
# $ pip install re
# ------------------------------

# HOW TO RUN PROGRAM
# ------------------------------
# INPUT: .csv files with separator = station_number date, assumed original columns =
# -------------------------------------------------------------------------------------------
#    "PRES","HGHT","TEMP","DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"
#     hPa     m      C      C      C      %      %    g/kg    deg   knot     K      K      K 
# -------------------------------------------------------------------------------------------
# OUTPUT: corresponding files with separator = station_number date, columns = height, temperature (averaged)
# new height = 7km to 25km with 1km step, average the temperature for middle heights, interpolate for NaNs 

# MAKE A COPY OF THE ENTIRE TOP-LEVEL DIRECTORY THAT CONTAINS ALL THE STATION FOLDERS
# INSERT PATH FOR COPIED TOP-LEVEL DIRECTORY HERE, REMOVE THE / AT THE END
DIRECTORY_PATH = "/home/neeraj/ascii-dataset-cleaning/data/copied_dir"
INTERPOLATE_EVERY_X_METRES = 200
# $ python station_data_interpolation.py
# ------------------------------

list_of_files = glob(DIRECTORY_PATH + "/**/*.csv")
start_time = timeit.time.ctime()
my_cols = ["PRES","HGHT","TEMP","DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"]
heights = np.arange(5000, 27000, INTERPOLATE_EVERY_X_METRES)
DIV_FACTOR = INTERPOLATE_EVERY_X_METRES / 1000.0
SCALE_FACTOR = int(1000.0 / INTERPOLATE_EVERY_X_METRES)
THRESHOLD = 2.0

# for every data file inside the folder
for file in list_of_files:

	# read, remove columns not required
	df = pd.read_csv(file, names=my_cols)
	df.drop(["DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"], axis = 1, inplace = True)

	# remove incomplete data at the beginning of each subset
	incomplete_data = df['TEMP'].isnull()
	separator_row = df['PRES'] > 1500
	df.drop(df.index[incomplete_data & ~separator_row], inplace=True)
	df.reset_index(drop=True, inplace=True)

	# obtain the indices for splitting
	separator_row = df["TEMP"].isnull()
	indices_for_split = np.array(df.index[separator_row]).tolist()

	# split the dataframe at those indices
	dfs = np.split(df, indices_for_split)
	dfs = dfs[1:]

	# collect all rows into a list, then create dataframe at the end, MUCH FASTER!
	avg_df_rows_list = []

	for subset in dfs:
		
		# reset indices to find separator at index 0 always     
		subset.reset_index(drop=True, inplace=True)
		
		# handle the separator row separately ;)
		sep_row = {}
		sep_row["HGHT"] = subset.loc[0]['PRES']
		sep_row["AVG_TEMP"] = subset.loc[0]['HGHT']

		# remove the first row from subset, set dataframe as float32 again
		subset.drop(subset.index[0], inplace=True)
		subset = subset.astype(np.float64)
		
		# find max recorded height in data, do not consider heights above this
		max_hgt_in_subset = float(np.array(subset['HGHT'], dtype=np.float64).max())
		
		# handle the data
		hgt_list = []
		avg_temp_list = []

		for index, hgt in enumerate(heights[:-1]):
			if max_hgt_in_subset > hgt:
				height_range = subset['HGHT'].between(hgt, heights[index+1], inclusive=False)
				hgt_list.append(hgt)
				avg_temp_list.append(subset[height_range]['TEMP'].mean())
		
		# interpolate to remove NaNs in avg_temp, restrain decimal digits
		avg_temp_list = pd.Series(avg_temp_list).interpolate(method='linear').tolist()
		avg_temp_list = [format(x, '.2f') for x in avg_temp_list]
		
		temp_diff_list = []
		for index, hgt in enumerate(hgt_list):
			if (index > 0):
				temp_diff = ((float(avg_temp_list[index]) - float(avg_temp_list[index-1])) / DIV_FACTOR) * -1
				temp_diff = float(format(temp_diff, '.2f'))
			else:
				temp_diff = ''
			temp_diff_list.append(temp_diff)

		sep_row["LAPSE_RATE"] = ''
		avg_df_rows_list.append(sep_row)		

		# convert to key-value form for adding to df
		for index, hgt in enumerate(hgt_list):
			data_row = {}
			data_row["HGHT"] = hgt
			data_row["AVG_TEMP"] = avg_temp_list[index]
			data_row["TEMP_DIFF"] = temp_diff_list[index]
			avg_df_rows_list.append(data_row)

	# create new df from the accumulated rows
	avg_df = pd.DataFrame(avg_df_rows_list, columns=["HGHT", "AVG_TEMP", "TEMP_DIFF"])
	avg_df.reset_index(drop=True, inplace=True)

	# test for NaN values, dataframe should be empty
	no_value_available = avg_df['AVG_TEMP'].isnull()
	if (avg_df[no_value_available].empty == False):
		print "NaN values present in AVG_TEMP column!"

	# write to file. phew!
	avg_df.to_csv(file, index=False, header=False)
	print "station data interpolated: ", file

# successful execution!
print "PROGRAM START: ", start_time
print "PROGRAM END: ", timeit.time.ctime()
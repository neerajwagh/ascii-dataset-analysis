# -*- coding: utf-8 -*-
from __future__ import division

import os.path
import time

import numpy as np
import pandas as pd
from dateutil import parser
from glob2 import glob
from netCDF4 import Dataset as NetCDFFile
from netCDF4 import date2index

DIRECTORY_PATH = "../data/computed_tropopause_data/delhi_all_data.csv"

# = 2 means a grid of 5x5 cells with the station/city in the centre cell
# # for 200km
# NO_OF_ADJACENT_GRIDS_TO_SCAN_AIRS = 2
# NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD = 8
# NO_OF_ADJACENT_GRIDS_TO_SCAN_TRMM = 8
# for 500km

# since IMD data is at 0.25 degree resolution, we're scanning 2 degrees away from station = 200km, in all directions
NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD = 8

out_columns = [
	"st. no.", 
	"dates",
	"max_rainfall_200km_radius_IMD"
]

MONTH_MAP = {
	'Jan' : '01',
	'Feb' : '02',
	'Mar' : '03',
	'Apr' : '04',
	'May' : '05',
	'Jun' : '06',
	'Jul' : '07',
	'Aug' : '08',
	'Sep' : '09',
	'Oct' : '10',
	'Nov' : '11',
	'Dec' : '12'
}

df = pd.read_csv(DIRECTORY_PATH)

# station coordinates - Delhi
search_for_lat = 28.7041
search_for_lon = 77.1025

# open IMD data for that year, check max rainfall
nc = NetCDFFile("./../data/original/IMD/bbs_suv_imd_1979_2016.nc")

lat = nc.variables['latitude'][:]
lon = nc.variables['longitude'][:]
lon_list = lon.tolist()
lat_list = lat.tolist()
time = nc.variables['time']
rainfall = nc.variables['rf_1'][:]

# these define the enclosing grid of above station
bounding_box_left = 0
bounding_box_right = 0
bounding_box_top = 0
bounding_box_bottom = 0

for index, i in enumerate(lon_list):
	if search_for_lon > i and search_for_lon < lon_list[index+1]:
		bounding_box_left = index
		bounding_box_right = index + 1
		break

# define how much area around the coordinates to scan
# lon data is increasing 
lon_range_lower = bounding_box_left - NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD
lon_range_higher = bounding_box_right + NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD

for index, i in enumerate(lat_list):
	if search_for_lat > i and search_for_lat < lat_list[index+1]:
		bounding_box_bottom = index
		bounding_box_top = index + 1
		break

# lat data is increasing
lat_range_lower = bounding_box_bottom - NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD
lat_range_higher = bounding_box_top + NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD

rows_list = []
# for every DT day in file
for row in df.itertuples():
	date = getattr(row, "dates")

	temp = {}
	temp["st. no."] = "delhi"
	temp["dates"] = date

	year = date.split(' ')[-1]
	month = date.split(' ')[2]
	day = date.split(' ')[1]

	# we don't have IMD rainfall before 1979 and after 2016, skip those DT days
	if int(year) < 1979 or int(year) > 2016:
		continue

	# we are only interested in JJAS
	if month not in ["Jun", "Jul", "Aug", "Sep"]:
		continue

	formatted_date = year + "-" + MONTH_MAP[month] + "-" + day
	day_index = date2index(parser.parse(formatted_date), time)
	
	# check rainfall at DT day + 5 days lead
	for lead_day in [6, 5, 4, 3, 2, 1]:
		try:
			# float rf_1(time, z, latitude, longitude) ;
			max_rainfall = rainfall[day_index:day_index+lead_day, 0, lat_range_lower:lat_range_higher+1, lon_range_lower:lon_range_higher+1].max()
			break
		except:
			pass

	if type(max_rainfall) in [np.float32, np.float64]:
		temp["max_rainfall_200km_radius_IMD"] = max_rainfall
	else:
		print "NA data!"
		temp["max_rainfall_200km_radius_IMD"] = "na_data"
	
	rows_list.append(temp)
	print "DT day processed:", date
	print "---------------------------------------------------"

# write rows_list to csv
out_df = pd.DataFrame(rows_list, columns=out_columns)
# print "% of cloud presence in available data: ", out_df["cloud_present"].value_counts(1)['1']
out_df.to_csv("../data/computed_tropopause_data/Delhi_max_IMD_rainfall.csv", index=False, header=True)
print "done!"

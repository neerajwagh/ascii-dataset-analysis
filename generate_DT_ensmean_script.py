# -*- coding: utf-8 -*-
from __future__ import division

import os.path
import time
import numpy as np
import pandas as pd

station_list = [
	"Delhi",
	"Nagpur",
	"Guwahati",
	"Mangalore",
	"Karaikal"
	]

months_list = [
	"Aug",
	"Dec"
	]

for station_name in station_list:
	for month in months_list:
		output_file_name = "./Neeraj_ensmean_scr_Anom_DTday_" + month + "_" + station_name + ".sh"
		out_fp = open(output_file_name, "w")

		command = "cdo ensmean "
		for year in range(2005, 2018):
			year = str(year)
			
			# cdo ensmean Neeraj_timmean_Delhi_DTDec1979.nc Neeraj_timmean_Delhi_DTDec1980.nc Neeraj_ensmean_Delhi_DTDec.nc
			command += "Neeraj_timmean_" + station_name + "_DT" + month + year + ".nc "
			
		command +=  "Neeraj_ensmean_" + station_name + "_DT" + month + ".nc\n"
		out_fp.write(command)
		out_fp.close()
		print "done: ", month, station_name
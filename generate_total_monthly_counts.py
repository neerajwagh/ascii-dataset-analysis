# -*- coding: utf-8 -*-
from __future__ import division
from glob2 import glob
import timeit
import pandas as pd
import numpy as np
import re

# TODO: file processing can be made parallel, be careful of concurrent file append to yearly_counts.csv

# SETUP ENVIRONMENT FOR EXECUTION
# ------------------------------
# install python 2.7
# install pip utility
# $ pip install glob2
# $ pip install re
# ------------------------------

# HOW TO RUN PROGRAM
# ------------------------------
# INPUT: .csv files with separator = station_number date, assumed original columns =
# -------------------------------------------------------------------------------------------
#    "PRES","HGHT","TEMP","DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"
#     hPa     m      C      C      C      %      %    g/kg    deg   knot     K      K      K 
# -------------------------------------------------------------------------------------------
# OUTPUT: corresponding files with separator = station_number date, columns = height, temperature (averaged)
# new height = 7km to 25km with 1km step, average the temperature for middle heights, interpolate for NaNs 

# MAKE A COPY OF THE ENTIRE TOP-LEVEL DIRECTORY THAT CONTAINS ALL THE STATION FOLDERS
# INSERT PATH FOR COPIED TOP-LEVEL DIRECTORY HERE, REMOVE THE / AT THE END
station_name = "guwahati"
DIRECTORY_PATH = "../data/data_validity/" + station_name
# ------------------------------

list_of_files = glob(DIRECTORY_PATH + "/**/*_data_counts.csv")
start_time = timeit.time.ctime()
my_cols = ["YEAR", "MONTH", "NO_OF_DAYS_DATA_OBSERVED", "NO_OF_DAYS_DATA_VALID"]
MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

TOTAL_COUNTS_DATA_VALID = {
	"Jan" : 0,
	"Feb" : 0,
	"Mar" : 0,
	"Apr" : 0,
	"May" : 0,
	"Jun" : 0,
	"Jul" : 0,
	"Aug" : 0,
	"Sep" : 0,
	"Oct" : 0,
	"Nov" : 0,
	"Dec" : 0
}

TOTAL_COUNTS_DATA_OBSERVED = {
	"Jan" : 0,
	"Feb" : 0,
	"Mar" : 0,
	"Apr" : 0,
	"May" : 0,
	"Jun" : 0,
	"Jul" : 0,
	"Aug" : 0,
	"Sep" : 0,
	"Oct" : 0,
	"Nov" : 0,
	"Dec" : 0
}

# iterate over yearly files
for file in list_of_files:

	df = pd.read_csv(file, names=my_cols)
	
	for month in TOTAL_COUNTS_DATA_VALID.keys():
		
		obs_count = int(df[df["MONTH"] == month]["NO_OF_DAYS_DATA_OBSERVED"])
		TOTAL_COUNTS_DATA_OBSERVED[month] += obs_count
		
		valid_count = int(df[df["MONTH"] == month]["NO_OF_DAYS_DATA_VALID"])
		TOTAL_COUNTS_DATA_VALID[month] += valid_count
		
# monthly counts accumulated across all years, arrange in a table
avg_df_rows_list = []
for month in MONTHS:
	row = {}
	row["MONTH"] = month
	row["TOTAL_COUNTS_DATA_OBSERVED"] = TOTAL_COUNTS_DATA_OBSERVED[month]
	row["TOTAL_COUNTS_DATA_VALID"] = TOTAL_COUNTS_DATA_VALID[month]
	avg_df_rows_list.append(row)

# create df for the year, write
year_df = pd.DataFrame(avg_df_rows_list, columns=["MONTH", "TOTAL_COUNTS_DATA_OBSERVED", "TOTAL_COUNTS_DATA_VALID"])
year_df.to_csv(DIRECTORY_PATH + "/" + station_name + "_total_monthly_counts.csv", index=False, header=True)

# successful execution!
print "done!"
# -*- coding: utf-8 -*-
from __future__ import division

import re
import timeit
# module load GNU-python/2.7.14_ccplot
import numpy as np
import pandas as pd
from glob2 import glob

# FIXME: file processing can be made parallel, be careful of concurrent file append to yearly_counts.csv

# SETUP ENVIRONMENT FOR EXECUTION
# ------------------------------
# install python 2.7
# install pip utility
# $ pip install glob2
# $ pip install re
# ------------------------------

# HOW TO RUN PROGRAM
# ------------------------------
# INPUT: .csv files with separator = station_number date, assumed original columns =
# -------------------------------------------------------------------------------------------
#    "PRES","HGHT","TEMP","DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"
#     hPa     m      C      C      C      %      %    g/kg    deg   knot     K      K      K 
# -------------------------------------------------------------------------------------------
# OUTPUT: corresponding files with separator = station_number date, columns = height, temperature (averaged)
# new height = 7km to 25km with 1km step, average the temperature for middle heights, interpolate for NaNs 

# MAKE A COPY OF THE ENTIRE TOP-LEVEL DIRECTORY THAT CONTAINS ALL THE STATION FOLDERS
# INSERT PATH FOR COPIED TOP-LEVEL DIRECTORY HERE, REMOVE THE / AT THE END
DIRECTORY_PATH = "./../data/copied_dir/nagpur_ST_all"
INTERPOLATE_EVERY_X_METRES = 100
THRESHOLD = 2.00
# $ python tropopause_calculation.py
# ------------------------------

# list_of_files = glob(DIRECTORY_PATH + "/**/*.csv")
list_of_files = glob(DIRECTORY_PATH + "/*.csv")

YEARLY_COUNTS_FILE_PATH = DIRECTORY_PATH + "/yearly_counts.csv"
start_time = timeit.time.ctime()
my_cols = ["PRES","HGHT","TEMP","DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"]

heights = np.arange(10000, 27000, INTERPOLATE_EVERY_X_METRES)
DIV_FACTOR = INTERPOLATE_EVERY_X_METRES / 1000.0
SCALE_FACTOR = int(1000.0 / INTERPOLATE_EVERY_X_METRES)

# for every data file inside the folder
for file in list_of_files:

	# setup a yearly count
	YEARLY_COUNT = 0

	# read, remove columns not required
	df = pd.read_csv(file, names=my_cols)
	df.drop(["DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"], axis = 1, inplace = True)

	# remove incomplete data at the beginning of each subset
	incomplete_data = df['TEMP'].isnull()
	separator_row = df['PRES'] > 1500
	df.drop(df.index[incomplete_data & ~separator_row], inplace=True)
	df.reset_index(drop=True, inplace=True)

	# obtain the indices for splitting
	separator_row = df["TEMP"].isnull()
	indices_for_split = np.array(df.index[separator_row]).tolist()

	# split the dataframe at those indices
	dfs = np.split(df, indices_for_split)
	dfs = dfs[1:]

	# collect all rows into a list, then create dataframe at the end, MUCH FASTER!
	avg_df_rows_list = []
	only_st_df_rows_list = []

	for subset in dfs:
		
		# reset indices to find separator at index 0 always     
		subset.reset_index(drop=True, inplace=True)
		
		# handle the data separator row
		sep_row = {}
		# station number
		sep_row["HGHT"] = subset.loc[0]['PRES']
		# date
		sep_row["AVG_TEMP"] = subset.loc[0]['HGHT']

		# remove the separator row from subset, set dataframe as float again
		subset.drop(subset.index[0], inplace=True)
		subset = subset.astype(np.float64)

		# find max recorded height in data, do not consider heights above this
		if (subset['HGHT'].empty):
			print "subset empty!"
		max_hgt_in_subset = float(np.array(subset['HGHT'], dtype=np.float64).max())
		
		# handle the data
		hgt_list = []
		avg_temp_list = []

		for index, hgt in enumerate(heights[:-1]):
		# for index, hgt in enumerate(heights[1:]):
			if max_hgt_in_subset > hgt:
				# forward differencing notion applied here
				# average of all the raw data b/w 15.1km and 15.2km applied to 15.1km
				height_range = subset['HGHT'].between(hgt, heights[index+1], inclusive=False)
				# height_range = subset['HGHT'].between(heights[index-1], hgt, inclusive=False)
				hgt_list.append(hgt)
				avg_temp_list.append(subset[height_range]['TEMP'].mean())

		# interpolate to remove NaNs in avg_temp, restrain decimal digits
		avg_temp_list = pd.Series(avg_temp_list).interpolate(method='linear').tolist()
		avg_temp_list = [format(x, '.2f') for x in avg_temp_list]
		
		lapse_rate_list = []
		# hgt_list and avg_temp_list have the same lengths
		for index, hgt in enumerate(hgt_list):
			if (index < len(avg_temp_list) - 1):
				# forward difference as per https://en.wikipedia.org/wiki/Finite_difference#Forward,_backward,_and_central_differences
				# temp_diff = ((float(avg_temp_list[index]) - float(avg_temp_list[index-1])) / DIV_FACTOR) * -1
				temp_diff = ((float(avg_temp_list[index+1]) - float(avg_temp_list[index])) / DIV_FACTOR) * -1
				temp_diff = float(format(temp_diff, '.2f'))
			else:
				temp_diff = ''
			lapse_rate_list.append(temp_diff)

		# # write avg_temp_list and lapse_rate_list to file for manual verification
		# avg_df = pd.DataFrame({'avg_temperature': avg_temp_list, 'lapse_rate': lapse_rate_list, 'height': hgt_list})
		# avg_df.to_csv(DIRECTORY_PATH + "/manual_test.csv", index=False, header=True)
		# continue

		# ignore the '' in the LR list before applying WMO criteria
		# lapse_rate_list = lapse_rate_list[1:]
		lapse_rate_list = lapse_rate_list[:-1]

		# analyze lapse rate list for the expected pattern
		# WMO TROPOPAUSE DEFINITION
		# (1) the first tropopause is defined as the lowest level at which the lapse rate Y (defined as -dT/dz) decreases to 2degCkm-1 or less, provided that the average lapse rate between this level and all higher levels within 2 km does not exceed 2degCkm-1, and (2) if above the first tropopause the average lapse rate between any level and all higher levels within 1 km exceeds 3degCkm-1, then a second tropopause is defined under the same criterion as under (1). The second tropopause may be either within or above the 1 km layer discussed in step 2.
		
		# REINIT ALL NUMERICAL LOCAL VARIABLES SO 2 PROFILES DON'T GET MIXED UP
		lr_lt_THRESHOLD_fo = -1
		two_km_above = -1
		avg_rate = -1
		avg_lr_gt_3_fo = -1
		second_lrt_fo = -1

		# tests for first tropopause
		# WMO step 1
		lt_THRESHOLD = [(x <= THRESHOLD) for x in lapse_rate_list]
		lr_lt_THRESHOLD_occurs = False
		try:
			# lapse rate less than threshold first occurence 
			lr_lt_THRESHOLD_fo = lt_THRESHOLD.index(True)
			lr_lt_THRESHOLD_occurs = True
		except ValueError:
			# move onto next profile
			print "discarded: LR < 2 never found: ", sep_row["AVG_TEMP"]
			continue

		avg_lr_lt_2 = False
		# FIXME: check the index at which two_km_above lands
		two_km_above = lr_lt_THRESHOLD_fo + 2 * SCALE_FACTOR
		if two_km_above <= len(lapse_rate_list) - 1:
			# FIXME: check the indexes involved in averaging
			avg_rate = np.array(lapse_rate_list[lr_lt_THRESHOLD_fo : two_km_above + 1]).mean()
			if avg_rate < 2.0:
				avg_lr_lt_2 = True
			else:
				# move onto next profile
				print "discarded: avg_lr_lt_2 is false: ", sep_row["AVG_TEMP"]
				continue
		else:
			# move onto next profile
			print "discarded: not enough data for avg_lr_lt_2: ", sep_row["AVG_TEMP"]
			continue

		# tests for second tropopause
		# WMO step 2
		avg_lr_gt_3_occurs = False
		# loop = sliding window of all levels within 1km, upto 1km less than the maximum height available
		for i in range(lr_lt_THRESHOLD_fo + 1, len(lapse_rate_list) - SCALE_FACTOR + 1): 
			avg_rate = np.array(lapse_rate_list[i : i + SCALE_FACTOR]).mean()
			if avg_rate > 3.0:
				avg_lr_gt_3_fo = i
				avg_lr_gt_3_occurs = True
				break

		# WMO step 3
		second_lrt_occurs = False
		if avg_lr_gt_3_occurs:
			for i in range(avg_lr_gt_3_fo, len(lapse_rate_list)):
				if (lapse_rate_list[i] < THRESHOLD):
					two_km_above = i + 2 * SCALE_FACTOR	
					if two_km_above <= len(lapse_rate_list) - 1:
						avg_rate = np.array(lapse_rate_list[i : two_km_above + 1]).mean()
						if avg_rate < 2.0:
							second_lrt_fo = i
							second_lrt_occurs = True
							break
					else:
						# DISCARD PROFILE OR TAKE AVERAGE OF WHATEVER AMOUNT OF DATA WE HAVE LEFT?
						print "discarded: not enough data to confirm LRT2: ", sep_row["AVG_TEMP"]
						break
		else:
			# move onto next profile
			print "discarded: avg_lr_gt_3 never occurs: ", sep_row["AVG_TEMP"]
			# continue

		if (
			# confirm WMO step 1
			lr_lt_THRESHOLD_occurs and 
			avg_lr_lt_2 and

			# confirm WMO step 2
			avg_lr_gt_3_occurs and
			
			# confirm WMO step 3
			second_lrt_occurs
			):
			
			# update count
			YEARLY_COUNT = YEARLY_COUNT + 1

			# record levels at which first and second tropopause were confirmed
			data_row = {}
			data_row["st. no."] = sep_row["HGHT"]
			data_row["dates"] = sep_row["AVG_TEMP"]
			data_row["lrt1"] = hgt_list[lr_lt_THRESHOLD_fo]
			data_row["temp1"] = avg_temp_list[lr_lt_THRESHOLD_fo]
			data_row["lrt2"] = hgt_list[second_lrt_fo]
			data_row["temp2"] = avg_temp_list[second_lrt_fo]
			avg_df_rows_list.append(data_row)

			print "+ve: ", sep_row["AVG_TEMP"]

		elif (
			# confirm WMO step 1
			lr_lt_THRESHOLD_occurs and 
			avg_lr_lt_2 and

			# confirm DT does NOT exist
			(
				~avg_lr_gt_3_occurs or
				~second_lrt_occurs
			)
			):

			# record levels at which first and second tropopause were confirmed
			data_row = {}
			data_row["st. no."] = sep_row["HGHT"]
			data_row["dates"] = sep_row["AVG_TEMP"]
			data_row["lrt1"] = hgt_list[lr_lt_THRESHOLD_fo]
			data_row["temp1"] = avg_temp_list[lr_lt_THRESHOLD_fo]
			data_row["lrt2"] = ""
			data_row["temp2"] = ""
			only_st_df_rows_list.append(data_row)

			print "only ST found: ", sep_row["AVG_TEMP"]
		
	# create new df from the accumulated rows
	only_st_df = pd.DataFrame(only_st_df_rows_list, columns=["st. no.", "dates", "lrt1", "temp1", "lrt2", "temp2"]) 
	only_st_df.reset_index(drop=True, inplace=True)

	avg_df = pd.DataFrame(avg_df_rows_list, columns=["st. no.", "dates", "lrt1", "temp1", "lrt2", "temp2"])
	avg_df.reset_index(drop=True, inplace=True)

	# test for NaN values, dataframe should be empty
	# no_value_available = avg_df['AVG_TEMP'].isnull()
	# if (avg_df[no_value_available].empty == False):
	# 	print "NaN values present in AVG_TEMP column!"

	# prepare a new df for YEAR ---> #DAYS THAT SATISFY CONSTRAINT, append to a common file
	row = []
	dict = {}
	# find year from first row in avg_df or file name (if avg_df is empty)
	if (avg_df.empty):
		dict["YEAR"] = re.findall(r"\_(\d{4})\.", file)[0]	
		dict["COUNT"] = 0
	else:
		dict["YEAR"] = avg_df.loc[0]["dates"].split(' ')[-1]
		dict["COUNT"] = YEARLY_COUNT
	row.append(dict)

	year_count_df = pd.DataFrame(row, columns=["YEAR", "COUNT"])
	year_count_df.to_csv(YEARLY_COUNTS_FILE_PATH, index=False, header=False, mode='a')
	
	# write to file. CAUTION: OVERWRITES INPUT FILE!
	avg_df.to_csv(file, index=False, header=True)
	only_st_df.to_csv(file + ".only_ST", index=False, header=True)
	print "temp averaged + interpolated, yearly counts written: ", file

# ARRANGE THE YEARS IN INCREASING ORDER FOR EASIER PLOTTING
year_count_df.sort_values(by="YEAR", inplace=True)
year_count_df = pd.read_csv(YEARLY_COUNTS_FILE_PATH, names=["YEAR", "COUNT"])
year_count_df.to_csv(YEARLY_COUNTS_FILE_PATH, index=False, header=False)

# successful execution!
print "PROGRAM START: ", start_time
print "PROGRAM END: ", timeit.time.ctime()
print "\n** YEARLY COUNTS FOR PLOTTING ARE STORED IN: " + YEARLY_COUNTS_FILE_PATH + " **\n"
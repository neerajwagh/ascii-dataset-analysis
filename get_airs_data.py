#!/usr/bin/python
from cookielib import CookieJar
from urllib import urlencode
import os.path
import urllib2

# shell commands to download the same data

# DOES NOT WORK!
# aria2c --netrc-path=~/.netrc --save-cookies=~/.urs_cookies --http-auth-challenge=false --optimize-concurrent-downloads=false -i 
# AIRS3STD_V006_links.txt -j5 --save-session=./session_info --http-user=<> --http-passwd=<> --content-disposition

# WORKS! BUT SERIALLY!
# wget --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --auth-no-challenge=on --keep-session-cookies --content-disposition --no-clobber -i AIRS3STD_V006_links.txt 

# WORKS! IN PARALLEL!
# --resume
# --retry-failed
# $ cat TRMM_3B42RT_V7_links_20180123_141736.txt | parallel --eta --bar --resume --joblog jobs_info.txt -j20 wget --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --no-check-certificate --auth-no-challenge=on --reject "index.html*" --keep-session-cookies --content-disposition --no-clobber {}

# The user credentials that will be used to authenticate access to the data 
username = ""
password = ""

# Create a password manager to deal with the 401 reponse that is returned from
# Earthdata Login 
password_manager = urllib2.HTTPPasswordMgrWithDefaultRealm()
password_manager.add_password(None, "https://urs.earthdata.nasa.gov", username, password)
 
# Create a cookie jar for storing cookies. This is used to store and return
# the session cookie given to use by the data server (otherwise it will just
# keep sending us back to Earthdata Login to authenticate).  Ideally, we
# should use a file based cookie jar to preserve cookies between runs. This
# will make it much more efficient.
cookie_jar = CookieJar()


# Install all the handlers.
opener = urllib2.build_opener(
    urllib2.HTTPBasicAuthHandler(password_manager),
    #urllib2.HTTPHandler(debuglevel=1),    # Uncomment these two lines to see
    #urllib2.HTTPSHandler(debuglevel=1),   # details of the requests/responses
    urllib2.HTTPCookieProcessor(cookie_jar))
urllib2.install_opener(opener)
 
 
# Create and submit the request. There are a wide range of exceptions that
# can be thrown here, including HTTPError and URLError. These should be
# caught and handled.
file = open('./AIRS3STD_V006_links.txt')
for url in file:

    filename = url[url.rfind('/')+1:url.rfind('L3')-1]
    request = urllib2.Request(url)
    
    if not os.path.isfile("./../data/original/airs_bulk_download/" + filename + ".nc4"):
        try:
            response = urllib2.urlopen(request)
            body = response.read()
            # save to disk
            with open("./../data/original/airs_bulk_download/" + filename + ".nc4", 'wb') as fd: 
                    fd.write(body)
            print "file done: ", filename
        
        except urllib2.HTTPError:
            with open("./errors.txt", "w+") as err_fd:
                err_fd.write(url)
            print "error saved: ", url
    else:
        print "file already present: ", filename

print "all downloaded!"
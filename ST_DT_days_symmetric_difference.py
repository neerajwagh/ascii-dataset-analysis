from __future__ import division

import numpy as np
import pandas as pd

station = "mangalore"

ST_df = pd.read_csv("./../data/computed_tropopause_data_ST/" + station + "_all_ST_data.csv", usecols=[1])
ST_df.dropna(inplace=True)

DT_df = pd.read_csv("./../data/computed_tropopause_data_NEW/" + station + "_all_data.csv", usecols=[1])
DT_df.dropna(inplace=True)

ST_set = set([x.split('Z ')[1] for x in ST_df["dates"]])
DT_set = set([x.split('Z ')[1] for x in DT_df["dates"]])
new_ST_set = ST_set - DT_set

new_ST = [[x] for x in new_ST_set]
out_df = pd.DataFrame.from_records(new_ST, columns=["dates"])
out_df.to_csv("./../data/computed_tropopause_data_ST/" + station + "_new_ST_try.csv", index=False, header=True)
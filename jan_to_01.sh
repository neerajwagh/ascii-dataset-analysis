#! /bin/bash

sed -i 's/\tJan\t/ 01 /g' delhi_ST_all.txt
sed -i 's/\tJan\t/ 01 /g' guwahati_ST_all.txt
sed -i 's/\tJan\t/ 01 /g' karaikal_ST_all.txt
sed -i 's/\tJan\t/ 01 /g' mangalore_ST_all.txt
sed -i 's/\tJan\t/ 01 /g' nagpur_ST_all.txt

sed -i 's/\tFeb\t/ 02 /g' delhi_ST_all.txt
sed -i 's/\tFeb\t/ 02 /g' guwahati_ST_all.txt
sed -i 's/\tFeb\t/ 02 /g' karaikal_ST_all.txt
sed -i 's/\tFeb\t/ 02 /g' mangalore_ST_all.txt
sed -i 's/\tFeb\t/ 02 /g' nagpur_ST_all.txt

sed -i 's/\tMar\t/ 03 /g' delhi_ST_all.txt
sed -i 's/\tMar\t/ 03 /g' guwahati_ST_all.txt
sed -i 's/\tMar\t/ 03 /g' karaikal_ST_all.txt
sed -i 's/\tMar\t/ 03 /g' mangalore_ST_all.txt
sed -i 's/\tMar\t/ 03 /g' nagpur_ST_all.txt

sed -i 's/\tApr\t/ 04 /g' delhi_ST_all.txt
sed -i 's/\tApr\t/ 04 /g' guwahati_ST_all.txt
sed -i 's/\tApr\t/ 04 /g' karaikal_ST_all.txt
sed -i 's/\tApr\t/ 04 /g' mangalore_ST_all.txt
sed -i 's/\tApr\t/ 04 /g' nagpur_ST_all.txt

sed -i 's/\tMay\t/ 05 /g' delhi_ST_all.txt
sed -i 's/\tMay\t/ 05 /g' guwahati_ST_all.txt
sed -i 's/\tMay\t/ 05 /g' karaikal_ST_all.txt
sed -i 's/\tMay\t/ 05 /g' mangalore_ST_all.txt
sed -i 's/\tMay\t/ 05 /g' nagpur_ST_all.txt

sed -i 's/\tJun\t/ 06 /g' delhi_ST_all.txt
sed -i 's/\tJun\t/ 06 /g' guwahati_ST_all.txt
sed -i 's/\tJun\t/ 06 /g' karaikal_ST_all.txt
sed -i 's/\tJun\t/ 06 /g' mangalore_ST_all.txt
sed -i 's/\tJun\t/ 06 /g' nagpur_ST_all.txt

sed -i 's/\tJul\t/ 07 /g' delhi_ST_all.txt
sed -i 's/\tJul\t/ 07 /g' guwahati_ST_all.txt
sed -i 's/\tJul\t/ 07 /g' karaikal_ST_all.txt
sed -i 's/\tJul\t/ 07 /g' mangalore_ST_all.txt
sed -i 's/\tJul\t/ 07 /g' nagpur_ST_all.txt

sed -i 's/\tAug\t/ 08 /g' delhi_ST_all.txt
sed -i 's/\tAug\t/ 08 /g' guwahati_ST_all.txt
sed -i 's/\tAug\t/ 08 /g' karaikal_ST_all.txt
sed -i 's/\tAug\t/ 08 /g' mangalore_ST_all.txt
sed -i 's/\tAug\t/ 08 /g' nagpur_ST_all.txt

sed -i 's/\tSep\t/ 09 /g' delhi_ST_all.txt
sed -i 's/\tSep\t/ 09 /g' guwahati_ST_all.txt
sed -i 's/\tSep\t/ 09 /g' karaikal_ST_all.txt
sed -i 's/\tSep\t/ 09 /g' mangalore_ST_all.txt
sed -i 's/\tSep\t/ 09 /g' nagpur_ST_all.txt

sed -i 's/\tOct\t/ 10 /g' delhi_ST_all.txt
sed -i 's/\tOct\t/ 10 /g' guwahati_ST_all.txt
sed -i 's/\tOct\t/ 10 /g' karaikal_ST_all.txt
sed -i 's/\tOct\t/ 10 /g' mangalore_ST_all.txt
sed -i 's/\tOct\t/ 10 /g' nagpur_ST_all.txt

sed -i 's/\tNov\t/ 11 /g' delhi_ST_all.txt
sed -i 's/\tNov\t/ 11 /g' guwahati_ST_all.txt
sed -i 's/\tNov\t/ 11 /g' karaikal_ST_all.txt
sed -i 's/\tNov\t/ 11 /g' mangalore_ST_all.txt
sed -i 's/\tNov\t/ 11 /g' nagpur_ST_all.txt

sed -i 's/\tDec\t/ 12 /g' delhi_ST_all.txt
sed -i 's/\tDec\t/ 12 /g' guwahati_ST_all.txt
sed -i 's/\tDec\t/ 12 /g' karaikal_ST_all.txt
sed -i 's/\tDec\t/ 12 /g' mangalore_ST_all.txt
sed -i 's/\tDec\t/ 12 /g' nagpur_ST_all.txt
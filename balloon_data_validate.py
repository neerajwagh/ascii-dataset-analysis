# -*- coding: utf-8 -*-
from __future__ import division
from glob2 import glob
import timeit
import pandas as pd
import numpy as np
import re

# TODO: file processing can be made parallel, be careful of concurrent file append to yearly_counts.csv

# SETUP ENVIRONMENT FOR EXECUTION
# ------------------------------
# install python 2.7
# install pip utility
# $ pip install glob2
# $ pip install re
# ------------------------------

# MAKE A COPY OF THE ENTIRE TOP-LEVEL DIRECTORY THAT CONTAINS ALL THE STATION FOLDERS
# INSERT PATH FOR COPIED TOP-LEVEL DIRECTORY HERE, REMOVE THE / AT THE END
DIRECTORY_PATH = "../data/data_validity/karaikal"
DATA_LIMIT = 20000.0
# ------------------------------

list_of_files = glob(DIRECTORY_PATH + "/**/*.csv")
start_time = timeit.time.ctime()
my_cols = ["PRES","HGHT","TEMP","DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"]

# for every data file inside the folder
for file in list_of_files:

	NO_OF_DAYS_DATA_VALID = {
		"Jan" : 0,
		"Feb" : 0,
		"Mar" : 0,
		"Apr" : 0,
		"May" : 0,
		"Jun" : 0,
		"Jul" : 0,
		"Aug" : 0,
		"Sep" : 0,
		"Oct" : 0,
		"Nov" : 0,
		"Dec" : 0
	}

	NO_OF_DAYS_DATA_OBSERVED = {
		"Jan" : 0,
		"Feb" : 0,
		"Mar" : 0,
		"Apr" : 0,
		"May" : 0,
		"Jun" : 0,
		"Jul" : 0,
		"Aug" : 0,
		"Sep" : 0,
		"Oct" : 0,
		"Nov" : 0,
		"Dec" : 0
	}

	# read, remove columns not required
	df = pd.read_csv(file, names=my_cols)
	df.drop(["DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"], axis = 1, inplace = True)

	# remove incomplete data at the beginning of each subset
	incomplete_data = df['TEMP'].isnull()
	separator_row = df['PRES'] > 1500
	df.drop(df.index[incomplete_data & ~separator_row], inplace=True)
	df.reset_index(drop=True, inplace=True)

	# obtain the indices for splitting
	separator_row = df["TEMP"].isnull()
	indices_for_split = np.array(df.index[separator_row]).tolist()

	# split the dataframe at those indices
	dfs = np.split(df, indices_for_split)
	dfs = dfs[1:]

	for subset in dfs:
		row = {}
		# reset indices to find separator at index 0 always
		subset.reset_index(drop=True, inplace=True)
		
		# update observed count based on month
		obs_month = subset.loc[0]['HGHT'].split(' ')[2]
		NO_OF_DAYS_DATA_OBSERVED[obs_month] += 1

		# remove the headers
		subset.drop(subset.index[0], inplace=True)
		subset = subset.astype(np.float64)

		if (subset['HGHT'].empty):
			print "subset empty!"
		max_hgt_in_subset = float(np.array(subset['HGHT'], dtype=np.float64).max())
		
		# check validity of max height, update corresponding month
		if max_hgt_in_subset >= DATA_LIMIT:
			NO_OF_DAYS_DATA_VALID[obs_month] += 1
			
	# entire year counts stored, arrange in a table
	year = file.split('_')[-1].split('.')[0]
	avg_df_rows_list = []
	for month in NO_OF_DAYS_DATA_OBSERVED.keys():
		row = {}
		row["YEAR"] = year
		row["MONTH"] = month	
		row["NO_OF_DAYS_DATA_OBSERVED"] = NO_OF_DAYS_DATA_OBSERVED[month]
		row["NO_OF_DAYS_DATA_VALID"] = NO_OF_DAYS_DATA_VALID[month]
		avg_df_rows_list.append(row)

	# create df for the year, write
	year_df = pd.DataFrame(avg_df_rows_list, columns=["YEAR", "MONTH", "NO_OF_DAYS_DATA_OBSERVED", "NO_OF_DAYS_DATA_VALID"])
	year_df.reset_index(drop=True, inplace=True)
	year_df.to_csv(DIRECTORY_PATH + "/" + year + "_data_counts.csv", index=False, header=True)
	print "counts written: ", file

# successful execution!
print "done!"
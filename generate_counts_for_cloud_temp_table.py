from __future__ import division

import numpy as np
import pandas as pd

columns = [
"st. no.", "dates", 

"cloud_present_lt_255K_3x3grid",
"cloud_present_lt_255K_5x5grid", 
# "cloud_present_lt_255K_7x7grid",

"cloud_present_lt_235K_3x3grid",
"cloud_present_lt_235K_5x5grid", 
# "cloud_present_lt_235K_7x7grid", 

"cloud_present_lt_220K_3x3grid",
"cloud_present_lt_220K_5x5grid", 
# "cloud_present_lt_220K_7x7grid", 

"min_cloud_top_pressure_3x3grid",
"min_cloud_top_pressure_5x5grid"
# "min_cloud_top_pressure_7x7grid"
]

SEASON_MAP = {
	"winter": {
		"months": ["Dec", "Jan", "Feb"]
	},
	"pre-monsoon": {
		"months": ["Mar", "Apr", "May"]
	},
	"monsoon": {
		"months": ["Jun", "Jul", "Aug", "Sep"]
	},
	"post-monsoon": {
		"months": ["Oct", "Nov"]
	}
}

TOTAL_DT_DAYS = {
	"Jan" : 0,
	"Feb" : 0,
	"Mar" : 0,
	"Apr" : 0,
	"May" : 0,
	"Jun" : 0,
	"Jul" : 0,
	"Aug" : 0,
	"Sep" : 0,
	"Oct" : 0,
	"Nov" : 0,
	"Dec" : 0
}

VERY_DCC_DAYS = {
	"Jan" : 0,
	"Feb" : 0,
	"Mar" : 0,
	"Apr" : 0,
	"May" : 0,
	"Jun" : 0,
	"Jul" : 0,
	"Aug" : 0,
	"Sep" : 0,
	"Oct" : 0,
	"Nov" : 0,
	"Dec" : 0
}

DCC_DAYS = {
	"Jan" : 0,
	"Feb" : 0,
	"Mar" : 0,
	"Apr" : 0,
	"May" : 0,
	"Jun" : 0,
	"Jul" : 0,
	"Aug" : 0,
	"Sep" : 0,
	"Oct" : 0,
	"Nov" : 0,
	"Dec" : 0
}

BKG_CLOUDINESS_DAYS = {
	"Jan" : 0,
	"Feb" : 0,
	"Mar" : 0,
	"Apr" : 0,
	"May" : 0,
	"Jun" : 0,
	"Jul" : 0,
	"Aug" : 0,
	"Sep" : 0,
	"Oct" : 0,
	"Nov" : 0,
	"Dec" : 0
}

# use only the 5x5 grid columns, drop rows that have "na_file"
station = "delhi"
df = pd.read_csv("./../data/computed_tropopause_data_NEW/" + station + "_all_data_cloudtop_check_NEW.csv", na_values=["na_file", "na_data"])
# df = pd.read_csv("./../data/computed_tropopause_data_ST/" + station + "_all_ST_data_cloudtop_check.csv", usecols=[0,1,2,4,6], na_values=["na_file"])
df.dropna(inplace=True)
# new_ST_df = pd.read_csv("./../data/computed_tropopause_data_ST/" + station + "_new_ST_try.csv", usecols=[0])
# new_ST_set = set(new_ST_df["dates"])

for row in df[1:].itertuples():

	DT_day = getattr(row, "dates")

	# # skip the dates that occur in both ST and DT all_data.csv 
	# if DT_day.split('Z ')[1] in new_ST_set:
	# 	continue

	month = DT_day.split(' ')[2]

	# skip days where minimum CTP is greater than 400hPa
	if (getattr(row, "min_cloud_top_pressure_3x3grid") > 400.0):
		continue

	# VDCC = <220 YES
	if (int(getattr(row, "cloud_present_lt_220K_3x3grid")) == 1):
		VERY_DCC_DAYS[month] += 1
	# DCC = <235 YES and <220 NO
	if (int(getattr(row, "cloud_present_lt_235K_3x3grid")) == 1 and int(getattr(row, "cloud_present_lt_220K_3x3grid")) == 0):
		DCC_DAYS[month] += 1

	# BCC = <255 YES and <235 NO
	if (int(getattr(row, "cloud_present_lt_255K_3x3grid")) == 1 and int(getattr(row, "cloud_present_lt_235K_3x3grid")) == 0):
		BKG_CLOUDINESS_DAYS[month] += 1

	TOTAL_DT_DAYS[month] += 1

# aggregate monthly counts according to seasons
print "\n\nfor station: ", station
print "--------------------"
for i in ["winter", "pre-monsoon", "monsoon", "post-monsoon"]:
	list_of_months_in_season = SEASON_MAP[i]["months"]
	
	very_dcc_seasonal_count = 0
	for month in list_of_months_in_season:
		very_dcc_seasonal_count += VERY_DCC_DAYS[month]
	
	dcc_seasonal_count = 0
	for month in list_of_months_in_season:
		dcc_seasonal_count += DCC_DAYS[month]
	
	bkg_cloudiness_seasonal_count = 0
	for month in list_of_months_in_season:
		bkg_cloudiness_seasonal_count += BKG_CLOUDINESS_DAYS[month]
	
	dt_days_seasonal_count = 0
	for month in list_of_months_in_season:
		dt_days_seasonal_count += TOTAL_DT_DAYS[month]
	
	print "\nfor season: ", i
	print "--------------------"
	print "very_dcc: ", very_dcc_seasonal_count/dt_days_seasonal_count*100
	print "dcc: ", dcc_seasonal_count/dt_days_seasonal_count*100
	print "bkg_cloudiness: ", bkg_cloudiness_seasonal_count/dt_days_seasonal_count*100
	print "total_dt_days: ", dt_days_seasonal_count
	print "--------------------"
	
# The MODIS cloud mask is a science data product that will be produced regularly as an
# Earth Observing System (EOS) standard product. It’s main purpose is to identify scenes
# where land, ocean and atmosphere products should be retrieved based upon the amount of
# obstruction of the surface due to clouds and thick aerosol.

# The Cloud_Mask SDS actually contains the 48 bit cloud mask product

# To use the mask effectively, you must understand your tolerance for cloud or clear when
# performing a retrieval. Once that decision has been made, look over the product
# description to see which bits you will need to extract from the file.

# ./h4tonccf ../data/original/CloudSat/2016183002249_54129_CS_2B-GEOPROF_GRANULE_P_R04_E06.hdf

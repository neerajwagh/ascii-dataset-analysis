# -*- coding: utf-8 -*-
from __future__ import division

import os.path
import time

import numpy as np
import pandas as pd
from glob2 import glob
from netCDF4 import Dataset as NetCDFFile

# # https://github.com/ray-project/ray
# import ray
# # https://blog.dominodatalab.com/simple-parallelization/
# from joblib import Parallel, delayed

# https://stackoverflow.com/questions/582336/how-can-you-profile-a-script
# FIXME: too slow! takes 30s for one row of the csv if AIRS + TRMM + PV scanned. TOO DAMN SLOW! Bottleneck?
# https://stackoverflow.com/questions/20548628/how-to-do-parallel-programming-in-python
# FIXME: parallelize scans!

# DIRECTORY_PATH = "../data/computed_tropopause_data_NEW"
DIRECTORY_PATH = "../data/computed_tropopause_data_ST"

# if % of NA grids among total grids scanned is greater than 65, report as NA
NA_PERCENT_TOLERANCE = 65

# = 2 means a grid of 5x5 cells with the station/city in the centre cell
# since AIRS data is at 1 degree resolution, we're scanning 2 degrees away from station = 200km, in all directions
# NO_OF_ADJACENT_GRIDS_TO_SCAN_ = 1
NO_OF_ADJACENT_GRIDS_TO_SCAN = 2
NO_OF_ADJACENT_GRIDS_TO_SCAN_NEW = 1

# for presence of very deep convective cloud	
TEMPERATURE_THRESHOLD_VDCC = 220.0
# for presence of deep convective cloud
TEMPERATURE_THRESHOLD_DCC = 235.0
# for presence of background convective cloudiness
TEMPERATURE_THRESHOLD_BCC = 255.0
out_columns=[
"st. no.", "dates", 

"cloud_present_lt_255K_3x3grid",
"cloud_present_lt_255K_5x5grid", 
# "cloud_present_lt_255K_7x7grid", 

"cloud_present_lt_235K_3x3grid",
"cloud_present_lt_235K_5x5grid", 
# "cloud_present_lt_235K_7x7grid", 

"cloud_present_lt_220K_3x3grid",
"cloud_present_lt_220K_5x5grid", 
# "cloud_present_lt_220K_7x7grid", 

"min_cloud_top_pressure_3x3grid",
"min_cloud_top_pressure_5x5grid", 
# "min_cloud_top_pressure_7x7grid",

"min_cloud_top_temp_3x3grid",
"min_cloud_top_temp_5x5grid"
# "min_cloud_top_temp_7x7grid"
]

COORDINATES_MAP = {
	'43285': {
		"name": "mangalore",
		"lat": 12.9141,
		"lon": 74.8560
	},
	# vidd
	'42182': {
		"name": "delhi",
		"lat": 28.7041,
		"lon": 77.1025
	},

	'43346': {
		"name": "karaikal",
		"lat": 10.9254,
		"lon": 79.8380
	},
	# vanp
	'42867': {
		"name": "nagpur",
		"lat": 21.1458,
		"lon": 79.0882
	},
	# vegt
	'42410': {
		"name": "guwahati",
		"lat": 26.1445,
		"lon": 91.7362
	}

}

MONTH_MAP = {
	'Jan' : '01',
	'Feb' : '02',
	'Mar' : '03',
	'Apr' : '04',
	'May' : '05',
	'Jun' : '06',
	'Jul' : '07',
	'Aug' : '08',
	'Sep' : '09',
	'Oct' : '10',
	'Nov' : '11',
	'Dec' : '12'
}

# list_of_files = glob(DIRECTORY_PATH + "/*_all_data.csv")
list_of_files = glob(DIRECTORY_PATH + "/*_all_ST_data.csv")


for file in list_of_files:

	df = pd.read_csv(file)
	# extract station from file
	STATION_ID = str(int(df['st. no.'][0]))

	new_ST_df = pd.read_csv("./../data/computed_tropopause_data_ST/" + COORDINATES_MAP[STATION_ID]["name"] + "_new_ST_try.csv", usecols=[0])
	new_ST_set = set(new_ST_df["dates"])

	# fetch station coordinates
	search_for_lat = COORDINATES_MAP[STATION_ID]["lat"]
	search_for_lon = COORDINATES_MAP[STATION_ID]["lon"]

	rows_list = []
	# for every DT day in file
	for row in df.itertuples():
		date = getattr(row, "dates")
		
		# since AIRS data is from 2003 onwards, skip DT dates before it
		if int(date.strip().split(' ')[-1]) < 2003:
			continue

		# skip the dates that occur in both ST and DT all_data.csv 
		if date.split('Z ')[1] in new_ST_set:
			continue

		temp = {}
		temp["st. no."] = STATION_ID
		temp["dates"] = date

		# construct corresponding AIRS filename from DT date
		airs_filename_pattern = "AIRS." + date.split(' ')[-1] + "." + MONTH_MAP[date.split(' ')[2]] + "." + date.split(' ')[1]
		airs_available_files = [x for x in glob("./../data/original/AIRS/" + airs_filename_pattern + "*.nc4")]

		if airs_available_files:

			# open nc4 file and query
			nc = NetCDFFile(airs_available_files[0])
			
			lat = nc.variables['Latitude'][:]
			lon = nc.variables['Longitude'][:]
			lon_list = lon.tolist()
			lat_list = lat.tolist()
			
			cloud_top_temp_ascending = nc.variables['CloudTopTemp_A'][:]
			cloud_top_temp_descending = nc.variables['CloudTopTemp_D'][:]
			
			cloud_top_pres_ascending = nc.variables['CloudTopPres_A'][:]
			cloud_top_pres_descending = nc.variables['CloudTopPres_D'][:]
			
			# these define the enclosing grid of above station
			bounding_box_left = 0
			bounding_box_right = 0
			bounding_box_top = 0
			bounding_box_bottom = 0

			for index, i in enumerate(lon_list):
				if search_for_lon > i and search_for_lon < lon_list[index+1]:
					bounding_box_left = index
					bounding_box_right = index + 1
					break

			for index, i in enumerate(lat_list):
				if search_for_lat < i and search_for_lat > lat_list[index+1]:
					bounding_box_bottom = index + 1
					bounding_box_top = index
					break

			# define how much area around the coordinates to scan
			# lat data is decreasing
			lat_range_lower = bounding_box_bottom + NO_OF_ADJACENT_GRIDS_TO_SCAN
			lat_range_higher = bounding_box_top - NO_OF_ADJACENT_GRIDS_TO_SCAN
			# lon data is increasing 
			lon_range_lower = bounding_box_left - NO_OF_ADJACENT_GRIDS_TO_SCAN
			lon_range_higher = bounding_box_right + NO_OF_ADJACENT_GRIDS_TO_SCAN

			lat_range_lower_new = bounding_box_bottom + NO_OF_ADJACENT_GRIDS_TO_SCAN_NEW
			lat_range_higher_new = bounding_box_top - NO_OF_ADJACENT_GRIDS_TO_SCAN_NEW
			# lon data is increasing 
			lon_range_lower_new = bounding_box_left - NO_OF_ADJACENT_GRIDS_TO_SCAN_NEW
			lon_range_higher_new = bounding_box_right + NO_OF_ADJACENT_GRIDS_TO_SCAN_NEW

			# CHECK 1: cloud present in 3x3 grid below 255K
			temp["cloud_present_lt_255K_3x3grid"] = 0
			found_flag = False
			for i in range(lat_range_higher_new, lat_range_lower_new + 1):
				for j in range(lon_range_lower_new, lon_range_higher_new + 1):
					if (\
						found_flag == False and \
						((cloud_top_temp_ascending[i,j] != cloud_top_temp_ascending.fill_value and cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD_BCC) or (cloud_top_temp_descending[i,j] != cloud_top_temp_descending.fill_value and cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD_BCC))\
						):
						# print "temp: ", cloud_top_temp_ascending[i,j], cloud_top_temp_descending[i,j]
						# print "temp_type: ", type(cloud_top_temp_ascending[i,j]), type(cloud_top_temp_descending[i,j])
						found_flag = True
						temp["cloud_present_lt_255K_3x3grid"] = 1

			# CHECK 1: cloud present in 5x5 grid below 255K
			temp["cloud_present_lt_255K_5x5grid"] = 0
			found_flag = False
			for i in range(lat_range_higher, lat_range_lower + 1):
				for j in range(lon_range_lower, lon_range_higher + 1):
					if (\
						found_flag == False and \
						((cloud_top_temp_ascending[i,j] != cloud_top_temp_ascending.fill_value and cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD_BCC) or (cloud_top_temp_descending[i,j] != cloud_top_temp_descending.fill_value and cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD_BCC))\
						):
						# print "temp: ", cloud_top_temp_ascending[i,j], cloud_top_temp_descending[i,j]
						# print "temp_type: ", type(cloud_top_temp_ascending[i,j]), type(cloud_top_temp_descending[i,j])
						found_flag = True
						temp["cloud_present_lt_255K_5x5grid"] = 1

			# # CHECK 2: cloud present in 7x7 grid below 255K
			# temp["cloud_present_lt_255K_7x7grid"] = 0
			# found_flag = False
			# for i in range(lat_range_higher_new, lat_range_lower_new + 1):
			# 	for j in range(lon_range_lower_new, lon_range_higher_new + 1):
			# 		if (\
			# 			found_flag == False and \
			# 			((cloud_top_temp_ascending[i,j] != cloud_top_temp_ascending.fill_value and cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD_BCC) or (cloud_top_temp_descending[i,j] != cloud_top_temp_descending.fill_value and cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD_BCC))\
			# 			):
			# 			found_flag = True
			# 			temp["cloud_present_lt_255K_7x7grid"] = 1


			# CHECK 3: cloud present in 3x3 grid below 235K
			temp["cloud_present_lt_235K_3x3grid"] = 0
			found_flag = False
			for i in range(lat_range_higher_new, lat_range_lower_new + 1):
				for j in range(lon_range_lower_new, lon_range_higher_new + 1):
					if (\
						found_flag == False and \
						((cloud_top_temp_ascending[i,j] != cloud_top_temp_ascending.fill_value and cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD_DCC) or (cloud_top_temp_descending[i,j] != cloud_top_temp_descending.fill_value and cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD_DCC))\
						):
						found_flag = True
						temp["cloud_present_lt_235K_3x3grid"] = 1

			# CHECK 3: cloud present in 5x5 grid below 235K
			temp["cloud_present_lt_235K_5x5grid"] = 0
			found_flag = False
			for i in range(lat_range_higher, lat_range_lower + 1):
				for j in range(lon_range_lower, lon_range_higher + 1):
					if (\
						found_flag == False and \
						((cloud_top_temp_ascending[i,j] != cloud_top_temp_ascending.fill_value and cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD_DCC) or (cloud_top_temp_descending[i,j] != cloud_top_temp_descending.fill_value and cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD_DCC))\
						):
						found_flag = True
						temp["cloud_present_lt_235K_5x5grid"] = 1

			# # CHECK 4: cloud present in 7x7 grid below 235K
			# temp["cloud_present_lt_235K_7x7grid"] = 0
			# found_flag = False
			# for i in range(lat_range_higher_new, lat_range_lower_new + 1):
			# 	for j in range(lon_range_lower_new, lon_range_higher_new + 1):
			# 		if (\
			# 			found_flag == False and \
			# 			((cloud_top_temp_ascending[i,j] != cloud_top_temp_ascending.fill_value and cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD_DCC) or (cloud_top_temp_descending[i,j] != cloud_top_temp_descending.fill_value and cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD_DCC))\
			# 			):
			# 			found_flag = True
			# 			temp["cloud_present_lt_235K_7x7grid"] = 1


			# CHECK 5: cloud present in 3x3 grid below 220K
			temp["cloud_present_lt_220K_3x3grid"] = 0
			found_flag = False
			for i in range(lat_range_higher_new, lat_range_lower_new + 1):
				for j in range(lon_range_lower_new, lon_range_higher_new + 1):
					if (\
						found_flag == False and \
						((cloud_top_temp_ascending[i,j] != cloud_top_temp_ascending.fill_value and cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD_VDCC) or (cloud_top_temp_descending[i,j] != cloud_top_temp_descending.fill_value and cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD_VDCC))\
						):
						found_flag = True
						temp["cloud_present_lt_220K_3x3grid"] = 1


			# CHECK 5: cloud present in 5x5 grid below 220K
			temp["cloud_present_lt_220K_5x5grid"] = 0
			found_flag = False
			for i in range(lat_range_higher, lat_range_lower + 1):
				for j in range(lon_range_lower, lon_range_higher + 1):
					if (\
						found_flag == False and \
						((cloud_top_temp_ascending[i,j] != cloud_top_temp_ascending.fill_value and cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD_VDCC) or (cloud_top_temp_descending[i,j] != cloud_top_temp_descending.fill_value and cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD_VDCC))\
						):
						found_flag = True
						temp["cloud_present_lt_220K_5x5grid"] = 1

			# # CHECK 6: cloud present in 7x7 grid below 220K
			# temp["cloud_present_lt_220K_7x7grid"] = 0
			# found_flag = False
			# for i in range(lat_range_higher_new, lat_range_lower_new + 1):
			# 	for j in range(lon_range_lower_new, lon_range_higher_new + 1):
			# 		if (\
			# 			found_flag == False and \
			# 			((cloud_top_temp_ascending[i,j] != cloud_top_temp_ascending.fill_value and cloud_top_temp_ascending[i,j] < TEMPERATURE_THRESHOLD_VDCC) or (cloud_top_temp_descending[i,j] != cloud_top_temp_descending.fill_value and cloud_top_temp_descending[i,j] < TEMPERATURE_THRESHOLD_VDCC))\
			# 			):
			# 			found_flag = True
			# 			temp["cloud_present_lt_220K_7x7grid"] = 1

			# CHECK 7: minimum cloup top pressure in 3x3 grid
			min_pressure = min(cloud_top_pres_ascending[lat_range_higher_new:lat_range_lower_new+1, lon_range_lower_new:lon_range_higher_new+1].min(), cloud_top_pres_descending[lat_range_higher_new:lat_range_lower_new+1, lon_range_lower_new:lon_range_higher_new+1].min())
			if type(min_pressure) is np.float32:
				temp["min_cloud_top_pressure_3x3grid"] = min_pressure
			else:
				temp["min_cloud_top_pressure_3x3grid"] = "na_data"

			# CHECK 7: minimum cloup top pressure in 5x5 grid
			min_pressure = min(cloud_top_pres_ascending[lat_range_higher:lat_range_lower+1, lon_range_lower:lon_range_higher+1].min(), cloud_top_pres_descending[lat_range_higher:lat_range_lower+1, lon_range_lower:lon_range_higher+1].min())
			if type(min_pressure) is np.float32:
				temp["min_cloud_top_pressure_5x5grid"] = min_pressure
			else:
				temp["min_cloud_top_pressure_5x5grid"] = "na_data"

			# # CHECK 8: minimum cloup top pressure in 7x7 grid
			# min_pressure = min(cloud_top_pres_ascending[lat_range_higher_new:lat_range_lower_new+1, lon_range_lower_new:lon_range_higher_new+1].min(), cloud_top_pres_descending[lat_range_higher_new:lat_range_lower_new+1, lon_range_lower_new:lon_range_higher_new+1].min())
			# if type(min_pressure) is np.float32:
			# 	temp["min_cloud_top_pressure_7x7grid"] = min_pressure
			# else:
			# 	temp["min_cloud_top_pressure_7x7grid"] = "na_data"


			# CHECK 9: minimum cloup top temperature in 3x3 grid
			min_temp = min(cloud_top_temp_ascending[lat_range_higher_new:lat_range_lower_new+1, lon_range_lower_new:lon_range_higher_new+1].min(), cloud_top_temp_descending[lat_range_higher_new:lat_range_lower_new+1, lon_range_lower_new:lon_range_higher_new+1].min())
			if type(min_temp) is np.float32:
				temp["min_cloud_top_temp_3x3grid"] = min_temp
			else:
				temp["min_cloud_top_temp_3x3grid"] = "na_data"


			# CHECK 9: minimum cloup top temperature in 5x5 grid
			min_temp = min(cloud_top_temp_ascending[lat_range_higher:lat_range_lower+1, lon_range_lower:lon_range_higher+1].min(), cloud_top_temp_descending[lat_range_higher:lat_range_lower+1, lon_range_lower:lon_range_higher+1].min())
			if type(min_temp) is np.float32:
				temp["min_cloud_top_temp_5x5grid"] = min_temp
			else:
				temp["min_cloud_top_temp_5x5grid"] = "na_data"

			# # CHECK 10: minimum cloup top temperature in 7x7 grid
			# min_temp = min(cloud_top_temp_ascending[lat_range_higher_new:lat_range_lower_new+1, lon_range_lower_new:lon_range_higher_new+1].min(), cloud_top_temp_descending[lat_range_higher_new:lat_range_lower_new+1, lon_range_lower_new:lon_range_higher_new+1].min())
			# if type(min_temp) is np.float32:
			# 	temp["min_cloud_top_temp_7x7grid"] = min_temp
			# else:
			# 	temp["min_cloud_top_temp_7x7grid"] = "na_data"

		# FIXME: no corresponding AIRS file found - download remaining AIRS files again!
		else:
			for col_name in out_columns[2:]:
				temp[col_name] = "na_file"
		
		rows_list.append(temp)
		print "ST day processed:", date
		print "---------------------------------------------------"

	# write rows_list to csv
	out_df = pd.DataFrame(rows_list, columns=out_columns)
	# print "% of cloud presence in available data: ", out_df["cloud_present"].value_counts(1)['1']
	out_df.to_csv(DIRECTORY_PATH + "/" + file.split('/')[-1].split('.')[0] + "_cloudtop_check_NEW.csv", index=False, header=True)
	print "cloud top check complete for station:", file

print "all stations done!"
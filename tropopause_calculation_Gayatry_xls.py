# -*- coding: utf-8 -*-
from __future__ import division
from glob2 import glob
import timeit
import pandas as pd
import numpy as np
import re

# SETUP ENVIRONMENT FOR EXECUTION
# ------------------------------
# install python 2.7
# install pip utility
# $ pip install glob2
# $ pip install re
# ------------------------------
# INSERT PATH FOR DATA DIRECTORY HERE, REMOVE THE / AT THE END
DIRECTORY_PATH = "../data/copied_dir"
INTERPOLATE_EVERY_X_METRES = 100.0
# degrees celcius
THRESHOLD = 2.00
# ------------------------------

list_of_files = glob(DIRECTORY_PATH + "/**/*.xls")
start_time = timeit.time.ctime()
heights = np.arange(10000, 27000, INTERPOLATE_EVERY_X_METRES)
DIV_FACTOR = INTERPOLATE_EVERY_X_METRES / 1000.0
SCALE_FACTOR = int(1000.0 / INTERPOLATE_EVERY_X_METRES)

# for every data file inside the folder
for file in list_of_files:

	df = pd.ExcelFile(file).parse('Sheet1')

	# only 10 days in xls file
	for i in range(0,10):

		# isolate df for one day - 3 columns at a time
		day_df = df.iloc[:, (3*i) : (3*i) + 3]
		day_df = day_df.dropna()
		day_df = day_df.drop(day_df.index[0])
		day_df = day_df.astype(np.float64)
		day_df.reset_index(drop=True, inplace=True)
		
		# construct date from column names
		columns_list = day_df.columns.tolist()
		month = "Aug"
		day = str(columns_list[0].day)
		year = str(2016)
		output_file_name = day + "_" + month + "_" + year + ".csv"

		# column order : altitude (m), pressure (mb), temperature (K)
		day_df.columns.values[0] = "height"
		day_df.columns.values[1] = "pressure"
		day_df.columns.values[2] = "temperature"
		
		# find max recorded height in data, do not consider heights above this
		# raw data in K, convert to degC
		day_df["temperature"] = day_df["temperature"] - 273.15
		max_hgt_in_subset = float(np.array(day_df['height'], dtype=np.float64).max())
		
		# handle the data
		hgt_list = []
		avg_temp_list = []

		for index, hgt in enumerate(heights[:-1]):
			if max_hgt_in_subset > hgt:
				height_range = day_df['height'].between(hgt, heights[index+1], inclusive=False)
				hgt_list.append(hgt)
				avg_temp_list.append(day_df[height_range]['temperature'].mean())

		# interpolate to remove NaNs in avg_temp, restrain decimal digits
		avg_temp_list = pd.Series(avg_temp_list).interpolate(method='linear').tolist()
		avg_temp_list = [format(x, '.2f') for x in avg_temp_list]
		
		lapse_rate_list = []
		for index, hgt in enumerate(hgt_list):
			if (index > 0):
				temp_diff = ((float(avg_temp_list[index]) - float(avg_temp_list[index-1])) / DIV_FACTOR) * -1
				temp_diff = float(format(temp_diff, '.2f'))
			else:
				temp_diff = ''
			lapse_rate_list.append(temp_diff)

		# ignore the first blank '' in the list for testing
		lapse_rate_for_testing = lapse_rate_list[1:]

		# analyze lapse rate list for the expected pattern
		# THRESHOLD IS A GLOBAL VARIABLE

		# TROPOPAUSE DEFINITION
		# (1) the first tropopause is defined as the lowest level at which the lapse rate Y (defined as -dT/dz) decreases to 2degCkm-1 or less, provided that the average lapse rate between this level and all higher levels within 2 km does not exceed 2degCkm-1, and (2) if above the first tropopause the average lapse rate between any level and all higher levels within 1 km exceeds 3degCkm-1, then a second tropopause is defined under the same criterion as under (1). The second tropopause may be either within or above the 1 km layer discussed in step 2. 
		
		# tests for first tropopause
		between_0_and_THRESHOLD = [(x <= THRESHOLD) for x in lapse_rate_for_testing]
		try:
			lapse_rate_between_0_and_THRESHOLD_first_occurence = between_0_and_THRESHOLD.index(True)
			lapse_rate_between_0_and_THRESHOLD_occurence = True
		except ValueError:
			lapse_rate_between_0_and_THRESHOLD_occurence = False

		avg_lapse_rate_2km_after_first_occurence_less_than_equal_THRESHOLD = False
		if lapse_rate_between_0_and_THRESHOLD_occurence:
			two_km_above = lapse_rate_between_0_and_THRESHOLD_first_occurence + 2 * SCALE_FACTOR	
			if two_km_above <= len(lapse_rate_for_testing) - 1:
				avg_rate = np.array(lapse_rate_for_testing[lapse_rate_between_0_and_THRESHOLD_first_occurence + 1 : two_km_above + 1]).mean()
				if avg_rate <= THRESHOLD:
					avg_lapse_rate_2km_after_first_occurence_less_than_equal_THRESHOLD = True

		# tests for second tropopause		
		lapse_rate_greater_than_3_occurence = False
		if lapse_rate_between_0_and_THRESHOLD_occurence:
			for i in range(lapse_rate_between_0_and_THRESHOLD_first_occurence + 1, len(lapse_rate_for_testing) - SCALE_FACTOR + 1): 
				avg_rate = np.array(lapse_rate_for_testing[i : i + SCALE_FACTOR]).mean()
				# FIXME: i : i + 2*SCALE_FACTOR?
				if avg_rate > 3.0:
					lapse_rate_greater_than_3_first_occurence = i
					lapse_rate_greater_than_3_occurence = True
					break

		lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_within_3km_occurence = False
		lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence = 0
		if lapse_rate_greater_than_3_occurence:
			one_km_above = lapse_rate_greater_than_3_first_occurence + 1 * SCALE_FACTOR
			two_km_above = lapse_rate_greater_than_3_first_occurence + 2 * SCALE_FACTOR
			three_km_above = lapse_rate_greater_than_3_first_occurence + 3 * SCALE_FACTOR
			# within_3km is sensitive to how much height data is available after greater_than_3_first_occurence
			if ( (three_km_above <= len(lapse_rate_for_testing) - 1) ):
				less_than_THRESHOLD = [(x < THRESHOLD) for x in lapse_rate_for_testing[lapse_rate_greater_than_3_first_occurence + 1: three_km_above + 1]]
				if less_than_THRESHOLD.count(True) > 0: 
					lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_within_3km_occurence = True
					lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence = lapse_rate_greater_than_3_first_occurence + less_than_THRESHOLD.index(True) + 1
			elif ( (two_km_above <= len(lapse_rate_for_testing) - 1) ):
				less_than_THRESHOLD = [(x < THRESHOLD) for x in lapse_rate_for_testing[lapse_rate_greater_than_3_first_occurence + 1: two_km_above + 1]] 
				if less_than_THRESHOLD.count(True) > 0: 
					lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_within_3km_occurence = True
					lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence = lapse_rate_greater_than_3_first_occurence + less_than_THRESHOLD.index(True) + 1
			elif ( (one_km_above <= len(lapse_rate_for_testing) - 1) ):
				less_than_THRESHOLD = [(x < THRESHOLD) for x in lapse_rate_for_testing[lapse_rate_greater_than_3_first_occurence + 1: one_km_above + 1]]
				if less_than_THRESHOLD.count(True) > 0: 
					lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_within_3km_occurence = True
					lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence = lapse_rate_greater_than_3_first_occurence + less_than_THRESHOLD.index(True) + 1

		final_avg_lapse_rate_is_less_than_THRESHOLD = False
		if lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence != 0:
			if (lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence + 2*SCALE_FACTOR) <= (len(lapse_rate_for_testing) - 1):
				# enough data to take average for next 2kms
				avg_rate = np.array(lapse_rate_for_testing[lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence + 1: lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence + 2*SCALE_FACTOR + 1]).mean()
				if avg_rate <= THRESHOLD:
					final_avg_lapse_rate_is_less_than_THRESHOLD = True
			else:
				# report true even if less data available, acceptable
				final_avg_lapse_rate_is_less_than_THRESHOLD = True

		if (
			# confirm first tropopause
			lapse_rate_between_0_and_THRESHOLD_occurence and 
			avg_lapse_rate_2km_after_first_occurence_less_than_equal_THRESHOLD and

			# confirm second tropopause
			lapse_rate_greater_than_3_occurence and
			lapse_rate_greater_than_3_first_occurence > lapse_rate_between_0_and_THRESHOLD_first_occurence and
			
			lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_within_3km_occurence and
			lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence > lapse_rate_greater_than_3_first_occurence and
			final_avg_lapse_rate_is_less_than_THRESHOLD
			):
			
			# record levels at which first and second tropopause were confirmed
			avg_df_rows_list = []

			data_row_first = {}
			data_row_first["height"] = hgt_list[lapse_rate_between_0_and_THRESHOLD_first_occurence + 1]
			data_row_first["average_temperature"] = avg_temp_list[lapse_rate_between_0_and_THRESHOLD_first_occurence + 1]
			avg_df_rows_list.append(data_row_first)
			
			data_row_second = {}
			data_row_second["height"] = hgt_list[lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence + 1]
			data_row_second["average_temperature"] = avg_temp_list[lapse_rate_after_greater_than_3_is_less_than_THRESHOLD_first_occurence + 1]
			avg_df_rows_list.append(data_row_second)

			# create new df from the accumulated rows, write to file
			avg_df = pd.DataFrame(avg_df_rows_list, columns=["height", "average_temperature"])
			avg_df.reset_index(drop=True, inplace=True)
			avg_df.to_csv("../data/copied_dir/" + output_file_name,  mode='a', index=False, header=True)
			print "tropopause found on (file written) : ", output_file_name

		else:
			print "no tropopause found on : ", output_file_name

# successful execution!
print "PROGRAM START: ", start_time
print "PROGRAM END: ", timeit.time.ctime()
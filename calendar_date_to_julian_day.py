import datetime
from calendar import isleap

station_name = "nagpur"
INPUT_FILE = "../data/for_rajib_NEW/for_WV/" + station_name + "_DT_all.txt"
OUTPUT_FILE = "../data/for_rajib_NEW/for_WV/" + station_name + "_DT_all_julian.txt"

MONTH_MAP = {
	'Jan' : '01',
	'Feb' : '02',
	'Mar' : '03',
	'Apr' : '04',
	'May' : '05',
	'Jun' : '06',
	'Jul' : '07',
	'Aug' : '08',
	'Sep' : '09',
	'Oct' : '10',
	'Nov' : '11',
	'Dec' : '12'
}

# calculate offset since 1979
def calculate_offset(year):
	offset = 0
	for previous_year in range(1979, year):
		if isleap(previous_year):
			offset += 366
		else:
			offset += 365
	return offset

input_file = open(INPUT_FILE, 'r')
output_file = open(OUTPUT_FILE, 'w')

for dt_day in input_file:
	
	# data file is from 1979 to 2016 only, skip others
	year = int(dt_day.split(' ')[2])
	if year < 1979 or year > 2016:
		continue

	date = dt_day.split(' ')[0] + "-" + MONTH_MAP[dt_day.split(' ')[1]] + "-" + str(year)
	print date
	day_of_year = datetime.datetime.strptime(date, '%d-%m-%Y').timetuple().tm_yday

	offset = calculate_offset(year)	
	index = offset + day_of_year

	# print to file
	output_file.write(str(index) + "\n")

input_file.close()
output_file.close()
print "done!"
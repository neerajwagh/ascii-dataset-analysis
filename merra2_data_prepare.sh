#!/bin/bash 

# module load cdo/1.8.1_netcdf

for year in `seq 1995 1 2017`
do
    # Aug
    regex="ls *."$year"08*"
    cdo mergetime `$regex` dailym_MERRA2_SH_Aug$year.nc
    # Dec
    regex="ls *."$year"12*"
    cdo mergetime `$regex` dailym_MERRA2_SH_Dec$year.nc
    echo "$year done"
done

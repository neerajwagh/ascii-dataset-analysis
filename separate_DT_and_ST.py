from glob2 import glob
import commands
from os import system

# USE FOR SEPARATING TES AURA H20 FILES

station = "nagpur"
DIRECTORY_PATH = "/home/neeraj/Downloads/TES-Aura_WaterVapor/l5ftl01.larc.nasa.gov/tesl1l2l3/TES/TL3H2OD.005"

list_of_files = glob(DIRECTORY_PATH + "/*")

for folder in list_of_files:
	
	# TES Aura name : year.month.day
	day = folder.split('/')[-1].split('.')[2]
	month = folder.split('/')[-1].split('.')[1]
	year = folder.split('/')[-1].split('.')[0]
	tes_aura_name = year + " " + month + " " + day

	# for_rajib txt files: day month year
	day_in_file = day + " " + month + " " + year
	
	# ---------------- ST CHECK ------------------
	shell_command = "cat ../data/for_rajib_NEW/only_ST_all/unique_" + station + "_ST_all.txt | grep '" + day_in_file + "'"
	return_value, output = commands.getstatusoutput(shell_command)
	if output != "":
		# move to ST folder of station
		print "date: ", folder.split('/')[-1], output, "ST"
		shell_command = "cp -r " + folder + " /home/neeraj/Downloads/TES-Aura_WaterVapor/" + station +"_ST_days/"
		system(shell_command)
	# --------------------------------------------


	# ---------------- DT CHECK --------------------
	shell_command = "cat ../data/for_rajib_NEW/for_WV/unique_" + station + "_DT_all.txt | grep '" + day_in_file + "'"
	return_value, output = commands.getstatusoutput(shell_command)
	if output != "":
		# move to DT folder of station
		print "date: ", folder.split('/')[-1], output, "DT"
		shell_command = "cp -r " + folder + " /home/neeraj/Downloads/TES-Aura_WaterVapor/" + station + "_DT_days/"
		system(shell_command)
	# --------------------------------------------
	
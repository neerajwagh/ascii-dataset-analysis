#!/usr/bin/perl
#
#  This routine checks the files at the cloudsat site and determines which
#  files have not already been brought in.  
#  
#  Jul 2011 Phil Partain - rearranged structure, updated for new server
#  Jan 2007 Cristian Mitrescu - several modifications
#  adapted from Kim Richardson - NRL Monterey

print "Running cloudsat_ftp.plx script\n";

use Net::FTP;
use Sys::Hostname;
use File::Basename;

#
#  Setup the constants
#
$username = 'rajpol';
$password = 'raj22014';
$remotehost = "ftp1.cloudsat.cira.colostate.edu";
$product = "2B-GEOPROF.P_R04";
# Dates required: 
# MAR 2016 (61-91)
# JUL 2016 (183-213)
# OCT 2016 (275-305)
# NOV 2015 (305-335)
$year = 2015;
$jday_start= 310;
$jday_end = 335;
$localdir = "./../data/original/CloudSat/November_2015/";   #  where to store them locally

#  See the "step" loop below to make local year/jday directories
#  and the option to overwrite existing files.
#........................................


$remotedir = "${product}/${year}/";

#
#  Open the connection to the server
#  Change the Debug to 0 after we are sure of everything.
#
print "Open ftp connection to $remotehost\n";
$ftp = Net::FTP->new($remotehost,
                      Timeout => 180,
                      Passive => 1,
                      Debug => 0) or die "Can't connect: $@\n";

$ftp->login($username,$password)  or die "Couldn't login\n";

$ftp->cwd("$remotedir");

# cd into first dir because will cd out of it for each iteration
$tmpday = $jday_start;
if($tmpday < 100) { $tmpday = "0$tmpday"; }
if($tmpday < 10)  { $tmpday = "0$tmpday"; }
$ftp->cwd("$tmpday");

for ($step = $jday_start;$step <= $jday_end;$step++) {
      $jday = $step;
      if($jday < 100) { $jday = "0$jday"; }
      if($jday < 10)  { $jday = "0$jday"; }

      $wherefrom = "../${jday}/";

      $ftp->cwd("$wherefrom");
      @remote_files = $ftp->dir("*.zip");  # this type only
#       print "@remote_files\n";
      $k = length($remote_files[0]);
      if($k ne 0) {
           print "GET THIS DATE -- JULDAY $jday\n";
           $localdir = "${localdir}";
#          this creates a local dir:  year/jday   -  if you don't want it, comment out the next 4 lines!!!
#          $localdir = "${localdir}${year}";
#          if (! -e $localdir) { mkdir($localdir,0775); }          #  make local year directory
#          $localdir = "${localdir}/${jday}";
#          if (! -e $localdir) { mkdir($localdir,0775); }          #  make local jday directory

          foreach $file (@remote_files) {
             ($permissions,$one,$userid,$usergroup,$filesize,$month,$day,$fn_year,$fn) =
             split(/\s+/,$file);
#            print "remote file: $fn\n";
            @splitname = split(".zip",$fn);
            $localfile = $splitname[0];
            $localfilezip = "${localdir}/${fn}";
            $localfile = "${localdir}/${localfile}";
#              if (!( `ls -1 ${localfile}*`)) {   # this line ensures that you don't overwrite existing data
                                                 # comment out if you want to overwrite (or just delete those files manually)
                print "getting: $localfilezip\n";
                $ftp->binary;
                $ftp->get($fn,$localfilezip);
#              }   # end of - this line ensures that you don't overwrite existing data
          }  # end getting a particular jday
      } else {
         print "NOT AVAILABLE -- JULDAY $jday\n";
      }

}  # end step
$ftp->quit();

from glob2 import glob
import timeit
import re
from sys import exit
from os import rename

# FIXME: DO NOT KNOW HOW MANY SPACES TO SKIP PER COLUMN OF MISSING DATA
# 485.7   6096   -3.5  -13.7  -12.2     45     47   2.75    140     16  331.4  341.1  331.9
# 400.0   7610  -13.3  -24.3  -21.9     39     45   1.35    120     15  337.6  342.7  337.9
# 300.0   9760  -25.9                                        90     16  348.8         348.8
# 250.0  11060  -35.5                                        95     15  353.1         353.1

# gets converted to

# 485.7   6096   -3.5  -13.7  -12.2     45     47   2.75    140     16  331.4  341.1  331.9
# 400.0   7610  -13.3  -24.3  -21.9     39     45   1.35    120     15  337.6  342.7  337.9
# 300.0   9760  -25.9   90     16  348.8    348.8
# 250.0  11060  -35.5   95     15  353.1    353.1


# SETUP ENVIRONMENT FOR EXECUTION
# ------------------------------
# install python 2.7
# install pip utility
# $ pip install glob2
# $ pip install re
# ------------------------------

# HOW TO RUN PROGRAM
# ------------------------------
# INPUT: ASCII Wyoming station data
EXTRACT_THESE_MONTHS = [
						"Jan",
						"Feb",
						"Mar",
						"Apr",
						"May",
						"Jun",
						"Jul",
						"Aug",
						"Sep",
						"Oct",
						"Nov",
						"Dec"
						]
# Make sure these match exactly to phrases in the ascii file
# OUTPUT: corresponding excel file with data from only above-specified months

# MAKE A COPY OF THE ENTIRE TOP-LEVEL DIRECTORY THAT CONTAINS ALL THE STATION FOLDERS
# INSERT PATH FOR COPIED TOP-LEVEL DIRECTORY HERE, REMOVE THE / AT THE END
DIRECTORY_PATH = "./../data/copied_dir/nagpur_ST_all"
# $ python convert_ascii_to_csv.py 
# ** WARNING: USE ONLY FIRST 3 COLUMNS OF OUTPUT CSV! **
# ------------------------------

month_regex = ''
for month in EXTRACT_THESE_MONTHS:
	month_regex += month + '|'
month_regex = month_regex[:-1]

list_of_files = glob(DIRECTORY_PATH + "/**/*.out")
start_time = timeit.time.ctime()

# for every data file inside the folder
for file in list_of_files:

	# open file, read content, close file
	f_in = open(file , 'r')
	file_content = f_in.read()	
	f_in.close()

	# extract station information
	# STATION_ID = re.findall(r"Station\ identifier\:\ (.*[\r|\n]+)", file_content)[0]
	STATION_NUMBER = re.findall(r"Station\ number\:\ (.*[\r|\n]+)", file_content)[0]

	# identify only data between the ----- separator and the phrase 'station information', and only for July and August months
	# TAKES 3-4 SECONDS, OPTIMIZE IF POSSIBLE!
	# VIDD Observations at 00Z 01 Jan 2012
	# -------------------------------------------------------------------------------------------
	# PRES   HGHT   TEMP   DWPT   FRPT   RELH   RELI   MIXR   DRCT   SKNT   THTA   THTE   THTV
	# hPa     m      C      C      C      %      %    g/kg    deg   knot     K      K      K 
	# -------------------------------------------------------------------------------------------
	# 1000.0    131                                                                             
	# 983.0    216   13.0    6.0

	date_regex = re.compile(r"([0-9]{2}Z\ [0-9]{2}\ (" + month_regex + r")\ [0-9]{4})[\r|\n]+\-{5,100}[\r|\n]+[A-Z|\ |a-z|\%|\/|\r|\n]+\-{5,100}[\r|\n]+[0-9|\-|\.|\ |\r|\n]+Station\ information")
	dates_only = date_regex.findall(file_content)
	dates_only = [x[0] for x in dates_only]

	data_regex = re.compile(r"[0-9]{2}Z\ [0-9]{2}\ (" + month_regex + r")\ [0-9]{4}[\r|\n]+\-{5,100}[\r|\n]+[A-Z|\ |a-z|\%|\/|\r|\n]+\-{5,100}[\r|\n]+([0-9|\-|\.|\ |\r|\n]+)Station\ information")
	data_only = data_regex.findall(file_content)
	data_only = [x[1] for x in data_only]
	
	if(len(dates_only) != len(data_only)):
		exit("REGEX ALERT! #DATES CAPTURED DOES NOT MATCH #SUBSETS OF DATA IN ASCII FILE!")

	# open same file for writing
	f_out = open(file, 'w')
	
	# collect all subsets of data together, separated by "STATION_NUMBER 00Z 01 Jan 2012"
	for index, subset in enumerate(data_only):
		
		separator_row = STATION_NUMBER.strip() + "," + dates_only[index] + "\n"
		subset_data_rows = ''		
		rows_of_subset = subset.split('\n')
		for row in rows_of_subset:
			row = row.strip()
			csv_row = re.sub('[\ ]+', ',', row)
			# since we need only height and temp, ignore the other columns

			subset_data_rows += csv_row + '\n'

		final_subset_content = separator_row + subset_data_rows[:-1]
		
		# write subset to csv file
		f_out.write(final_subset_content)
	
	# close file
	f_out.close()
	
	# rename from .out to .csv
	old_file_name = file
	new_file_name = file.split('.out')[0] + '.csv' 
	rename(old_file_name, new_file_name)
	print "conversion to csv complete: ", old_file_name

print "PROGRAM START: ", start_time
print "PROGRAM END: ", timeit.time.ctime()

#!/usr/bin/env python

# https://blog.dominodatalab.com/simple-parallelization/
from joblib import Parallel, delayed
import multiprocessing
import sys
from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()

def download_for_year(year):

	# dates = ["0101", "0201", "0301", "0401", "0501", "0601", "0701", "0801", "0901", "1001", "1101", "1201"]
	# # generate date string for every year
	# year_string = ""
	# year = str(year)
	# for date in dates:
	#     year_string += year + date + "/"
	# year_string = year_string.strip('/')

	# generate date string for every year
	year_string = ""
	year = str(year)
	year_string = year + "-08-01/to/" + year + "-08-31"

	config_dict["date"] = year_string
	config_dict["target"] = year + "_cloud_water_ice_Aug_4xdaily" + ".nc"

	# fetch data
	server.retrieve(config_dict)

	print "---------------------------------------"
	print "year done: ", year
	print "---------------------------------------"
	return

# inspect the MARS request and create object accordingly
config_dict = {
    "class": "ei",
    "dataset": "interim",
    # "date": "2015-08-01/to/2015-08-31",
    "expver": "1",
    "grid": "0.75/0.75",
    "levelist": "1/2/3/5/7/10/20/30/50/70/100/125/150/175/200/225/250/300/350/400/450/500/550/600/650/700/750/775/800/825/850/875/900/925/950/975/1000",
    "levtype": "pl",
    "param": "246.128/247.128",
    "step": "0",
    "stream": "oper",
    "time": "00:00:00/06:00:00/12:00:00/18:00:00",
    "type": "an",
	"format": "netcdf"
	}

# clim years from 1979 to 2017
# num_cores = multiprocessing.cpu_count()
Parallel(n_jobs=3)(delayed(download_for_year)(year) for year in range(1979, 2018))
# start_year = int(sys.argv[1])
# end_year = int(sys.argv[2])

# for year in range(start_year, end_year):
# 	download_for_year(year)
print "all years done!"
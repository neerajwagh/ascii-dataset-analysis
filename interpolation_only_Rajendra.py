# -*- coding: utf-8 -*-
from __future__ import division
from glob2 import glob
import timeit
import pandas as pd
import numpy as np
import re

# SETUP ENVIRONMENT FOR EXECUTION
# ------------------------------
# install python 2.7
# install pip utility
# $ pip install glob2
# $ pip install re
# ------------------------------
# INSERT PATH FOR DATA DIRECTORY HERE, REMOVE THE / AT THE END
DIRECTORY_PATH = "../data/copied_dir"
INTERPOLATE_EVERY_X_METRES = 200.0
# ------------------------------

list_of_files = glob(DIRECTORY_PATH + "/**/*.xlsx")
start_time = timeit.time.ctime()
heights = np.arange(10000, 27000, INTERPOLATE_EVERY_X_METRES)

# for every data file inside the folder
for file in list_of_files:

	output_file_name = file.split('/')[-1].split('.')[0] + ".csv"
	print "processing : ", output_file_name
	
	df = pd.ExcelFile(file).parse('NM')
	df.columns.values[0] = "Altitude"
	df.columns.values[1] = "Temperature"
	df = df[["Altitude", "Temperature"]]
	df = df.dropna()
	df = df.astype(np.float64)
	df.reset_index(drop=True, inplace=True)
	# find max recorded height in data, do not consider heights above this
	df["Altitude"] = df["Altitude"] * 1000
	max_hgt_in_subset = float(np.array(df["Altitude"], dtype=np.float64).max())

	# handle the data
	hgt_list = []
	avg_temp_list = []

	for index, hgt in enumerate(heights[:-1]):
		if max_hgt_in_subset > hgt:
			height_range = df["Altitude"].between(hgt, heights[index+1], inclusive=False)
			hgt_list.append(hgt)
			avg_temp_list.append(df[height_range]['Temperature'].mean())

	# interpolate to remove NaNs in avg_temp, restrain decimal digits
	avg_temp_list = pd.Series(avg_temp_list).interpolate(method='linear').tolist()
	avg_temp_list = [format(x, '.2f') for x in avg_temp_list]

	# bring heights back to km
	# hgt_list = hgt_list / 1000.0

	avg_df_rows_list = []
	for i in range(len(avg_temp_list)):
		x = {}
		x["Altitude"] = hgt_list[i] / 1000.0
		x["Temperature"] = avg_temp_list[i]
		avg_df_rows_list.append(x)
	 
	# create new df from the accumulated rows, write to file
	avg_df = pd.DataFrame(avg_df_rows_list, columns=["Altitude", "Temperature"])
	avg_df.reset_index(drop=True, inplace=True)
	avg_df.to_csv("../data/copied_dir/" + output_file_name, index=False, header=True)

# successful execution!
print "PROGRAM START: ", start_time
print "PROGRAM END: ", timeit.time.ctime()
# -*- coding: utf-8 -*-
from __future__ import division

import os.path
import time
import numpy as np
import pandas as pd

station_list = [
	"Delhi",
	"Nagpur",
	"Guwahati",
	"Mangalore",
	"Karaikal"
	]

months_list = [
	"Aug",
	"Dec"
	]

for station_name in station_list:
	for month in months_list:
		output_file_name = "./Neeraj_timmean_scr_Anom_DTday_" + month + "_" + station_name + ".sh"
		out_fp = open(output_file_name, "w")

		for year in range(2005, 2018):
			year = str(year)
			
			# cdo timmean Neeraj_Delhi_DTDec1979.nc Neeraj_timmean_Delhi_DTDec1979.nc
			command = "cdo timmean Neeraj_" + station_name + "_DT" + month + year + ".nc " + "Neeraj_timmean_" + station_name + "_DT" + month + year + ".nc\n"
			
			out_fp.write(command)

		out_fp.close()
		print "done: ", month, station_name
from __future__ import division
import numpy as np
import pandas as pd
from glob2 import glob

station_name = "guwahati"
DIRECTORY = "../data/computed_tropopause_data/" + station_name + "_all_data.csv"
MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
out_columns = [
	"year",
	"month",
	"no_of_DT_days",
	"no_of_valid_days",
	"percentage"
	]

df = pd.read_csv(DIRECTORY)
rows_list = []
# for all years
for year in range(1973, 2018):
	year = str(year)
	filename = "../data/data_validity/" + station_name + "/" + year + "_data_counts.csv" 
	try:
		df_validity = pd.read_csv(filename)
	except:
		print "year for which validity file absent: ", year
		continue
	# iterate once for every month
	# yearly_dt_total = 0
	# yearly_valid_total = 0
	for month in MONTHS:
		# select rows with only a particular month
		temp_df = df.loc[df['dates'].str.contains(month)]
		# yearly_dt_total = yearly_dt_total + df.loc[df['dates'].str.contains(month + " " + year)].shape[0]
		# yearly_valid_total = yearly_valid_total + int(df_validity.loc[df_validity['MONTH'] == month]["NO_OF_DAYS_DATA_VALID"])
		# add row of month to output df

		temp = {}
		temp["year"] = year
		temp["month"] = month
		# temp["no_of_DT_days"] = yearly_dt_total
		# temp["no_of_valid_days"] = yearly_valid_total
		temp["no_of_DT_days"] = df.loc[df['dates'].str.contains(month + " " + year)].shape[0]
		temp["no_of_valid_days"] = int(df_validity.loc[df_validity['MONTH'] == month]["NO_OF_DAYS_DATA_VALID"])
		try:
			temp["percentage"] = (temp["no_of_DT_days"] / temp["no_of_valid_days"]) * 100.0
			if temp["percentage"] > 100.0:
				continue
		except:
			# temp["percentage"] = "na"
			continue
		rows_list.append(temp)

out_df = pd.DataFrame(rows_list, columns=out_columns)
out_df.to_csv("../data/computed_tropopause_data/" + station_name + "_monthly_trend.csv", index=False, header=True)
print "Monthly trend generated for ", station_name
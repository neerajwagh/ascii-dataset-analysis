# -*- coding: utf-8 -*-
from __future__ import division

import re
import timeit

import numpy as np
import pandas as pd

# http://docs.scipy.org/doc/numpy/reference/generated/numpy.seterr.html
# np.seterr(divide='ignore', invalid='ignore')

# SETUP ENVIRONMENT FOR EXECUTION
# ------------------------------
# install python 2.7
# install pip utility
# $ pip install glob2
# $ pip install re
# ------------------------------
# INSERT PATH FOR DATA DIRECTORY HERE, REMOVE THE / AT THE END
# FILE_PATH = "../data/copied_dir/nainital/COMPILE_Naini.xls"
FILE_PATH = "../data/copied_dir/nainital/COMPILE_Naini_2Aug_3Aug.xls"
OUTPUT_FILENAME = "DT_days_Nainital"
INTERPOLATE_EVERY_X_METRES = 100.0
# degC/km
THRESHOLD = 2.00
# ------------------------------

heights = np.arange(12000, 27000, INTERPOLATE_EVERY_X_METRES)
# heights = np.arange(0, 30000, INTERPOLATE_EVERY_X_METRES)

DIV_FACTOR = INTERPOLATE_EVERY_X_METRES / 1000.0
SCALE_FACTOR = int(1000.0 / INTERPOLATE_EVERY_X_METRES)

# skip first row while reading
df = pd.ExcelFile(FILE_PATH).parse(sheet_name='Sheet1', na_values=["#VALUE!", "#DIV/0!", "NaN"])

# make list of days according to spec sheet
# day_list = ["5Aug2016", "6Aug2016", "8Aug2016", "11aAug2016", "11bAug2016", "12aAug2016", "12bAug2016", "15aAug2016", "15bAug2016", "16Aug2016", "17aAug2016", "17bAug2016", "18aAug2016", "18bAug2016", "19Aug2016", "20aAug2016", "20bAug2016", "21Aug2016", "21bAug2016", "22Aug2016", "23Aug2016", "24Aug2016", "26Aug2016", "28Aug2016", "30Aug2016"]
day_list = ["2Aug2016", "3Aug2016"]

# find indices for empty columns
indices_for_split = []
for index, i in enumerate(df.columns):
	if i.find("Unnamed") == 0:
		indices_for_split.append(index)

# split at empty columns
dfs = np.split(df, indices_for_split, axis=1)

# iterate over data for every day
for day_index, day_df in enumerate(dfs):

	# there's the blank separator column in the df, remove it
	day_df.dropna(axis=1, how='all', inplace=True)
	# remove rows with nan values, rows have unequal length so this is necessary
	day_df.reset_index(drop=True, inplace=True)
	day_df = day_df.astype(np.float64)

	# day_df.columns.values[0] = "pressure"
	# day_df.columns.values[2] = "temperature"
	# day_df.columns.values[3] = "height"

	day_df.columns.values[0] = "height"
	day_df.columns.values[1] = "pressure"
	day_df.columns.values[2] = "temperature"
	day_df["height"] = day_df["height"] * 1000.0

	day_df.dropna(axis=0, how='any', subset=["pressure", "temperature", "height"], inplace=True)
	print "------------------------------------------------"
	# find max recorded height in data, do not consider heights above this
	# check if data is in K, convert to degC
	day_df["temperature"] = day_df["temperature"] - 273.15
	max_hgt_in_subset = float(np.array(day_df['height'], dtype=np.float64).max())

	# handle the data
	hgt_list = []
	avg_temp_list = []

	for index, hgt in enumerate(heights[:-1]):
		if hgt < max_hgt_in_subset:
			height_range = day_df['height'].between(hgt, heights[index+1], inclusive=False)
			hgt_list.append(hgt)
			avg_temp_list.append(day_df[height_range]['temperature'].mean())
	
	# interpolate to remove NaNs in avg_temp, restrain decimal digits
	avg_temp_list = pd.Series(avg_temp_list).interpolate(method='linear').tolist()
	avg_temp_list = [format(x, '.2f') for x in avg_temp_list]
	
	lapse_rate_list = []
	# hgt_list and avg_temp_list have the same lengths
	for index, hgt in enumerate(hgt_list):
		if (index < len(avg_temp_list) - 1):
			# forward difference as per https://en.wikipedia.org/wiki/Finite_difference#Forward,_backward,_and_central_differences
			temp_diff = ((float(avg_temp_list[index+1]) - float(avg_temp_list[index])) / DIV_FACTOR) * -1
			temp_diff = float(format(temp_diff, '.2f'))
		else:
			temp_diff = ''
		lapse_rate_list.append(temp_diff)

	# remove data points with extreme lapse rates (caused due to clouds)
	# valid range in -11 to +11 
	# verify_df = pd.DataFrame({'avg_temperature': avg_temp_list, 'lapse_rate': lapse_rate_list, 'height': hgt_list})
	# verify_df.drop(verify_df.index[(verify_df["lapse_rate"] < -11.0) | (verify_df["lapse_rate"] > 11.0)], inplace=True)
	
	# # update all lists
	# avg_temp_list = verify_df["avg_temperature"].tolist() 
	# lapse_rate_list = verify_df["lapse_rate"].tolist()
	# hgt_list = verify_df["height"].tolist()

	# write to file for manual verification
	verify_df = pd.DataFrame({'avg_temperature': avg_temp_list, 'lapse_rate': lapse_rate_list, 'height': hgt_list})
	verify_df.to_csv("./../data/copied_dir/nainital/" + OUTPUT_FILENAME + "_" + day_list[day_index] + ".csv", index=False, header=True)
	# continue

	# ignore the last blank '' in the list for testing
	lapse_rate_list = lapse_rate_list[:-1]

	# TROPOPAUSE DEFINITION
	# (1) the first tropopause is defined as the lowest level at which the lapse rate Y (defined as -dT/dz) decreases to 2degCkm-1 or less, provided that the average lapse rate between this level and all higher levels within 2 km does not exceed 2degCkm-1, and (2) if above the first tropopause the average lapse rate between any level and all higher levels within 1 km exceeds 3degCkm-1, then a second tropopause is defined under the same criterion as under (1). The second tropopause may be either within or above the 1 km layer discussed in step 2. 

	# REINIT ALL NUMERICAL LOCAL VARIABLES SO 2 PROFILES DON'T GET MIXED UP
	lr_lt_THRESHOLD_fo = -1
	two_km_above = -1
	avg_rate = -1
	avg_lr_gt_3_fo = -1
	second_lrt_fo = -1

	# tests for first tropopause
	# WMO step 1
	lt_THRESHOLD = [(x <= THRESHOLD) for x in lapse_rate_list]
	lr_lt_THRESHOLD_occurs = False
	try:
		# lapse rate less than threshold first occurence 
		lr_lt_THRESHOLD_fo = lt_THRESHOLD.index(True)
		lr_lt_THRESHOLD_occurs = True
	except ValueError:
		# move onto next profile
		print "discarded: LR < 2 never found: ", day_list[day_index]
		continue

	avg_lr_lt_2 = False
	two_km_above = lr_lt_THRESHOLD_fo + 2 * SCALE_FACTOR
	if two_km_above <= len(lapse_rate_list) - 1:
		avg_rate = np.array(lapse_rate_list[lr_lt_THRESHOLD_fo : two_km_above + 1]).mean()
		if avg_rate < 2.0:
			avg_lr_lt_2 = True
		else:
			# move onto next profile
			print "discarded: avg_lr_lt_2 is false: ", day_list[day_index]
			continue
	else:
		# move onto next profile
		print "discarded: not enough data for avg_lr_lt_2: ", day_list[day_index]
		continue

	# tests for second tropopause
	# WMO step 2
	avg_lr_gt_3_occurs = False
	# loop = sliding window of all levels within 1km, upto 1km less than the maximum height available
	for i in range(lr_lt_THRESHOLD_fo + 1, len(lapse_rate_list) - SCALE_FACTOR + 1): 
		avg_rate = np.array(lapse_rate_list[i : i + SCALE_FACTOR]).mean()
		if avg_rate > 3.0:
			avg_lr_gt_3_fo = i
			avg_lr_gt_3_occurs = True
			break

	# WMO step 3
	second_lrt_occurs = False
	if avg_lr_gt_3_occurs:
		for i in range(avg_lr_gt_3_fo, len(lapse_rate_list)):
			if (lapse_rate_list[i] < THRESHOLD):
				two_km_above = i + 2 * SCALE_FACTOR
				if two_km_above <= len(lapse_rate_list) - 1:
					avg_rate = np.array(lapse_rate_list[i : two_km_above + 1]).mean()
					if avg_rate < 2.0:
						second_lrt_fo = i
						second_lrt_occurs = True
						break
				else:
					# DISCARD PROFILE OR TAKE AVERAGE OF WHATEVER AMOUNT OF DATA WE HAVE LEFT?
					print "discarded: not enough data to confirm LRT2: ", day_list[day_index]
					break
	else:
		# move onto next profile
		print "discarded: avg_lr_gt_3 never occurs: ", day_list[day_index]
		continue

	if (
		# confirm WMO step 1
		lr_lt_THRESHOLD_occurs and 
		avg_lr_lt_2 and

		# confirm WMO step 2
		avg_lr_gt_3_occurs and
		
		# confirm WMO step 3
		second_lrt_occurs
		):
	
		# record levels at which first and second tropopause were confirmed
		avg_df_rows_list = []
			
		# record levels at which first and second tropopause were confirmed
		data_row = {}
		data_row["date"] = day_list[day_index]
		data_row["lrt1"] = hgt_list[lr_lt_THRESHOLD_fo]
		data_row["temp1"] = avg_temp_list[lr_lt_THRESHOLD_fo]
		data_row["lrt2"] = hgt_list[second_lrt_fo]
		data_row["temp2"] = avg_temp_list[second_lrt_fo]
		avg_df_rows_list.append(data_row)

		# create new df from the accumulated rows, write to file
		avg_df = pd.DataFrame(avg_df_rows_list, columns=["date", "lrt1", "temp1", "lrt2", "temp2"])
		avg_df.to_csv("./../data/copied_dir/nainital/" + OUTPUT_FILENAME + "_all.csv", mode='a', index=False, header=True)
		print "tropopause found on (written in file) : ", day_list[day_index]

	else:
		print "no tropopause found on : ", day_list[day_index]

# successful execution!
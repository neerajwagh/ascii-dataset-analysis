# -*- coding: utf-8 -*-
from __future__ import division

import numpy as np
import pandas as pd
from glob2 import glob

my_cols = ["PRES","HGHT","TEMP","DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"]
all_files = glob("../data/Wyoming_Balloon_Data_csv_only/mangalore/*.csv")
df = pd.concat((pd.read_csv(f, names=my_cols) for f in all_files))
df.drop(["DWPT","FRPT","RELH","RELI","MIXR","DRCT","SKNT","THTA","THTE","THTV"], axis = 1, inplace = True)
df.dropna(subset=["TEMP"], inplace=True)
df.reset_index(drop=True, inplace=True)
# df.to_csv("./test.csv", index=False, header=True)

df = df.astype(np.float32)
df["HGHT"] = df["HGHT"] / 1000.0

# 220 K = -53.15 degC
# 235 K = -38.15 degC
# 255 K = -18.15 degC

filtered_220K = df[(df["TEMP"] > -53.5) & (df["TEMP"] < -53.0)]
filtered_235K = df[(df["TEMP"] > -38.5) & (df["TEMP"] < -38.0)]
filtered_255K = df[(df["TEMP"] > -18.5) & (df["TEMP"] < -18.0)]

print "avg hght at 220K", filtered_220K["HGHT"].mean()
print "avg hght at 235K", filtered_235K["HGHT"].mean()
print "avg hght at 255K", filtered_255K["HGHT"].mean()
print "--------------------"
print "avg pres at 220K", filtered_220K["PRES"].mean()
print "avg pres at 235K", filtered_235K["PRES"].mean()
print "avg pres at 255K", filtered_255K["PRES"].mean()

# -*- coding: utf-8 -*-
from __future__ import division
from glob2 import glob
from netCDF4 import Dataset as NetCDFFile
import pandas as pd
import numpy as np
import os.path
import time
# # https://github.com/ray-project/ray
# import ray
# # https://blog.dominodatalab.com/simple-parallelization/
# from joblib import Parallel, delayed

# ray.init()

# https://stackoverflow.com/questions/582336/how-can-you-profile-a-script
# FIXME: too slow! takes 30s for one row of the csv. TOO DAMN SLOW! Bottleneck?
# https://stackoverflow.com/questions/20548628/how-to-do-parallel-programming-in-python
# FIXME: parallelize scans!

DIRECTORY_PATH = "../data/copied_dir"
# if % of NA grids among total grids scanned is greater than 65, report as NA
NA_PERCENT_TOLERANCE = 65
# = 2 means a grid of 5x5 cells with the station/city at the centre cell will be scanned
# since AIRS data is at 1 degree resolution, we're scanning 2 degrees away from station = 200km, in all directions
NO_OF_ADJACENT_GRIDS_TO_SCAN = 2
# temperature threshold to check for presence of cloud
TEMPERATURE_THRESHOLD = 258.0
# if greater, Rossby wave breaking confirmed
PV_THRESHOLD = 2.0

# SEASON_MAP = {
#     "winter": {
#         "months": ["Dec", "Jan", "Feb"],
#         "threshold": 270.0,
#         # no of cells around the station/city's bounding grid to check for threshold
#         "no_of_adjacent_grids_to_scan": 1
#     },
#     "pre-monsoon": {
#         "months": ["Mar", "Apr", "May"],
#         "threshold": 270.0,
#         "no_of_adjacent_grids_to_scan": 1
#     },
#     "monsoon": {
#         "months": ["Jun", "Jul", "Aug", "Sep"],
#         "threshold": 270.0,
#         "no_of_adjacent_grids_to_scan": 1
#     },
#     "post-monsoon": {
#         "months": ["Oct", "Nov"],
#         "threshold": 270.0,
#         "no_of_adjacent_grids_to_scan": 1
#     }
# }

COORDINATES_MAP = {
	# '42027' : {
	# 	"name": "srinagar",
	# 	"lat": 34.083656,
	# 	"lon": 74.797371
	# },
	'43346': {
		"name": "karaikal",
		"lat": 10.9254,
		"lon": 79.8380
	},
	'43285': {
		"name": "mangalore",
		"lat": 12.9141,
		"lon": 74.8560
	},
	'42867': {
		"name": "nagpur",
		"lat": 21.1458,
		"lon": 79.0882
	},
	'42410': {
		"name": "guahati",
		"lat": 26.1445,
		"lon": 91.7362
	},
	'42182': {
		"name": "delhi",
		"lat": 28.7041,
		"lon": 77.1025
	}
}

MONTH_MAP = {
	'Jan' : '01',
	'Feb' : '02',
	'Mar' : '03',
	'Apr' : '04',
	'May' : '05',
	'Jun' : '06',
	'Jul' : '07',
	'Aug' : '08',
	'Sep' : '09',
	'Oct' : '10',
	'Nov' : '11',
	'Dec' : '12'
}

list_of_files = glob(DIRECTORY_PATH + "/*.xlsx")
my_cols = ["st. no.", "dates"]

for file in list_of_files:

	df = pd.ExcelFile(file, names=my_cols).parse('All_data')
	STATION_ID = str(int(df['st. no.'][0]))
	dates_list = [str(x) for x in df["dates"].tolist()]

	# fetch station coordinates
	search_for_lat = COORDINATES_MAP[STATION_ID]["lat"]
	search_for_lon = COORDINATES_MAP[STATION_ID]["lon"]
	
	rows_list = []
	for date in dates_list:

		# since PV data is from 1979 onwards, skip tropopause dates before it
		if int(date.strip().split(' ')[-1]) < 1979:
			continue
		
		temp = {}
		temp["st. no."] = STATION_ID
		temp["dates"] = date
		
		break_flag_200hPa = False
		break_flag_250hPa = False

		# report if PV > 2 at 200hPa and 250hPa, if date is Aug
		if "Aug" in date:
			temp["pv>2_at_200hPa"] = 0
			temp["pv>2_at_250hPa"] = 0

			# print time.ctime()
			nc_pv = NetCDFFile("../data/original/ERA_Interim/Potential_Vorticity/" + date.split(' ')[-1] + "_pv_Aug_dailymean.nc")
			
			lat_pv = nc_pv.variables['latitude'][:]
			lon_pv = nc_pv.variables['longitude'][:]
			lev_pv = nc_pv.variables['level'][:]
			lon_pv = lon_pv - 180.0
			lon_list_pv = lon_pv.tolist()
			lat_list_pv = lat_pv.tolist()
			
			pv = nc_pv.variables['pv'][:]
			# use scale_factor and add_offset only while running model
			pv = pv * 1E+06

			bounding_box_left_pv = 0
			bounding_box_right_pv = 0
			bounding_box_top_pv = 0
			bounding_box_bottom_pv = 0

			for index, i in enumerate(lon_list_pv):
				if search_for_lon > i and search_for_lon < lon_list_pv[index+1]:
					bounding_box_left_pv = index
					bounding_box_right_pv = index + 1
					break

			for index, i in enumerate(lat_list_pv):
				if search_for_lat < i and search_for_lat > lat_list_pv[index+1]:
					bounding_box_bottom_pv = index + 1
					bounding_box_top_pv = index
					break

			# define how much area around the coordinates to scan
			# lat data is decreasing
			lat_range_lower_pv = bounding_box_bottom_pv + NO_OF_ADJACENT_GRIDS_TO_SCAN
			lat_range_higher_pv = bounding_box_top_pv - NO_OF_ADJACENT_GRIDS_TO_SCAN
			# lon data is increasing 
			lon_range_lower_pv = bounding_box_left_pv - NO_OF_ADJACENT_GRIDS_TO_SCAN
			lon_range_higher_pv = bounding_box_right_pv + NO_OF_ADJACENT_GRIDS_TO_SCAN

			day = int(date.split(' ')[1])
			time_index = day - 1
			for p in range(lat_range_higher_pv, lat_range_lower_pv + 1):
				for q in range(lon_range_lower_pv, lon_range_higher_pv + 1):
					
					# check PV 3 days before and 3 days after for all DT days
					if time_index >= 3 and time_index < 28:
						for t in range(time_index-3, time_index+3):
							# print "here!"
							if pv[t, lev_pv.tolist().index(200), p, q] > PV_THRESHOLD:
								print "1"
								temp["pv>2_at_200hPa"] = 1
								break_flag_200hPa = True
							if pv[t, lev_pv.tolist().index(250), p, q] > PV_THRESHOLD:
								print "1"
								temp["pv>2_at_250hPa"] = 1
								break_flag_250hPa = True
							if break_flag_200hPa and break_flag_250hPa:
								# within the 5x5 grid, any point works for checking values. no need to check every grid
								break

					elif time_index < 3:
						for t in range(0, 3):
							if pv[t, lev_pv.tolist().index(200), p, q] > PV_THRESHOLD:
								print "1"
								temp["pv>2_at_200hPa"] = 1
								break_flag_200hPa = True
							if pv[t, lev_pv.tolist().index(250), p, q] > PV_THRESHOLD:
								print "1"
								temp["pv>2_at_250hPa"] = 1
								break_flag_250hPa = True
							if break_flag_200hPa and break_flag_250hPa:
								# within the 5x5 grid, any point works for checking values. no need to check every grid
								break

					elif time_index >= 28:
						for t in range(28, 31):
							try:
								if pv[t, lev_pv.tolist().index(200), p, q] > PV_THRESHOLD:
									print "1"
									temp["pv>2_at_200hPa"] = 1
									break_flag_200hPa = True
								if pv[t, lev_pv.tolist().index(250), p, q] > PV_THRESHOLD:
									print "1"
									temp["pv>2_at_250hPa"] = 1
									break_flag_250hPa = True
								if break_flag_200hPa and break_flag_250hPa:
									# within the 5x5 grid, any point works for checking values. no need to check every grid
									break
							except:
								break

		# no PV data for months other than Aug
		else:
			temp["pv>2_at_200hPa"] = "NA"
			temp["pv>2_at_250hPa"] = "NA"

		rows_list.append(temp)
		print "file: ", file.split('/')[-1], "DT processed: ", date

	# write rows_list to csv
	out_df = pd.DataFrame(rows_list, columns=["st. no.", "dates", "pv>2_at_200hPa", "pv>2_at_250hPa"])
	out_df.to_csv(DIRECTORY_PATH + "/" + file.split('/')[-1].split('.')[0] + "_pv2_0.csv", index=False, header=True)
	print "PV check complete for:", file
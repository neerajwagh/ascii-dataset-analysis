from __future__ import division

import numpy as np
import pandas as pd
from dateutil import parser
from netCDF4 import Dataset as NetCDFFile
from netCDF4 import date2index
from scipy.stats.stats import pearsonr

MONTHS = [
	# "Jan",
	# "Feb",
	# "Mar",
	# "Apr",
	# "May",
	"Jun",
	"Jul",
	"Aug",
	"Sep"
	# "Oct",
	# "Nov",
	# "Dec"
]

MONTH_MAP = {
	'Jan' : '01',
	'Feb' : '02',
	'Mar' : '03',
	'Apr' : '04',
	'May' : '05',
	'Jun' : '06',
	'Jul' : '07',
	'Aug' : '08',
	'Sep' : '09',
	'Oct' : '10',
	'Nov' : '11',
	'Dec' : '12'
}

MONTH_END_MAP = {
	'Jan' : '31',
	'Feb' : '28',
	'Mar' : '31',
	'Apr' : '30',
	'May' : '31',
	'Jun' : '30',
	'Jul' : '31',
	'Aug' : '31',
	'Sep' : '30',
	'Oct' : '31',
	'Nov' : '30',
	'Dec' : '31'
}

out_columns = [
	"st. no.",
	"year",
	"month",
	"total_DT_days",
	"total_days_rainfall_gt_60",
	"correlation_1",
	"total_days_rainfall_gt_120",
	"correlation_2"
]

# station coordinates - Delhi
search_for_lat = 28.7041
search_for_lon = 77.1025
NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD = 5

# read 
DIRECTORY_PATH = "./../data/computed_tropopause_data/delhi_all_data.csv"
df = pd.read_csv(DIRECTORY_PATH, na_values=["na_file", "na_data"])
df.dropna(inplace=True)

rows_list = []
DT_days_list = []
monthly_total_rainfall_gt_60_list = []
monthly_total_rainfall_gt_120_list = []

for year in range(1979, 2017):
	
	year = str(year)
	# filter by year
	year_df = df.loc[df['dates'].str.contains(year)]

	# open imd rainfall data
	nc = NetCDFFile("../data/original/IMD/rf_" + year + ".nc")

	lat = nc.variables['lat'][:]
	lon = nc.variables['lon'][:]
	lon_list = lon.tolist()
	lat_list = lat.tolist()
	time = nc.variables['time']

	# FIXME: variable names across IMD files are inconsistent
	try:
		rainfall = nc.variables['Rainfall'][:]
	except:
		rainfall = nc.variables['rf'][:]

	# these define the enclosing grid of above station
	bounding_box_left = 0
	bounding_box_right = 0
	bounding_box_top = 0
	bounding_box_bottom = 0

	for index, i in enumerate(lon_list):
		if search_for_lon > i and search_for_lon < lon_list[index+1]:
			bounding_box_left = index
			bounding_box_right = index + 1
			break
	# lon data is increasing 
	lon_range_lower = bounding_box_left - NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD
	lon_range_higher = bounding_box_right + NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD

	# FIXME: years 2013/4/5 have decreasing lat values
	if int(year) < 2013 or int(year) == 2016:
		for index, i in enumerate(lat_list):
			if search_for_lat > i and search_for_lat < lat_list[index+1]:
				bounding_box_bottom = index
				bounding_box_top = index + 1
				break
		# define how much area around the coordinates to scan
		# lat data is increasing
		lat_range_lower = bounding_box_bottom - NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD
		lat_range_higher = bounding_box_top + NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD
	else:
		for index, i in enumerate(lat_list):
			if search_for_lat < i and search_for_lat > lat_list[index+1]:
				bounding_box_bottom = index + 1
				bounding_box_top = index
				break
		# define how much area around the coordinates to scan
		# lat data is decreasing
		lat_range_lower = bounding_box_bottom + NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD
		lat_range_higher = bounding_box_top - NO_OF_ADJACENT_GRIDS_TO_SCAN_IMD
		
	for month in MONTHS:

		# find index where month begins and ends in nc file
		# FIXME: years 2005-2012 have time as "2010-xx-xx" 
		try:
			month_start_date = year + "-" + MONTH_MAP[month] + "-" + "01"
			month_start_index = date2index(parser.parse(month_start_date), time)
			print month_start_index
		except:
			month_start_date = "2010" + "-" + MONTH_MAP[month] + "-" + "01"
			month_start_index = date2index(parser.parse(month_start_date), time)
			
		try:
			month_end_date = year + "-" + MONTH_MAP[month] + "-" + MONTH_END_MAP[month]
			month_end_index = date2index(parser.parse(month_end_date), time)
			# print month_end_index
		except:
			month_end_date = "2010" + "-" + MONTH_MAP[month] + "-" + MONTH_END_MAP[month]
			month_end_index = date2index(parser.parse(month_end_date), time)
			
		monthly_rainfall_slice = rainfall[month_start_index:month_end_index+1, :, :]
		
		monthly_total_rainfall_gt_60 = 0
		monthly_total_rainfall_gt_120 = 0
		for day_index in range(0, int(MONTH_END_MAP[month])):
			# FIXME:
			if int(year) < 2013 or int(year) == 2016:
				rainfall_today = monthly_rainfall_slice[day_index, lat_range_lower:lat_range_higher+1, lon_range_lower:lon_range_higher+1].max()
			else:
				rainfall_today = monthly_rainfall_slice[day_index, lat_range_higher:lat_range_lower+1, lon_range_lower:lon_range_higher+1].max()
			# FIXME: fill in nan values with interpolation!
			if type(rainfall_today) in [np.float32, np.float64]:
				if rainfall_today > 60.0:
					# print "rainfall > 60: ", rainfall_today
					monthly_total_rainfall_gt_60 += 1
				if rainfall_today > 120.0:
					monthly_total_rainfall_gt_120 += 1
			else:
				print "rainfall_today is nan!"

		monthly_total_rainfall_gt_60_list.append(monthly_total_rainfall_gt_60)
		monthly_total_rainfall_gt_120_list.append(monthly_total_rainfall_gt_120)

		# filter by month, count
		month_df = year_df.loc[year_df['dates'].str.contains(month)]
		DT_days_list.append(month_df.shape[0])

		if (month == "Sep" and year == "2016"):
			# find correlation coefficient b/w the 2 variables for all years
			# https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.stats.pearsonr.html
			correlation = pearsonr(DT_days_list, monthly_total_rainfall_gt_60_list)[0]
			correlation2 = pearsonr(DT_days_list, monthly_total_rainfall_gt_120_list)[0]
			print correlation, correlation2
			# print "p-val: ", pearsonr(DT_days_list,)[1]
		else:
			correlation = ''
			correlation2 = ''

		# save
		temp = {}
		temp["st. no."] = "delhi"
		temp["year"] = year
		temp["month"] = month
		temp["total_DT_days"] = month_df.shape[0]
		temp["total_days_rainfall_gt_60"] = monthly_total_rainfall_gt_60
		temp["correlation_1"] = correlation
		temp["total_days_rainfall_gt_120"] = monthly_total_rainfall_gt_120
		temp["correlation_2"] = correlation2
		rows_list.append(temp)
		
		print "DONE! month: ", month, "year: ", year
	print "----------------------------------------"

out_df = pd.DataFrame(rows_list, columns=out_columns)
out_df.to_csv("../data/computed_tropopause_data/delhi_rainfall_correlation_analysis.csv", index=False, header=True)
#!/usr/bin/env python

# https://blog.dominodatalab.com/simple-parallelization/
from joblib import Parallel, delayed
import multiprocessing

from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()

def download_for_year(year):

    # dates = ["0101", "0201", "0301", "0401", "0501", "0601", "0701", "0801", "0901", "1001", "1101", "1201"]
    dates = ["0601", "0701", "0801", "0901"]
    
    # generate date string for every year
    year_string = ""
    year = str(year)
    for date in dates:
        year_string += year + date + "/"
    year_string = year_string.strip('/')

    config_dict["date"] = year_string
    config_dict["target"] = "../data/original/ERA_Interim/2_Metre_Temperature/" + year + "_2mtemp_JJAS_monthly_mean" + ".nc"

    # fetch data
    server.retrieve(config_dict)

    print "---------------------------------------"
    print "year done: ", year
    print "---------------------------------------"
    return

# inspect the MARS request and create object accordingly
config_dict = {
        "class": "ei",
        "dataset": "interim",
        "expver": "1",
        "grid": "0.75/0.75",
        
        # for 2 metre temperature
        # "date": "19790601/19790701/19790801/19790901",
        "param": "167.128",
        "levtype": "sfc",
        "stream": "moda",
        "type": "an",

        # for surface net solar radiation, clear sky
        # "levtype": "sfc",        
        # "param": "210.128",
        # "step": "0-12",
        # "stream": "mdfa",
        # "type": "fc",

        # for ozone mass mixing ratio - all years only august daily
        # "date": "2016-08-01/to/2016-08-31",
        # "levelist": "1/2/3/5/7/10/20/30/50/70/100/125/150/175/200/225/250/300/350/400/450/500/550/600/650/700/750/775/800/825/850/875/900/925/950/975/1000",
        # "param": "203.128",

        # for potential vorticity
        # "levelist": "1/2/3/5/7/10/20/30/50/70/100/125/150/175/200/225/250/300/350/400/450/500/550/600/650/700/750/775/800/825/850/875/900/925/950/975/1000",
        # "levtype": "pl",
        # "param": "60.128",
        # "step": "0",
        # "stream": "oper",
        # "time": "00:00:00/06:00:00/12:00:00/18:00:00",
        # "type": "an",

        "format": "netcdf"
    }

# clim years from 1979 to 2017
num_cores = multiprocessing.cpu_count()
# for years 2005 to 2017
Parallel(n_jobs=8)(delayed(download_for_year)(year) for year in range(1979, 2018))

print "all years done!"
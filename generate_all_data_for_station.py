import numpy as np
import pandas as pd
from glob2 import glob

station = "nagpur"
DIRECTORY = "../data/copied_dir/" + station + "_ST_all/*.csv.only_ST"
# https://stackoverflow.com/questions/6773584/how-is-pythons-glob-glob-ordered
files = sorted(glob(DIRECTORY))

# http://pandas.pydata.org/pandas-docs/stable/merging.html
frames = [ pd.read_csv(f) for f in files ]
result = pd.concat(frames)

result.to_csv("../data/computed_tropopause_data_ST/" + station + "_all_ST_data.csv", index=False, header=True)
print "all yearly DT files combined for ", station